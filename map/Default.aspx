﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="Simplovation.Web.Maps.VE" Namespace="Simplovation.Web.Maps.VE" TagPrefix="Simplovation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
	<table>
		<tr>
			<td style="border: 1px solid #000000">
				<Simplovation:Map runat="server" ID="Map1" Width="600px" Height="450px" 
                		CssClass="map"
                		Latitude="38.7741667" Longitude="-76.0766667"  Zoom="10" 
                		DashboardSize="Small" ShowTraffic="False" Altitude="0"/>
			</td>
		</tr>
	</table>
    </div>
    </form>
</body>
</html>
