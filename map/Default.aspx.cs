﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Simplovation.Web.Maps.VE;
using OMS.DataAccess.Repository;

public partial class _Default : System.Web.UI.Page
{
    private string connString = ConfigurationManager.ConnectionStrings["dbOMS"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
        /*
        Coordinate custCoord = App_eBilling_Customer_Data.getCustCoordinate(Profile.CustomerDetails.CustNbr);
        Map1.Latitude = custCoord.latitude;
        Map1.Longitude = custCoord.longitude;*/
        Map1.Zoom = 13;

        /*
        SqlConnection myConnection = new SqlConnection(connString);
       SqlDataAdapter dataAdapter = new SqlDataAdapter("sp_MapGetOutages", myConnection);
        DataSet dataSet = new DataSet();
        // fill the DataSet using our DataAdapter 
        dataAdapter.Fill(dataSet);*/
        var outages = OutageRepository.getOutage(start: "", end: "", IDs: null,  isOpen: true);

        foreach (OMS.DataAccess.Models.Outage currOutage in outages)
        {
            PushPin(currOutage.latitude, currOutage.longitude, currOutage.serviceName);
        }
        /*
        foreach (DataRow dr in dataSet.Tables[0].Rows)
        {
            PushPin((double)dr["Latitude"], (double)dr["Longitude"], (string)dr["Service"]);
        }*/
    }

    protected void PushPin(double sLat, double sLong, string sService)
    {
        //Declare and Initialize Variabels
        Simplovation.Web.Maps.VE.Map myLatLong = new Simplovation.Web.Maps.VE.Map();
        myLatLong.Latitude = sLat;
        myLatLong.Longitude = sLong;

        //Define PushPin
        Simplovation.Web.Maps.VE.Shape myShape = new Simplovation.Web.Maps.VE.Shape(myLatLong.LatLong);
        myShape.Title = string.Format("{0} Outage", sService);

        string Icon = "PushPin.png";
        myShape.CustomIcon = new CustomIconSpecification(Icon);

        //Set PushPin

        Map1.AddShape(myShape);
    }
}