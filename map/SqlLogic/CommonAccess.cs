﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for CommonAccess
/// </summary>
public class CommonAccess
{
    public static string CONNECTION_STRING = "";
    public static int COMMAND_TIMEOUT = 120;
    /// <summary>
    /// Gets Connection string from web.config
    /// </summary>
    /// <param name="strConnection">name of connection string</param>
    /// <returns>connection string</returns>
    public static string GetConnectionString(string strConnection)
    {
        if (string.IsNullOrEmpty(strConnection))
            throw new ArgumentNullException("strConnection");

        try
        {
            return ConfigurationManager.ConnectionStrings[strConnection].ConnectionString;
        }
        catch
        {
            return "";
        }
    }
    #region DataTable Functions (SQL text)
    /// <summary>
    /// Basic data access class to retrieve a datatable.  Using this method with the following
    /// signiture requires that the CONNECTION_STRING property be pre-configured.
    /// </summary>
    /// <param name="sqlText">Sql Text</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static DataTable GetDataTextCommand(string sqlText, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sqlText) || sqlText.Length == 0)
            throw new ArgumentNullException("sqlText");

        // Fail if CONNECTION_STRING is not set
        if (string.IsNullOrEmpty(CONNECTION_STRING))
            throw new ArgumentNullException("CONNECTION_STRING");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sqlText, conn))
            {
                conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable.
    /// </summary>
    /// <param name="connectionString">Database connection string</param>
    /// <param name="sqlText">Sql Text</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static DataTable GetDataTextCommand(string connectionString, string sqlText, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sqlText))
            throw new ArgumentNullException("sqlText");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentNullException("connectionString");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sqlText, conn))
            {
                conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable. Use this method with the following signature
    /// when the specified stored procedure does not require any parameters.
    /// </summary>
    /// <param name="sqlText">Sql Text</param>
    /// <param name="sproc">Stored procedure name</param>
    /// <returns></returns>
    public static DataTable GetDataTextCommand(string connectionString, string sqlText)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sqlText))
            throw new ArgumentNullException("sqlText");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentNullException("connectionString");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sqlText, conn))
            {
                conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = COMMAND_TIMEOUT;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable. Use this method with the following signature
    /// when the specified stored procedure does not require any parameters and the CONNECTION_STRING 
    /// property has been pre-configured.
    /// </summary>
    /// <param name="sqlText">Sql Text</param>
    /// <returns></returns>
    public static DataTable GetDataTextCommand(string sqlText)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sqlText))
            throw new ArgumentNullException("sqlText");

        // fail if CONNECTION_STRING is not set
        if (string.IsNullOrEmpty(CONNECTION_STRING))
            throw new ArgumentNullException("CONNECTION_STRING");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sqlText, conn))
            {
                conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = COMMAND_TIMEOUT;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    #endregion
    #region DataTable Functions

    /// <summary>
    /// Basic data access class to retrieve a datatable.  Using this method with the following
    /// signiture requires that the CONNECTION_STRING property be pre-configured.
    /// </summary>
    /// <param name="sproc">Stored procedure name</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static DataTable GetData(string sproc, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc) || sproc.Length == 0)
            throw new ArgumentNullException("sproc");

        // Fail if CONNECTION_STRING is not set
        if (string.IsNullOrEmpty(CONNECTION_STRING))
            throw new ArgumentNullException("CONNECTION_STRING");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable.
    /// </summary>
    /// <param name="connectionString">Database connection string</param>
    /// <param name="sproc">Stored procedure name</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static DataTable GetData(string connectionString, string sproc, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc))
            throw new ArgumentNullException("sproc");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentNullException("connectionString");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable. Use this method with the following signature
    /// when the specified stored procedure does not require any parameters.
    /// </summary>
    /// <param name="connectionString">Database connection</param>
    /// <param name="sproc">Stored procedure name</param>
    /// <returns></returns>
    public static DataTable GetData(string connectionString, string sproc)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc))
            throw new ArgumentNullException("sproc");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentNullException("connectionString");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    /// <summary>
    /// Basic data access class to retrieve a datatable. Use this method with the following signature
    /// when the specified stored procedure does not require any parameters and the CONNECTION_STRING 
    /// property has been pre-configured.
    /// </summary>
    /// <param name="sproc">Stored procedure name</param>
    /// <returns></returns>
    public static DataTable GetData(string sproc)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc))
            throw new ArgumentNullException("sproc");

        // fail if CONNECTION_STRING is not set
        if (string.IsNullOrEmpty(CONNECTION_STRING))
            throw new ArgumentNullException("CONNECTION_STRING");

        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
        }

        return dt;
    }

    public static void ExecuteNonQuery(string connectionString, string sproc, SqlParameter[] sqlParameters)
    {
        using (SqlConnection Conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 120;

                for (int k = 0; k < sqlParameters.Length; k++)
                {
                    command.Parameters.Add(sqlParameters[k]);
                }

                command.ExecuteNonQuery();
            }
        }
    }

    public static void ExecuteNonQuery(string connectionString, string sproc)
    {
        using (SqlConnection Conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 120;
                command.ExecuteNonQuery();
            }
        }
    }

    public static void ExecuteNonQuery(string sproc)
    {
        using (SqlConnection Conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sproc, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 120;
                command.ExecuteNonQuery();
            }
        }
    }

    public static void ExecuteNonQueryText(string connectionString, string sql, SqlParameter[] sqlParameters)
    {
        using (SqlConnection Conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sql, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 120;

                for (int k = 0; k < sqlParameters.Length; k++)
                {
                    command.Parameters.Add(sqlParameters[k]);
                }

                command.ExecuteNonQuery();
            }
        }
    }

    public static void ExecuteNonQueryText(string connectionString, string sql)
    {
        using (SqlConnection Conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sql, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 120;
                command.ExecuteNonQuery();
            }
        }
    }

    public static void ExecuteNonQueryText(string sql)
    {
        using (SqlConnection Conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sql, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 120;
                command.ExecuteNonQuery();
            }
        }
    }
    #endregion
    #region Scalar Functions Sql Text Command
    /// <summary>
    /// Basic data access class to retrieve a single string value.
    /// </summary>
    /// <param name="connectionString">Database connection string</param>
    /// <param name="sqlText">Sql command</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static string GetDataStringTextCommand(string connectionString, string sqlText, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sqlText) || sqlText.Length == 0)
            throw new ArgumentNullException("sqlText");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString) || connectionString.Length == 0)
            throw new ArgumentNullException("connectionString");

        string result = string.Empty;

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sqlText, conn))
            {
                conn.Open();
                command.CommandType = CommandType.Text;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                result = Convert.ToString(command.ExecuteScalar());
            }
        }
        return result;
    }
    #endregion
    #region Scalar Functions

    /// <summary>
    /// Basic data access class to retrieve a single string value.
    /// </summary>
    /// <param name="connectionString">Database connection string</param>
    /// <param name="sproc">Stored procedure name</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static string GetDataString(string connectionString, string sproc, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc) || sproc.Length == 0)
            throw new ArgumentNullException("sproc");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString) || connectionString.Length == 0)
            throw new ArgumentNullException("connectionString");

        string result = string.Empty;

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                result = Convert.ToString(command.ExecuteScalar());
            }
        }
        return result;
    }

    public static string GetDataStringBiDirectional(string connectionString, string sproc, SqlParameter[] sqlParameters, string biDirectionParameter)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc) || sproc.Length == 0)
            throw new ArgumentNullException("sproc");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString) || connectionString.Length == 0)
            throw new ArgumentNullException("connectionString");

        string result = string.Empty;

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                command.ExecuteReader();
                result = command.Parameters[biDirectionParameter].Value.ToString();

            }
        }
        return result;
    }

    /// <summary>
    /// Basic data access class to retrieve a single integer value.
    /// </summary>
    /// <param name="connectionString">Database connection string</param>
    /// <param name="sproc">Stored procedure name</param>
    /// <param name="sqlParameters">Required stored procedure parameters</param>
    /// <returns></returns>
    public static int GetDataInteger(string connectionString, string sproc, SqlParameter[] sqlParameters)
    {
        // Fail if sproc is not specified
        if (string.IsNullOrEmpty(sproc))
            throw new ArgumentNullException("sproc");

        // Fail if connectionString is not specified
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentNullException("connectionString");

        int result = 0;

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, conn))
            {
                conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = COMMAND_TIMEOUT;

                if (sqlParameters != null)
                {
                    for (int i = 0; i < sqlParameters.Length; i++)
                    {
                        command.Parameters.Add(sqlParameters[i]);
                    }
                }

                result = Convert.ToInt32(command.ExecuteScalar());
            }
        }
        return result;
    }

    public static bool GetDataBoolean(string sproc, SqlParameter[] sqlParams)
    {
        bool answer = false;
        using (SqlConnection Conn = new SqlConnection(CONNECTION_STRING))
        {
            using (SqlCommand command = new SqlCommand(sproc, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                for (int k = 0; k < sqlParams.Length; k++)
                {
                    command.Parameters.Add(sqlParams[k]);
                }
                answer = Convert.ToBoolean(Convert.ToInt32(command.ExecuteScalar().ToString()));
            }
        }
        return answer;
    }

    public static bool GetDataBoolean(string connectionString, string sproc, SqlParameter[] sqlParams)
    {
        bool answer = false;
        using (SqlConnection Conn = new SqlConnection(connectionString))
        {
            using (SqlCommand command = new SqlCommand(sproc, Conn))
            {
                Conn.Open();
                command.CommandType = CommandType.StoredProcedure;
                for (int k = 0; k < sqlParams.Length; k++)
                {
                    command.Parameters.Add(sqlParams[k]);
                }
                answer = Convert.ToBoolean(command.ExecuteScalar().ToString());
            }
        }
        return answer;
    }

    #endregion
	
}