﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Live.ServerControls.VE;

/// <summary>
/// Summary description for MapOutage
/// </summary>
public class MapOutage
{
    public LatLongWithAltitude coordinates;
    public string outageType;

    public MapOutage(double latitude, double longitude, string outageType)
	{
        this.coordinates = new LatLongWithAltitude(latitude, longitude);
        this.outageType = outageType;
	}


}