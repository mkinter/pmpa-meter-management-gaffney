﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "https://olp1.eastonutilities.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {

    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public SendAccountResetRootObject Reset(string input)
    {
        return CableBridgeWebService.SendAccountReset(input);
    }

    [WebMethod]
    public StatusCheckRootObject StatusCheck(string queueID, string username)
    {
        return CableBridgeWebService.StatusCheck(queueID, username);
    }
}
