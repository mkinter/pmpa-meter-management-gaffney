﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using Utilities.Logging;
using Utilities.Security;

/// <summary>
/// Summary description for CableBridgeWebService
/// </summary>
public class CableBridgeWebService
{
    //http://localhost:59223/CablebridgeAPI/3/Account/999998/Reset?input=test
    private static string ORIGIN_ID = Utilities.Data.DataUtilities.GetAppSetting("OriginID");
    private static string API_URL = string.Format(Utilities.Data.DataUtilities.GetAppSetting("CablebridgeAPI"), ORIGIN_ID);
    private static string ACCOUNTRESET_URL = string.Format("{0}{1}", API_URL, Utilities.Data.DataUtilities.GetAppSetting("AccountResetEndpoint"));
    private static string CHECKSTATUS_URL = string.Format("{0}{1}", API_URL, Utilities.Data.DataUtilities.GetAppSetting("CheckStatusEndpoint"));
    private static bool LOGTEST = Utilities.Data.DataUtilities.GetAppSetting("LogTest") == "1" ? true : false;

    private const string FAILMESSAGE = "Cable reset command did not process. Please contact Customer Service.";
    private const string SUCCESSMESSAGE = "Cable box reset complete! If you are still experiencing issues with your cable service, please contact Customer Service by phone (410.822.6110)";

    public static void SendAccountReset(string username, string locationId)
    {
        var parms = new
        {
            LocationId = locationId,
            Username = username
        };
        var jsonString = JsonConvert.SerializeObject(parms);
        var encrypted = AES2.Encrypt(jsonString);
        encrypted = System.Uri.EscapeDataString(encrypted);

        string address = string.Format(
            ACCOUNTRESET_URL,
            Uri.EscapeDataString(locationId),
            encrypted);
        string responsebody;
        using (WebClient client = new WebClient())
        {
            var responsebytes = client.UploadValues(address, "POST", new NameValueCollection());
            responsebody = new System.Text.UTF8Encoding().GetString(responsebytes);
        }

        var obj = JsonConvert.DeserializeObject<SendAccountResetRootObject>(responsebody);

    }
    public static SendAccountResetRootObject SendAccountReset(string input)
    {
        try
        {
            input = System.Uri.UnescapeDataString(input);
            if (!string.IsNullOrEmpty(input))
                input = AES2.Decrypt(input);
            else
                throw new ArgumentException("Invalid input: Query string parameter 'input' was not provided.");


            var locationID = InputUtility.GetInputValue(input, "LocationID");
            var username = InputUtility.GetInputValue(input, "Username");

            var parms = new
            {
                LocationId = locationID,
                Username = username
            };


            var jsonString = JsonConvert.SerializeObject(parms);
            var encrypted = AES2.Encrypt(jsonString);
            encrypted = System.Uri.EscapeDataString(encrypted);

            string address = string.Format(
                ACCOUNTRESET_URL,
                Uri.EscapeDataString(locationID),
                encrypted);
            string responsebody;
            using (WebClient client = new WebClient())
            {
                var responsebytes = client.UploadValues(address, "POST", new NameValueCollection());
                responsebody = new System.Text.UTF8Encoding().GetString(responsebytes);
            }


            var obj = JsonConvert.DeserializeObject<SendAccountResetRootObject>(responsebody);
            if (obj.ResponseCode != 1)
                obj.ResponseString = FAILMESSAGE;

            if (LOGTEST)
                LogWriter.Write(string.Format("CableBridgeWebService test: {0}. Method: SendAccountReset, Input: {1}", "just a test", input));

            return obj;

        }
        catch (Exception ex)
        {
            LogWriter.Write(string.Format("CableBridgeWebService exception: {0}. Method: SendAccountReset, Input: {1}", ex.Message, input));
            return new SendAccountResetRootObject{
                QueueId = "N/A",
                ResponseCode = 0, 
                ResponseString = "Internal Server Error. Please contact Customer Service."
            };
        }
    }

    public static StatusCheckRootObject StatusCheck(string queueID, string username)
    {
        try
        {
            var parms = new
            {
                Guid = queueID,
                Username = username
            };

            var jsonString = JsonConvert.SerializeObject(parms);
            var encrypted = AES2.Encrypt(jsonString);
            encrypted = System.Uri.EscapeDataString(encrypted);

            string address = string.Format(
                CHECKSTATUS_URL,
                encrypted,
                username);
            string responsebody;
            using (WebClient client = new WebClient())
            {
                responsebody = client.DownloadString(address);
            }

            var obj = JsonConvert.DeserializeObject<StatusCheckRootObject>(responsebody);
            if (obj.QueueStatusCode == 4)
            {
                obj.QueueStatusString = FAILMESSAGE;
            }

            if (obj.QueueStatusCode == 3)
            {
                obj.UserMessage = SUCCESSMESSAGE;
            }
            if (LOGTEST)
                LogWriter.Write(string.Format("CableBridgeWebService Test: {0}. Method: StatusCheck, QueueID: {1}. Username: {2}", "Just a test", queueID, username));


            //if (obj.QueueStatusCode == 2)
            //    obj.ResponseCode = 0;
            return obj;
        }
        catch (Exception ex)
        {
            LogWriter.Write(string.Format("CableBridgeWebService exception: {0}. Method: StatusCheck, QueueID: {1}. Username: {2}", ex.Message, queueID, username));
            return new StatusCheckRootObject
            {
                QueueStatusCode = 4,
                QueueStatusString = "N/A",
                ResponseCode = 0,
                ResponseString = "Internal Server Error. Please contact Customer Service."
            };
        }

    }


}
public class StatusCheckRootObject
{
    public int QueueStatusCode { get; set; }
    public string QueueStatusString { get; set; }
    public int ResponseCode { get; set; }
    public string ResponseString { get; set; }
    public string UserMessage { get; set; }
}

public class SendAccountResetRootObject
{
    public string QueueId { get; set; }
    public int ResponseCode { get; set; }
    public string ResponseString { get; set; }
}

public class InputUtility
{
    public static string GetUsername(string input)
    {
        try
        {
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(input);

            if (dict != null)
                return (string)dict["Username"];
            else
                throw new ArgumentException("Invalid input: Username was not provided in query string parameter 'input'.");
        }
        catch
        {
            throw new ArgumentException("Invalid input: Username was not provided in query string parameter 'input'.");
        }
    }

    public static string GetInputValue(string input, string value)
    {
        try
        {
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(input);

            if (dict != null)
                return (string)dict[value];
            else
                throw new ArgumentException(string.Format("Invalid input: {0} was not provided in query string parameter 'input'."), value);
                //return string.Empty;
        }
        catch
        {
            throw new ArgumentException(string.Format("Invalid input: {0} was not provided in query string parameter 'input'."), value);
        }
    }
}