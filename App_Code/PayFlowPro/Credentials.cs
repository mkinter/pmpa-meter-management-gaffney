﻿using System;
using System.Collections.Generic;
using PayPal.Payments.DataObjects;
using System.Data;

/// <summary>
/// Summary description for Credentials
/// </summary>
public class Credentials
{
    private static string connectionString = CommonAccess.GetConnectionString("dbGP"), host;
    private static UserInfo userInfo;
    private static int port;
    public static bool debug = false;
    static Credentials()
    {
        InitValues();
    }
    public static UserInfo GetUserInfo()
    {
        return userInfo;
    }

    public static int GetPort()
    {
        return port;
    }
    public static string GetHost()
    {
        return host;
    }
    private static void InitValues()
    {
        string user, vendor, partner, password;
        userInfo = null;
        if (debug)
        {
            DataTable dt = CommonAccess.GetDataTextCommand(CommonAccess.GetConnectionString("dbGPTest"), 
                "SELECT TOP 1 * FROM UMVerisignOptions");
            foreach (DataRow row in dt.Rows)
            {
                user = row["UserID"].ToString().Trim();
                vendor = row["Vendor"].ToString().Trim();
                partner = row["Partner"].ToString().Trim();
                password = row["Pwd"].ToString().Trim();
                port = Int32.Parse(row["Port"].ToString().Trim());
                //host = row["Host"].ToString().Trim();
                host = "pilot-payflowpro.paypal.com";
                //host = "payflowpro.paypal.com";
                userInfo = new UserInfo(user, vendor, partner, password);
            }
        }
        else
        {
            DataTable dt = CommonAccess.GetDataTextCommand(connectionString, "SELECT TOP 1 * FROM UMVerisignOptions");
            foreach (DataRow row in dt.Rows)
            {
                user = row["UserID"].ToString().Trim();
                vendor = row["Vendor"].ToString().Trim();
                partner = row["Partner"].ToString().Trim();
                password = row["Pwd"].ToString().Trim();
                port = Int32.Parse(row["Port"].ToString().Trim());
                //host = row["Host"].ToString().Trim();
                //host = "pilot-payflowpro.paypal.com";
                host = "payflowpro.paypal.com";
                userInfo = new UserInfo(user, vendor, partner, password);
            }
        }
    }
}