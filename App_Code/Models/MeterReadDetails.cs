﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MeterReadDetails
/// </summary>
public class MeterReadDetails
{
    public string ReadingDate { get; set; }
    public Decimal PosKWh { get; set; }
    public Decimal PrevPosKWh { get; set; }
    public string PrevPosKWhReadingDate { get; set; }
    public Decimal Usage { get; set; }
    public Decimal Demand { get; set; }

}