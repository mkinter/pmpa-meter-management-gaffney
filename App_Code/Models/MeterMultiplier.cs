﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Multiplier
/// </summary>
public class MeterMultiplier
{
    public int Id { get; set; }
    public int GroupId { get; set; }
    public int CompanyId { get; set; }
    public string MeterNumber { get; set; }
    public decimal Multiplier { get; set; }

}