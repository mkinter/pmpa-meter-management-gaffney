﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

/// <summary>
/// Summary description for BaseMeterRead
/// </summary>
public class BaseMeterRead
{
    public DateTime ReadingDate { get; set; }
    public Decimal ReadValue { get; set; }

    public BaseMeterRead()
    {
    }

	public BaseMeterRead(DateTime date, decimal value)
	{
        this.ReadingDate = date;
        this.ReadValue = value;
	}

    public int GetMonth()
    {
        if (ReadingDate != null)
            return ReadingDate.Month;
        else
            return 0;
    }

    public string GetMonthYear()
    {
        return string.Format("{0} {1}", GetMonthName(), GetYear().ToString());
    }

    public string GetMonthName()
    {
        if (ReadingDate != null)
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(GetMonth());
        else
            return string.Empty;
    }

    public int GetDay()
    {
        if (ReadingDate != null)
            return ReadingDate.Day;
        else
            return 0;
    }

    public int GetHour()
    {
        if (ReadingDate != null)
            return ReadingDate.Hour;
        else
            return 0;
    }

    public int GetYear()
    {
        if (ReadingDate != null)
            return ReadingDate.Year;
        else
            return 0;
    }

}