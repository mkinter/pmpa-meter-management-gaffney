﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class MonthData
{
    public List<DD> dayData { get; set; }
    public decimal MTotal { get; set; }
    public int MYear { get; set; }
    public string MName { get; set; }
    public MonthData()
    {

    }
}

public class DD
{
    public List<HD> hourData { get; set; }
    public decimal DTotal { get; set; }
    public string DName { get; set; }
    public DD() { }
}

public class HD
{
    public string h { get; set; }
    public decimal v { get; set; }
    public HD() { }
}