﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Utilities.Data;
using Utilities.Extensions;

/// <summary>
/// Summary description for MeterRepository
/// </summary>
public class MeterData
{
    private static string ConnectionString = Utilities.Data.DataUtilities.GetConnectionString("cpSOCKET_MultiSpeak");

    public MeterData()
    {

    }

    //public static List<MonthData> getChartDataNew(int companyId, string meterNuber, DateTime sDate, DateTime eDate, int interval)
    //{
    //    Dictionary<string, MonthData> data = new Dictionary<string, MonthData>();

    //    var retVal = new List<MonthData>();
    //    MonthData monthData = null;
    //    DayData dayData = null;
    //    HourData hourData = null;

    //    DateTime readingDate;
    //    Decimal usage = 0;
    //    BaseMeterRead rd = null;
    //    List<BaseMeterRead> reads = null;

    //    var parameters = new SqlParameter[]
    //        {
    //            new SqlParameter("@companyId", companyId),
    //            new SqlParameter("@meterNumber", meterNuber),
    //            new SqlParameter("@sDate", sDate),
    //            new SqlParameter("@eDate", eDate),
    //            new SqlParameter("@MinuteInterval", interval)
    //        };

    //    DataTable dt = CommonAccess.GetData(ConnectionString, "MR.GetReadingsByCombination", parameters);


    //    if (dt != null && dt.Rows.Count > 0)
    //    {
    //        reads = new List<BaseMeterRead>();

    //        foreach (DataRow row in dt.Rows)
    //        {
    //            rd = new BaseMeterRead(row.GetDateTimeMin("ReadingDate"), row.GetDecimal("Usage"));
    //            reads.Add(rd);
    //        }
    //    }

    //    if (reads != null && reads.Count > 0)
    //    {
    //        foreach (var r in reads)
    //        {
    //            if (!data.ContainsKey(r.GetMonthYear()))
    //                data[r.GetMonthYear()] = new MonthData();

    //            var t = data[r.GetMonthYear()];
    //            t.monthName = r.GetMonthName();
    //            t.monthTotalValue = t.monthTotalValue + r.ReadValue;

    //            if (t.dayData == null)
    //            {
    //                t.dayData = new List<DayData>();
    //            }


    //        }


    //        //var groups = (from r in reads
    //        //             group r by new {Year = r.GetYear(), Month = r.GetMonth(), MonthName = r.GetMonthName(), Day = r.GetDay(), Hour = r.GetHour()} into g
    //        //             select new { Year = g.Key.Year, Month = g.Key.Month, MonthName = g.Key.MonthName, YearMonth = g.Key.MonthName + ' ' + g.Key.Year.ToString(), Day = g.Key.Day, g.Key.Hour, Usage = g.Sum(x => x.ReadValue)}).OrderBy(a => a.Year).OrderBy(b => b.Month).OrderBy(c => c.Day).OrderBy(d => d.Hour);

    //        //if(groups != null)
    //        //{
    //        //    foreach(var g in groups)
    //        //    {
    //        //        if (!data.ContainsKey(g.YearMonth))
    //        //            data[g.YearMonth] = new MonthData();


    //        //    }

    //        //}
    //    }


    //    return retVal;
    //}

    public static List<MonthData> GetChartDataByGroup(int companyId, int groupId)
    {
        var retVal = new List<MonthData>();
        var endDate = DateTime.Now;
        var startDate = endDate.AddMonths(-13).Date;
        startDate = new DateTime(startDate.Year, startDate.Month, 1);

        var sqlParams = new SqlParameter[]{
            new SqlParameter("@companyId", companyId),
            new SqlParameter("@groupId", groupId),
            new SqlParameter("@sDate", startDate),
            new SqlParameter("@eDate", endDate)
        };

        //var meterDS = CommonAccess.GetData(ConnectionString, "MR.GetReadingsByGroup_v2", sqlParams);
        var meterDS = CommonAccess.GetData(ConnectionString, "MR.GetReadingsByGroup_v3", sqlParams);


        var tmpMonth = new MonthData();
        var tmpMonthTotal = 0m;
        var tmpDayDataList = new List<DD>();
        var tmpDayTotal = 0m;
        var tmpDayData = new DD();
        var tmpHourDataList = new List<HD>();
        //var previousMeterRead = meterDS.Rows[0].GetDecimal("posKWh");
        for (var i = 0; i < meterDS.Rows.Count; i++)
        {
            var currRow = meterDS.Rows[i] as DataRow;
            var currReadingDate = currRow.GetDateTimeMin("ReadingDate");
            var currMonthYear = currReadingDate.Year;
            var currMonth = currReadingDate.Month;
            var currDay = currReadingDate.Day.ToString();
            var currMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currMonth);
            var currHour = currReadingDate.Hour;

            //var usage = currRow.GetDecimal("Usage");
            var demand = currRow.GetDecimal("Demand");


            //var currMeterRead = currRow.GetDecimal("posKWh");
            //var currConsumptionValue = currMeterRead - previousMeterRead;

            //if (tmpDayData.DName == null)
            if(string.IsNullOrEmpty(tmpDayData.DName))
            {
                tmpDayData.DName = currDay;
            }
            //if (tmpMonth.MName == null)
            if(string.IsNullOrEmpty(tmpMonth.MName))
            {
                tmpMonth.MName = currMonthName;
                tmpMonth.MYear = currMonthYear;
            }
            //Finished with current Day object
            //if (tmpDayData.DName != currDay)
            if (currHour == 1 && tmpHourDataList.Count > 0)
            {
                tmpDayData.DTotal = tmpDayTotal;
                tmpMonthTotal += tmpDayTotal;
                tmpDayData.hourData = tmpHourDataList;
                tmpDayDataList.Add(tmpDayData);

                tmpHourDataList = new List<HD>();
                tmpDayData = new DD();
                tmpDayData.DName = currDay;
                tmpDayTotal = 0;
            }
            //Finished with previous month and this is not the start of the first month parse
            //if (tmpMonth.MName != currMonthName && tmpMonth.MName != "")
            //if(tmpMonth.MName != currMonthName && !string.IsNullOrEmpty(tmpMonth.MName))
            if (tmpMonth.MName != currMonthName && !string.IsNullOrEmpty(tmpMonth.MName) && currHour != 0)
            {
                tmpMonth.MTotal = tmpMonthTotal;
                tmpMonth.dayData = tmpDayDataList;
                retVal.Add(tmpMonth);

                tmpMonth = new MonthData();
                tmpMonth.MName = currMonthName;
                tmpMonth.MYear = currMonthYear;
                tmpMonthTotal = 0;
                tmpDayDataList = new List<DD>();
            }

            tmpHourDataList.Add(new HD
            {
                h = currHour.ToString(),
                v = demand
                //value = currConsumptionValue
            });
            tmpDayTotal += demand;
            //tmpDayTotal += currConsumptionValue;

            //previousMeterRead = currMeterRead;
        }

        tmpDayData.DTotal = tmpDayTotal;
        tmpMonthTotal += tmpDayTotal;
        tmpDayData.hourData = tmpHourDataList;
        tmpDayDataList.Add(tmpDayData);
        tmpMonth.MTotal = tmpMonthTotal;
        tmpMonth.dayData = tmpDayDataList;

        retVal.Add(tmpMonth);

        return retVal;
    }

    public static List<MeterReadDetails> GetMeterReadingsByGroupId(int companyId, int groupId, DateTime sDate, DateTime eDate, int interval)
    {
        List<MeterReadDetails> details = null;
        MeterReadDetails d = null;
        var sqlParams = new SqlParameter[]{
            new SqlParameter("@companyId", companyId),
            new SqlParameter("@groupId", groupId),
            new SqlParameter("@sDate", sDate),
            new SqlParameter("@eDate", eDate),
            new SqlParameter("@MinuteInterval", interval)
        };

        //var meterDS = CommonAccess.GetData(ConnectionString, "MR.GetReadingsByGroup_v2", sqlParams);
        var data = CommonAccess.GetData(ConnectionString, "MR.GetReadingsByGroup_v3", sqlParams);

        if(data != null && data.Rows != null && data.Rows.Count > 0)
        {
            details = new List<MeterReadDetails>();

            foreach (DataRow r in data.Rows)
            {
                d = new MeterReadDetails();

                d.ReadingDate = r.GetDateTimeMin("ReadingDate").ToString();
                d.PosKWh = r.GetDecimal("posKWh");
                d.PrevPosKWh = r.GetDecimal("PrevPosKWh");
                d.PrevPosKWhReadingDate = r.GetDateTimeMin("PrevPosKWhReadingDate").ToString();
                d.Usage = r.GetDecimal("Usage");
                d.Demand = r.GetDecimal("Demand");
                details.Add(d);
            }
        }
        return details;
    }

    public static List<MeterMultiplier> GetMultipliersByGroup(int companyId, int groupId)
    {
        List<MeterMultiplier> multipliers = null;
        MeterMultiplier m = null;

        var sqlParams = new SqlParameter[]{
            new SqlParameter("@companyId", companyId),
            new SqlParameter("@groupId", groupId)};

        var data = CommonAccess.GetData(ConnectionString, "MR.GetMultipliersByGroupId", sqlParams);

        if (data != null && data.Rows != null && data.Rows.Count > 0)
        {
            multipliers = new List<MeterMultiplier>();
            foreach(DataRow r in data.Rows)
            {
                m = new MeterMultiplier();
                m.Id = r.GetInt32("Id");
                m.MeterNumber = r.GetString("MeterNumber");
                m.Multiplier = r.GetDecimal("Multiplier");
                m.CompanyId = r.GetInt32("CompanyId");
                m.GroupId = r.GetInt32("MeterGroupId");
                multipliers.Add(m);
            }
        }

        return multipliers;
    }

    //public static List<MonthData> getChartData(string meterID)
    //{
    //    var retVal = new List<MonthData>();
    //    var endDate = DateTime.Now;
    //    var startDate = endDate.AddMonths(-13).Date;
    //    startDate = new DateTime(startDate.Year, startDate.Month, 1);
    //    var sqlParams = new SqlParameter[]{
    //        new SqlParameter("@meterID", meterID),
    //        new SqlParameter("@startDate", startDate),
    //        new SqlParameter("@endDate", endDate)
    //    };

    //    var meterDS = CommonAccess.GetData(connectionString: ConnectionString, sproc: "Portal.GetMeterReadsForConsumptionChart", sqlParameters: sqlParams);

        


    //    var tmpMonth = new MonthData();
    //    var tmpMonthTotal = 0m;
    //    var tmpDayDataList = new List<DayData>();
    //    var tmpDayTotal = 0m;
    //    var tmpDayData = new DayData();
    //    var tmpHourDataList = new List<HourData>();
    //    var previousMeterRead = meterDS.Rows[0].GetDecimal("posKWh");
    //    for (var i = 1; i < meterDS.Rows.Count; i++)
    //    {
    //        var currRow = meterDS.Rows[i] as DataRow;
    //        var currReadingDate = currRow.GetDateTimeMin("ReadingDate");
    //        var currMonth = currReadingDate.Month;
    //        var currDay = currReadingDate.Day.ToString();
    //        var currMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currMonth);
    //        var currHour = currReadingDate.Hour;
            
    //        //var previousMeterRead = meterDS.Rows[i-1].GetDecimal("posKWh");
    //        var currMeterRead = currRow.GetDecimal("posKWh");
    //        var currConsumptionValue = currMeterRead - previousMeterRead;

    //        if (tmpDayData.dayName == null)
    //        {
    //            tmpDayData.dayName = currDay;
    //        }
    //        if (tmpMonth.monthName == null)
    //        {
    //            tmpMonth.monthName = currMonthName;
    //        }
    //        //Finished with current Day object
    //        if (tmpDayData.dayName != currDay)
    //        {
    //            tmpDayData.dayTotalValue = tmpDayTotal;
    //            tmpMonthTotal += tmpDayTotal;
    //            tmpDayData.hourData = tmpHourDataList;
    //            tmpDayDataList.Add(tmpDayData);

    //            tmpHourDataList = new List<HourData>();
    //            tmpDayData = new DayData();
    //            tmpDayData.dayName = currDay;
    //            tmpDayTotal = 0;
    //        }
    //        //Finished with previous month and this is not the start of the first month parse
    //        if(tmpMonth.monthName != currMonthName && tmpMonth.monthName != "")
    //        {
    //            tmpMonth.monthTotalValue = tmpMonthTotal;
    //            tmpMonth.dayData = tmpDayDataList;
    //            retVal.Add(tmpMonth);

    //            tmpMonth = new MonthData();
    //            tmpMonth.monthName = currMonthName;
    //            tmpMonthTotal = 0;
    //            tmpDayDataList = new List<DayData>();
    //        }

    //        tmpHourDataList.Add(new HourData
    //        {
    //            hour = currHour.ToString(),
    //            value = currConsumptionValue
    //        });
    //        tmpDayTotal += currConsumptionValue;

    //        previousMeterRead = currMeterRead;
    //    }

    //    tmpDayData.dayTotalValue = tmpDayTotal;
    //    tmpMonthTotal += tmpDayTotal;
    //    tmpDayData.hourData = tmpHourDataList;
    //    tmpDayDataList.Add(tmpDayData);
    //    tmpMonth.monthTotalValue = tmpMonthTotal;
    //    tmpMonth.dayData = tmpDayDataList;

    //    retVal.Add(tmpMonth);

    //    //for (var currMonth = 1; currMonth <= 3; currMonth++)
    //    //{
    //    //    var tmpMonth = new MonthData();
    //    //    tmpMonth.monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currMonth);
    //    //    var tmpDayDataList = new List<DayData>();
    //    //    var tmpMonthTotal = 0;
    //    //    for (var i = 1; i <= 31; i++)
    //    //    {
    //    //        var tmpHourDataList = new List<HourData>();
    //    //        var tmpDayData = new DayData();
    //    //        tmpDayData.dayName = i.ToString();
    //    //        var tmpTotal = 0;
    //    //        for (var x = 0; x < 24; x++)
    //    //        {
    //    //            var val = rand.Next(0, 100);
    //    //            tmpHourDataList.Add(new HourData
    //    //            {
    //    //                hour = x.ToString(),
    //    //                value = val
    //    //            });
    //    //            tmpTotal += val;

    //    //        }
    //    //        tmpDayData.dayTotalValue = tmpTotal;
    //    //        tmpMonthTotal += tmpTotal;
    //    //        tmpDayData.hourData = tmpHourDataList;
    //    //        tmpDayDataList.Add(tmpDayData);
    //    //    }
    //    //    tmpMonth.monthTotalValue = tmpMonthTotal;
    //    //    tmpMonth.dayData = tmpDayDataList;

    //    //    retVal.Add(tmpMonth);
    //    //}

    //    return retVal;
    //}
}