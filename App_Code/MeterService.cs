﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for MeterService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class MeterService : System.Web.Services.WebService {

    public MeterService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //[WebMethod]
    //public List<MonthData> GetChartData(string meterNumber)
    //{
    //    return MeterData.getChartData(meterNumber);
    //}

    [WebMethod]
    public List<MonthData> GetChartDataByMeterGroupId(int companyId, int groupId)
    {
        return MeterData.GetChartDataByGroup(companyId, groupId);

    }

    [WebMethod]
    public List<MeterReadDetails> GetReadingsByMeterGroupId(int companyId, int groupId, long sDate, long eDate, int interval)
    {
        DateTime inputStart = new DateTime(1970,1,1).AddTicks(sDate * 10000);
        inputStart = inputStart.AddSeconds(1);
        DateTime inputEnd = new DateTime(1970, 1, 1).AddTicks(eDate * 10000);

        List<MeterReadDetails> details = MeterData.GetMeterReadingsByGroupId(companyId, groupId, inputStart, inputEnd, interval);

        if(details != null)
        {
            var r = from d in details
                      where DateTime.Parse(d.ReadingDate) >= inputStart && DateTime.Parse(d.ReadingDate) <= inputEnd
                      select d;

            if (r != null)
                details = r.ToList<MeterReadDetails>();
        }
        return details;
    }
    
    [WebMethod]
    public List<MeterMultiplier> GetMultipliersByGroup(int companyId, int groupId)
    {
        return MeterData.GetMultipliersByGroup(companyId, groupId);
    }
}
