﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Coordinate
{
    public double latitude { get; set; }
    public double longitude { get; set; }
}
