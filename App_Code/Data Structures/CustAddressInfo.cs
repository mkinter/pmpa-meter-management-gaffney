﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CustInfo
/// </summary>
public class CustAddressInfo
{
    public string CustNbr;
    public string Name;
    public string Address;
    public string City;
    public string State;
    public string ZipCode;
    
    public CustAddressInfo()
	{
        

	}
}