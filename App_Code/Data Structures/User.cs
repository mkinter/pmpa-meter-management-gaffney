﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DuplicationResponse
/// </summary>
public class User
{
    public string username { get; set; }
    public string custNbr { get; set; }

	public User()
	{
		
	}
    public User(string custNbr, string username)
    {
        this.username = username;
        this.custNbr = custNbr;
    }
}