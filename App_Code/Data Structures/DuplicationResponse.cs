﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DuplicationResponse
/// </summary>
public class DuplicationResponse
{
    public string ID { get; set; }
    public string custNbr { get; set; }
    public bool success { get; set; }
    public string message { get; set; }
    public string transID { get; set; }
    public bool duplication { get; set; }
	public DuplicationResponse()
	{
		
	}
    public DuplicationResponse(string ID, string custNbr, bool success, string message, string transID, bool duplication)
    {
        this.ID = ID;
        this.custNbr = custNbr;
        this.success = success;
        this.message = message;
        this.transID = transID;
        this.duplication = duplication;
    }
}