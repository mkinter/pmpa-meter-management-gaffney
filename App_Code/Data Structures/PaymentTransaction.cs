﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PaymentTransaction
/// </summary>
/// 
[Serializable()]
public class PaymentTransaction
{
    public string amount { get; set; }
    public string locationID{get; set;}
    public string custNbr{get; set;}
    public string strTransactionID{get; set;}
    public int paymentRecordID { get; set; }

	public PaymentTransaction() {	}

    public PaymentTransaction(string amount ,string locationID, string custNbr,string strTransactionID = "")
    {
        this.amount = amount;
        this.locationID = locationID;
        this.custNbr = custNbr;
        this.strTransactionID = strTransactionID;
    }
}