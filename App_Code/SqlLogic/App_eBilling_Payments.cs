﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for App_eBilling_Payments
/// </summary>
public static class App_eBilling_Payments
{
    private static string connStr = CommonAccess.GetConnectionString("App_eBilling_Payments");

    /* Returns paypal transactionID if exists for this user and amount within past 15 minutes. This is our current solution for duplicate payment entries */
    public static string DuplicationEntryCheck(string username, float amount)
    {
        SqlParameter[] vars;
        string retVal = "";

        vars = new SqlParameter[] {
            new SqlParameter("@username", username),
            new SqlParameter("@amount", amount)
        };

        DataTable dt = CommonAccess.GetData(connStr, "DuplicateEntryCheck", vars);

        foreach (DataRow row in dt.Rows)
        {
            retVal = row["ID"].ToString();
        }

        return retVal;
    }
    public static Tuple<bool, string, List<PaymentTransaction>> InitializePaymentRecord(List<PaymentTransaction> trxList, string username)
    {
        SqlParameter[] vars;
        Tuple<bool, string> retVal = new Tuple<bool,string> (false, "Initialized");

        foreach (PaymentTransaction trx in trxList)
        {
            vars = new SqlParameter[] {
                new SqlParameter("@custNbr", trx.custNbr),
                new SqlParameter("@locationID", trx.locationID),
                new SqlParameter("@amount", trx.amount),
                new SqlParameter("@username", username)
            };

            try 
            { 
                int ID = CommonAccess.GetDataInteger(connStr, "InitializePaymentRecord", vars);
                trx.paymentRecordID = ID;
            }
            catch (Exception ex)
            {
                ErrHandler.WriteError(string.Format("Username: {0}. Error: {1}", username, ex.Message));
                return new Tuple<bool, string, List<PaymentTransaction>>(false, "Online payment service is temporarily unavailable. Please try again later.", null);
            }
        }


        return new Tuple<bool, string, List<PaymentTransaction>>(true, "", trxList);
    }
	public static void CompletePayment(List<PaymentTransaction> trxList, string paypalTrxID, string username)
    {
        SqlParameter[] vars;
        foreach (PaymentTransaction trx in trxList)
        {
            vars = new SqlParameter[] {
                new SqlParameter("@paymentRecordID", trx.paymentRecordID),
                new SqlParameter("@paypalTransactionID", paypalTrxID),
                new SqlParameter("@status", "2")
            };

            try
            {
                CommonAccess.ExecuteNonQuery(connStr, "UpdatePaymentRecord", vars);
            }
            catch (Exception ex)
            {
                ErrHandler.WritePaymentFail(trx);
                ErrHandler.WriteError(string.Format("Username: {0}. Error: {1}", username, ex.Message));
            }
        }
    }
}