﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Live.ServerControls.VE;
using System.Data;

/// <summary>
/// Summary description for App_OutageManagementSystem
/// </summary>
public class App_OutageManagementSystem
{
    private static string connectionString = CommonAccess.GetConnectionString("dbOMS");
   
    public static List<MapOutage> GetRecentOutages()
    {
        List<MapOutage> retVal = new List<MapOutage>();
        DataTable dt = new DataTable();
        dt = CommonAccess.GetData(connectionString, "spRecentOutages");
        foreach (DataRow row in dt.Rows)
        {
            double latitude, longitude;
            string outageType = row["OutageType"].ToString();
            if (double.TryParse(row["Latitude"].ToString(), out latitude) && double.TryParse(row["longitude"].ToString(), out longitude))
            {
                retVal.Add(new MapOutage(latitude, longitude, outageType));
            }
            
        }
        return retVal;
    }
}