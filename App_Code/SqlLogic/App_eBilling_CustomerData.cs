﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Utilities.Logging;
/// <summary>
/// Summary description for App_eBilling_Customer_Data
/// </summary>
public class App_eBilling_Customer_Data
{
    private static string connectionString = CommonAccess.GetConnectionString("dbCustomerInfo");

    public static int VerifyAccountId(string accountId)
    {
        SqlParameter[] vars = new SqlParameter[]{
            new SqlParameter("@accountId", accountId)
        };

        return CommonAccess.GetDataInteger(connectionString, "MyAccount.spValidateAccountId", vars);
    }

    public static int VerifyAccountByMeterNumber(string meterNumber)
    {
        SqlParameter[] vars = new SqlParameter[]{
            new SqlParameter("@meterNum", meterNumber)
        };

        return CommonAccess.GetDataInteger(connectionString, "MyAccount.spValidateUserWithMeterNumber", vars);
    }

    public static bool IsWebEnabled(string accountId)
    {
        SqlParameter[] vars = new SqlParameter[]{
            new SqlParameter("@accountId", accountId)
        };

        return CommonAccess.GetDataBoolean(connectionString, "MyAccount.spIsWebEnabled", vars);
    }

    public static void setEbillFlag(bool enabled, string custnbr)
    {
        SqlParameter[] vars = new SqlParameter[]{
            new SqlParameter("@enabled", enabled ? "1" : "0"),
            new SqlParameter("@custNbr", custnbr)
        };

        //CommonAccess.ExecuteNonQuery(connectionString, "sp_SetEbillFlag", vars);
        return;
    }
    public static bool checkPasswordHash(string username, string hash)
    {
        bool retVal = false;
        SqlParameter[] vars = new SqlParameter[] {
            new SqlParameter("@username", username),
            new SqlParameter("@digest", hash)
        };

        DataTable dt = CommonAccess.GetData(connectionString, "PasswordRecovery.sp_CheckHash", vars);

        foreach (DataRow row in dt.Rows)
        {
            retVal = row["Success"].ToString() == "1" ? true : false;
        }

        return retVal;
    }
    public static ChartData getChart(string locationID, string custNbr, string type)
    {
        ChartData retVal = new ChartData();
        if (type == "Electric")
        {
            SqlParameter[] vars = new SqlParameter[] {
                new SqlParameter("@LocationID", locationID),
                new SqlParameter("@CustomerID", custNbr)
            };

            DataTable dt = CommonAccess.GetData(connectionString, "sp_ChartElectric_V2", vars);
            foreach (DataRow row in dt.Rows)
            {
                
            }
            retVal.titleText = "Electric Usage";
        }

        return retVal;
    }
    public static bool hasUtilities(string locationID)
    {
        bool retVal = true;
        SqlParameter[] vars = new SqlParameter[1];
        vars[0] = new SqlParameter("@locationID", locationID);
        
        DataTable dt = CommonAccess.GetData(connectionString, "spHasUtilities", vars);
        foreach (DataRow row in dt.Rows)
        {
            if (row["hasUtilities"].ToString() == "0")
                retVal = false;
            
        }
        return retVal;
    }

    public static string GetEmail(string custNbr)
    {
        string retVal = "";
        SqlParameter[] vars = new SqlParameter[1];
        vars[0] = new SqlParameter("@CustNbr", custNbr);

        DataTable dt = CommonAccess.GetData(connectionString, "sp_GetEmail", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal = row["Email"].ToString();
        }
        return retVal;
    }

    public static int getNewPaymentLogID()
    {
        SqlParameter[] vars = new SqlParameter[] { };
        DataTable dt = CommonAccess.GetData(connectionString, "PayBill.getNewID", vars);
        int retVal = -1;
        foreach (DataRow row in dt.Rows)
        {
            retVal = Int32.Parse(row["ID"].ToString());
        }
        return retVal;
    }
    public static void LogPaymentStep(int ID, string stage, string userID, string amount, string type, DateTime date)
    {
        /*
        SqlParameter[] vars = new SqlParameter[] {
            new SqlParameter("@ID", ID),
            new SqlParameter("@stage", stage),
            new SqlParameter("@userID", userID),
            new SqlParameter("@amount", amount),
            new SqlParameter("@type", type),
            new SqlParameter("@date", date)
        };
        CommonAccess.ExecuteNonQuery(connectionString, "PayBill.LogPaymentLog", vars);*/

        LogWriter.Write(string.Format("Payment: {0}. Amount: {1}.", stage, amount), userID);
    }
    #region Password Recovery DB methods
    public static PasswordRecoveryResponse validateRecoveryToken(string tokenID)
    {
        PasswordRecoveryResponse retVal = new PasswordRecoveryResponse();
        SqlParameter[] vars = new SqlParameter[] {new SqlParameter("tokenID", tokenID)};
        DataTable dt = CommonAccess.GetData(connectionString, "PasswordRecovery.sp_ValidateRecoveryToken", vars);

        foreach (DataRow row in dt.Rows)
        {
            retVal.success = row["Success"].ToString() == "1" ? true : false;
            retVal.username = row["username"].ToString();
            retVal.P_ID = row["P_ID"].ToString();
        }

        return retVal;
    }

    public static bool AddToken(string username, string tokenID)
    {
        SqlParameter[] vars = new SqlParameter[] {new SqlParameter("username", username), new SqlParameter("token", tokenID)};
        try
        {
            CommonAccess.ExecuteNonQuery(connectionString, "PasswordRecovery.sp_AddToken", vars);
            return true;
        }
        catch (Exception ex)
        {
            LogWriter.Write(string.Format("sp_AddToken failed. {0}", ex.Message));
            return false;
        }

    }
    public static void VoidToken(string P_ID)
    {
        SqlParameter[] vars = new SqlParameter[] { new SqlParameter("ID", P_ID) };

        try
        {
            CommonAccess.ExecuteNonQuery(connectionString, "PasswordRecovery.sp_VoidToken", vars);
        }
        catch (Exception ex)
        {
            LogWriter.Write(string.Format("sp_VoidToken failed. {0}", ex.Message));
        }

    }

    public static bool IsValidateGUID(string guid)
    {
        bool retVal = false;
        SqlParameter[] vars = new SqlParameter[] { new SqlParameter("ID", guid) };
        DataTable dt = CommonAccess.GetData(connectionString, "PasswordRecovery.sp_CheckGUID", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal = row["Valid"].ToString() == "1" ? true : false;
        }
        return retVal;
    }
    #endregion

    #region bill pay GUID session key generation
    public static string GenerateSessionKey()
    {
        string retVal = "";
        DataTable dt = CommonAccess.GetData(connectionString, "spGenerateSubmissionGUID");
        foreach (DataRow row in dt.Rows)
        {
            retVal = row["GUID"].ToString();
        }
        return retVal;
    }
    #endregion

    #region Paypal
    // TODO: DELETE
    public static int CreatePaypalHeader(string custNbr)
    {
        int retVal = -1;
        SqlParameter[] vars = new SqlParameter[] { 
            new SqlParameter("custnbr", custNbr), 
            new SqlParameter("DTSubmitted", DateTime.Now) };
        DataTable dt = CommonAccess.GetData(connectionString, "Paypal.sp_PaypalHeader", vars);
        foreach (DataRow row in dt.Rows)
        {
            int.TryParse(row["ID"].ToString(), out retVal);
        }

        return retVal;
    }

    public static DuplicationResponse IsPaymentDuplication(string custNbr)
    {
        DuplicationResponse retVal = new DuplicationResponse();
        SqlParameter[] vars = new SqlParameter[] { 
            new SqlParameter("custnbr", custNbr), 
            new SqlParameter("DT", DateTime.Now) };

        DataTable dt = CommonAccess.GetData(connectionString, "Paypal.sp_CheckDuplication", vars);

        foreach(DataRow curr in dt.Rows)
        {
            /*
            bool duplication;
            duplication = curr["Duplication"].ToString() == "1" ? true : false;

            retVal.duplication = duplication;*/
            retVal = new DuplicationResponse(curr["ID"].ToString(), curr["custNbr"].ToString(),
                curr["Success"].ToString() == "1" ? true : false, curr["Message"].ToString(),
                curr["TransactionID"].ToString(), curr["Duplication"].ToString() == "1" ? true : false);
        }

        return retVal;
    }

    // TODO: Delete
    public static void UpdatePaypalHeader(string transID, string msg, bool success, int ID)
    {
        SqlParameter[] vars = new SqlParameter[] { 
            new SqlParameter("transactionID", transID), 
            new SqlParameter("message", msg), 
            new SqlParameter("success", success ? "1" : "0"), 
            new SqlParameter("ID", ID) };
        
        CommonAccess.ExecuteNonQuery(connectionString, "Paypal.sp_UpdateHeader", vars);
    }

    public static void AddSqlPayment(decimal amt, string locID, string custNbr, string transID, int ID)
    {
        SqlParameter[] vars = new SqlParameter[] { 
            new SqlParameter("amt", amt), 
            new SqlParameter("locID", locID), 
            new SqlParameter("custNbr", custNbr), 
            new SqlParameter("transID", transID), 
            new SqlParameter("ID", ID) };

        CommonAccess.ExecuteNonQuery(connectionString, "Paypal.sp_AddSQLPayment", vars);
    }

    public static List<PaymentTransaction> getPayments(string ID)
    {
        List<PaymentTransaction> retVal = new List<PaymentTransaction>();
        SqlParameter[] vars = new SqlParameter[] { 
            new SqlParameter("ID", ID) };

        DataTable dt = CommonAccess.GetData(connectionString, "Paypal.sp_GetPayments", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal.Add(new PaymentTransaction(row["Amount"].ToString(), row["locationID"].ToString(), row["custnbr"].ToString(),
                row["transactionid"].ToString()));
        }
        return retVal;
    }
    #endregion


    public static Coordinate getCustCoordinate(string custNbr)
    {
        DataTable dt = new DataTable();
        Coordinate retVal = new Coordinate();
        SqlParameter[] vars = { new SqlParameter("CustNbr", custNbr) };
        dt = CommonAccess.GetData(connectionString, "sp_GetCustCoordinates", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal.latitude = double.Parse(row["Latitude"].ToString());
            retVal.longitude = double.Parse(row["Longitude"].ToString());
        }

        return retVal;
    }

    public static List<User> getUsernames(string email)
    {
        List<User> retVal = new List<User>();
        SqlParameter[] vars = new SqlParameter[] {new SqlParameter("email", email)};
        DataTable dt = CommonAccess.GetData(connectionString, "PasswordRecovery.sp_GetUsernames", vars);
        foreach (DataRow row in dt.Rows)
        {
            User toAdd = new User();
            toAdd.custNbr = row["CustNbr"].ToString();
            toAdd.username = row["UserName"].ToString();
            retVal.Add(toAdd);
        }

        return retVal;
    }
}