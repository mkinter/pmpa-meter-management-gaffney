﻿/*
using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using System.Web.Profile;
*/
/// <summary>
/// Summary description for App_eBilling
/// </summary>
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System;
using System.Collections.Generic;
using ClearTextPasswordImport.Data_Structures;

public class App_eBilling
{
    private static string connectionString = CommonAccess.GetConnectionString("dbMyCMSConnectionString");
    private static MembershipProvider provider = Membership.Providers["dbSqlMemberShipProviderAdmin"];
    private static string wsatDefaultRole = "Member";
    private static string wsatAdmin = "administrator";

    #region default admin variables
    private static string admin_username = "Admin";
    private static string admin_password = "Pa$$w0rd";
    private static string admin_email = "mlaw@eucmail.com";
    #endregion 

    public static void ReimportUsers()
    {
        DeleteAllUsers();

        DataTable users = CommonAccess.GetData(connectionString, "sp_Reimport_GetAccountBase");
        string username, password, email, custnbr, name;
        foreach (DataRow currUser in users.Rows)
        {
            username = currUser["Username"].ToString().Trim();
            password = currUser["Password"].ToString().Trim();
            email = username;
            custnbr = currUser["CUSTNMBR"].ToString().Trim();
            name = currUser["Name"].ToString().Trim();

            AddUser(username, password, email, custnbr, name);
        }

        AddAdmin(admin_username, admin_password, admin_email, "999999", "Admin");
    }
    public static void AddAdmin(string username, string password, string email, string custNbr, string name)
    {
        try
        {
            System.Web.Security.MembershipUser user = Membership.CreateUser(username, password, email);
        }
        catch (Exception ex)
        {
            return;
        }

        // add newly created user to default Role specified above
        if (Roles.RoleExists(wsatAdmin))
        {

            // Add the newly created user to the default Role.

            Roles.AddUserToRole(username, wsatAdmin);

            // Create an empty Profile for the newly created user
            ProfileCommon p = (ProfileCommon)ProfileCommon.Create(username, true);
            p.CustomerDetails.CustNbr = custNbr;

            p.CustomerDetails.CustName = name;
            p.Preferences.eBilling = "NotSet";
            // Save profile - must be done since we explicitly created it
            p.Save();

        }
    }
    private static void AddUser(string username, string password, string email, string custNbr, string name)
    {
       
        try
        {
            System.Web.Security.MembershipUser user = Membership.CreateUser(username, password, email);
        }
        catch (Exception ex)
        {
            return;
        }
        // add newly created user to default Role specified above
        if (Roles.RoleExists(wsatDefaultRole))
        {

            // Add the newly created user to the default Role.

            Roles.AddUserToRole(username, wsatDefaultRole);

            // Create an empty Profile for the newly created user
            ProfileCommon p = (ProfileCommon)ProfileCommon.Create(username, true);
            p.CustomerDetails.CustNbr = custNbr;

            p.CustomerDetails.CustName = name; 
            p.Preferences.eBilling = "NotSet";
            // Save profile - must be done since we explicitly created it
            p.Save();

        }
    }
    public static void DeleteAllUsers()
    {
        CommonAccess.ExecuteNonQuery(connectionString, "sp_Reimport_DeleteAllUsers");
    }

    
    public static string GetMailingAddress(string custNbr)
    {
        string retVal = "";
        SqlParameter[] vars = new SqlParameter[1];
        vars[0] = new SqlParameter("@CustNbr", custNbr);

        DataTable dt = CommonAccess.GetData(connectionString, "sp_GetCustInfo", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal = row["Address"].ToString();
        }
        return retVal;
    }
    
    public static CustAddressInfo getAddressInfo(string custNbr)
    {
        CustAddressInfo retVal = new CustAddressInfo();
        SqlParameter[] vars = new SqlParameter[1];
        vars[0] = new SqlParameter("@CustNbr", custNbr);

        DataTable dt = CommonAccess.GetData(connectionString, "sp_GetCustInfo", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal.CustNbr = custNbr;
            retVal.Name = row["Name"].ToString();
            retVal.Address = row["AddressLine1"].ToString();
            retVal.City = row["City"].ToString();
            retVal.State = row["State"].ToString();
            retVal.ZipCode = row["ZipCode"].ToString();
        }
        return retVal;
    }
    public static string GetEmail(string custNbr)
    {
        string retVal = "";
        SqlParameter[] vars = new SqlParameter[1];
        vars[0] = new SqlParameter("@CustNbr", custNbr);

        DataTable dt = CommonAccess.GetData(connectionString, "sp_GetEmail", vars);
        foreach (DataRow row in dt.Rows)
        {
            retVal = row["Email"].ToString();
        }
        return retVal;
    }

    public static List<User2> GetReImportUsers()
    {
        DataTable retVal = null;
        retVal = CommonAccess.GetData(connectionString, "sp_getReimportUsers");
        List<User2> users = new List<User2>();
        foreach (DataRow row in retVal.Rows)
        {
            string username, password;
            username = row["Username"].ToString();
            password = row["Password"].ToString();

            users.Add(new User2(username, password, row["CUSTNMBR"].ToString()));
        }
        return users;
    }
}