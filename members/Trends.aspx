﻿<%@ Page EnableEventValidation="false" Title="Trends" Language="C#" MasterPageFile="~/members/themes/default/default.master" AutoEventWireup="false" CodeFile="Trends.aspx.cs" Inherits="members_Trends" %>


<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" 
namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:Chart ID="chtElectric" runat="server" DataSourceID="SQL_Electric" Width="500px">
    <Series>
        <asp:Series Name="ThisYear" Legend="Legend1" XValueMember="Month" 
            YValueMembers="ThisYear" Color="255, 128, 0">
        </asp:Series>
        <asp:Series ChartArea="ChartArea1" Legend="Legend1" Name="LastYear" 
            XValueMember="Month" YValueMembers="LastYear" Color="255, 192, 128">
        </asp:Series>
    </Series>
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
            <AxisX TextOrientation="Rotated90" Interval="1" IsLabelAutoFit="False">
                <LabelStyle Angle="-45" />
            </AxisX>
        </asp:ChartArea>
    </ChartAreas>
        <Legends>
            <asp:Legend Name="Legend1">
            </asp:Legend>
        </Legends>
        <Titles>
            <asp:Title Name="Title1" Text="Electric Usage">
            </asp:Title>
        </Titles>
</asp:Chart>
    <asp:SqlDataSource ID="SQL_Electric" runat="server" 
        ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" 
        SelectCommand="sp_ChartElectric_V2" SelectCommandType="StoredProcedure" 
        OnSelected="SQL_Electric_Selected" >
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="LocationID" 
                SessionField="LocationID" Type="String" />
                
            <asp:ProfileParameter Name="CustomerID" PropertyName="CustomerDetails.CustNbr" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <asp:Chart ID="chtGas" runat="server" DataSourceID="SQL_Gas" Width="500px">
        <series>
            <asp:Series Name="ThisYear" Legend="Legend1" XValueMember="Month" 
                YValueMembers="ThisYear" Color="255, 128, 0">
            </asp:Series>
            <asp:Series ChartArea="ChartArea1" Legend="Legend1" Name="LastYear" 
                XValueMember="Month" YValueMembers="LastYear" Color="255, 192, 128">
            </asp:Series>
        </series>
        <chartareas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Interval="1" IsLabelAutoFit="False">
                    <LabelStyle Angle="-45" />
                </AxisX>
            </asp:ChartArea>
        </chartareas>
        <Legends>
            <asp:Legend Name="Legend1">
            </asp:Legend>
        </Legends>
        <Titles>
            <asp:Title Name="Title1" Text="Gas Usage">
            </asp:Title>
        </Titles>
    </asp:Chart>
    <asp:SqlDataSource ID="SQL_Gas" runat="server" 
        ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" 
        SelectCommand="sp_ChartGas_V2" SelectCommandType="StoredProcedure" 
        OnSelected="SQL_Gas_Selected">
        <SelectParameters>
            <asp:SessionParameter Name="LocationID" SessionField="LocationID" 
                Type="String" />
                <asp:ProfileParameter Name="CustomerID" PropertyName="CustomerDetails.CustNbr" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <asp:Chart ID="chtWater" runat="server" DataSourceID="SQL_Water"  Width="500px">
        <series>
            <asp:Series Name="ThisYear" Legend="Legend1" XValueMember="Month" 
                YValueMembers="ThisYear" Color="255, 128, 0">
            </asp:Series>
            <asp:Series ChartArea="ChartArea1" Legend="Legend1" Name="LastYear" 
                XValueMember="Month" YValueMembers="LastYear" Color="255, 192, 128">
            </asp:Series>
        </series>
        <chartareas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Interval="1" IsLabelAutoFit="False">
                    <LabelStyle Angle="-45" />
                </AxisX>
            </asp:ChartArea>
        </chartareas>
        <Legends>
            <asp:Legend Name="Legend1">
            </asp:Legend>
        </Legends>
        <Titles>
            <asp:Title Name="Water" Text="Water Usage">
            </asp:Title>
        </Titles>
    </asp:Chart>
    <asp:SqlDataSource ID="SQL_Water" runat="server" 
        ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" 
        SelectCommand="sp_ChartWater_V2" SelectCommandType="StoredProcedure" OnSelected="SQL_Water_Selected">
        <SelectParameters>
            <asp:SessionParameter Name="LocationID" SessionField="LocationID" 
                Type="String" />
                <asp:ProfileParameter Name="CustomerID" PropertyName="CustomerDetails.CustNbr" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    </asp:Content>
