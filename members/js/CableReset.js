﻿
//var webservicePrefix = '~/members/Cablebridge/WebService.asmx';
//var ResetURL = ResolveUrl(webservicePrefix + '/Reset');
//var StatusCheckURL = ResolveUrl(webservicePrefix + '/StatusCheck');
//var queueData;
//var statusCheckAttempts = 0;

//$(document).ready(function () {
//    $('.popup-with-form').magnificPopup({
//        type: 'inline',
//        preloader: false,
//        focus: '#name',

//        // When elemened is focused, some mobile browsers in some cases zoom in
//        // It looks not nice, so we disable it:
//        callbacks: {
//            beforeOpen: function () {
//                if ($(window).width() < 700) {
//                    this.st.focus = false;
//                } else {
//                    this.st.focus = '#name';
//                }
//                $('#lbl_Status').text("Initializing...");
//                $('#lbl_Description').text("");
//                $('#btn_ResetOK').hide();
//                SubmitReset();
//            }
//        }
//    });
//});

//function isStepActive(elementID) {
//    var classes = $('#' + elementID.toString()).attr('class' || '').split(' ');
//    var hasActive = false;
//    for (var index = 0; index < classes.length; index++) {
//        if (classes[index] == "step-active")
//            return true;
//    }

//    return false;
//}
//function SubmitReset() {
//    var tmpInput = $("#hfLocationReset").val();
//    $('#img_loading').show();
//    $("#reset_Step1").attr("class", "step");
//    $("#reset_Step2").attr("class", "step");
//    $("#reset_Step3").attr("class", "step");
//    $.ajax({
//        type: "GET",
//        url: ResetURL,
//        data: {
//            input: tmpInput
//        },
//        dataType: "html",
//        error: function (data) {
//            var tmp = data;
//        },
//        success: function (data) {
//            var xmlDoc = $.parseXML(data);
//            var parsedData = {};

//            $(xmlDoc).find("SendAccountResetRootObject").each(function () {
//                $(this).find("*").each(function () {
//                    parsedData[this.nodeName] = $(this).text();
//                })
//            });

//            if (parsedData.ResponseCode !== "1") {
//                if (typeof (parsedData.ResponseString) == 'undefined')
//                {
//                    $("#lbl_Status").text("Cable reset service is down. Please contact Customer Service");

//                    if (!isStepActive('reset_Step1'))
//                        $("#reset_Step1").addClass("step-failed");
//                    if (!isStepActive('reset_Step2'))
//                        $("#reset_Step2").addClass("step-failed");
//                    if (!isStepActive('reset_Step3'))
//                        $("#reset_Step3").addClass("step-failed");
                    

//                }
//                else
//                    $("#lbl_Status").text(parsedData.ResponseString);

//                $("#lbl_Status").css("color", 'red');
//                $('#img_loading').hide();
//            }
//            else {
//                $("#lbl_Status").css("color", 'orange');
//                queueData = parsedData;
//                StatusCheck();
//            }

//        }
//    });
//}


//function StatusCheck() {
//    var queueID = queueData.QueueId;
//    var username = $("#hfUsername").val();

//    statusCheckAttempts++;

//    if (statusCheckAttempts >= 360) {
//        $("#lbl_Status").text("Cable reset service timed out. Please contact Customer Service");
//        $("#lbl_Status").css("color", 'red');

//        if (!isStepActive('reset_Step1'))
//            $("#reset_Step1").addClass("step-failed");
//        if (!isStepActive('reset_Step2'))
//            $("#reset_Step2").addClass("step-failed");
//        if (!isStepActive('reset_Step3'))
//            $("#reset_Step3").addClass("step-failed");

//        $('#img_loading').hide();

//        return;

//    }

//    $.ajax({
//        type: "GET",
//        url: StatusCheckURL,
//        data: {
//            queueID: queueID,
//            username: username
//        },
//        dataType: "html",
//        error: function (data) {
//            var tmp = data;
//            setTimeout(StatusCheck, 5000);
//        },
//        success: function (data) {
//            var xmlDoc = $.parseXML(data);
//            var parsedData = {};

//            $(xmlDoc).find("StatusCheckRootObject").each(function () {
//                $(this).find("*").each(function () {
//                    parsedData[this.nodeName] = $(this).text();
//                })
//            });

//            if (parsedData.ResponseCode !== "1") {

//                $("#lbl_Status").text("Cable reset service is down. Please contact Customer Service");
//                $("#lbl_Status").css("color", 'red');

//                if (!isStepActive('reset_Step1'))
//                    $("#reset_Step1").addClass("step-failed");
//                if (!isStepActive('reset_Step2'))
//                    $("#reset_Step2").addClass("step-failed");
//                if (!isStepActive('reset_Step3'))
//                    $("#reset_Step3").addClass("step-failed");

//                $('#img_loading').hide();

//                return;

//            }
//            $("#lbl_Status").text(parsedData.QueueStatusString);

//            $("#reset_Step1").attr("class", "step");
//            $("#reset_Step2").attr("class", "step");
//            $("#reset_Step3").attr("class", "step");

//            if (parsedData.QueueStatusCode.toString() == "1") {
//                setTimeout(StatusCheck, 5000);

//                $("#reset_Step1").addClass("step-active");
//            }
//            else if (parsedData.QueueStatusCode.toString() == "2") {
//                setTimeout(StatusCheck, 5000);

//                $("#reset_Step1").addClass("step-active");
//                $("#reset_Step2").addClass("step-active");
//            }
//            else if (parsedData.QueueStatusCode.toString() == "3") {
//                $("#reset_Step1").addClass("step-active");
//                $("#reset_Step2").addClass("step-active");
//                $("#reset_Step3").addClass("step-active");

//                $("#lbl_Status").css("color", 'green');
//                $('#img_loading').hide();
//                $('#lbl_Description').text(parsedData.UserMessage);
//                $('#btn_ResetOK').click(function () {
//                    $('.popup-with-form').magnificPopup('close');
//                });
//                $('#btn_ResetOK').show();
//            }
//            else if (parsedData.QueueStatusCode.toString() == "4") {
//                $("#lbl_Status").css("color", 'red');
//                $('#img_loading').hide();

//            }
//            else{
//                $("#lbl_Status").css("color", 'orange');
//            }
//        }
//    });
//}


//function ResolveUrl(url) {
//    return url.replace("~/", baseUrl)/*.replace("../", "")*/;
//}