﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities.Logging;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Samples.CS.DataObjects.BasicTransactions;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Security;

public partial class members_BillPayProcessing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["paypalTrxID"] != null)
        {
            //List<PaymentTransaction> list = (List<PaymentTransaction>)Session["Payments"];
            string name = Profile.CustomerDetails.CustName;
            int index = name.IndexOf(" ");
            string firstname;
            if (index == -1)
                firstname = name;
            else
            {
                firstname = name.Substring(0, index);
                firstname = firstname == null ? "" : name.Substring(0, index).ToLower();
            }
            lblFirstName.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(firstname);

            //LogWriter.Write(string.Format("BillPayProcessing routine completed. User: {0}", Profile.UserName));

            lblPmtAmtConfirm.Text = string.Format("Payment Amount : {0}", Session["paypalAmount"]);
            lblTransNumber.Text = Session["paypalTrxID"].ToString();
            tableConfirmation.Visible = true;
            //App_eBilling_Customer_Data.LogPaymentStep(Int32.Parse(Session["paymentLogID"].ToString()), "5 - Succeeded in redirecting user to confirmation page", Membership.GetUser().ProviderUserKey.ToString(),
            //        Session["amount"].ToString(), "CC Paypal", DateTime.Now);
        }
        else
        {
            lblError.Text = "An error occurred.";
            //LogWriter.Write(string.Format("BillPayProcessing.aspx error occured, entered with no session variable payments. User: {0}", Profile.UserName));
            //Response.Redirect("~/members/BillPay.aspx");
        }

        Session.Clear();
    }
    //private bool postPaymentToSql(string amt, string locationID, string customerID, string transID)
    //{
    //    SqlCommand cmd;
    //    SqlDataReader dr;
    //    bool success = true;

    //    /* Form batch ID */
    //    string szBatch = "";
    //    DateTime dt = DateTime.Now;
    //    if (dt.Month < 10)
    //        szBatch = szBatch + "0" + dt.Month.ToString();
    //    else
    //        szBatch = szBatch + dt.Month.ToString();

    //    if (dt.Day < 10)
    //        szBatch = szBatch + "0" + dt.Day.ToString();
    //    else
    //        szBatch = szBatch + dt.Day.ToString();

    //    szBatch = "WBPY" + szBatch + dt.Year.ToString();

    //    SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(GetConnString(true));
    //    csb.ConnectTimeout = 10;
    //    /* Check if batch exists (if not, create it) */
    //    using (SqlConnection conn = new SqlConnection(csb.ConnectionString))
    //    {
    //        cmd = new SqlCommand("select * from UM00100 where umBatchID = @ID", conn);
    //        //cmd = new SqlCommand("select * from UM00100 with (holdlock,tablockx) WHERE 0 = 1 WAITFOR DELAY '00:05'", conn);
    //        cmd.Parameters.AddWithValue("@ID", szBatch);

    //        try
    //        {
    //            conn.Open();
    //            dr = cmd.ExecuteReader();
    //            if (!dr.HasRows)
    //            {
    //                dr.Close();
    //                conn.Close();
    //                cmd = new SqlCommand("INSERT INTO UM00100 (umApproved, umApprovedDate, umApprovedUser, umBatchComment," +
    //                    " umBatchID, umBatchSource, umBatchStatus, BACHFREQ, umBatchTotal, umControlTotal, umCtrlNum, " +
    //                    "umDateCreated, umDateModified, umDatePosted, umMarked, umNumTrans, umPostUserID, umGLPOST, " +
    //                    "umRecurrPost, umRecurrLastDate, umNumPostings, umMiscBatchDays, umCurncy, umChkBook, NOTEINDX) " +
    //                    " SELECT '', '01-01-1900', '', '', @ID, 'PAYMENTS', 0, 1, 0.00, 0.00, 0, '01-01-1900'," +
    //                    " '01-01-1900', '01-01-1900', 0, 0, '', CONVERT(CHAR(10),GETDATE(),101), 0, '01-01-1900', 0, 0," +
    //                    " '', CHEKBKID, 0 FROM UM40000 WHERE umSetupKey = 1", conn);
    //                cmd.Parameters.AddWithValue("@ID", szBatch);
    //                cmd.CommandType = System.Data.CommandType.Text;
    //                try
    //                {
    //                    conn.Open();
    //                    cmd.ExecuteNonQuery();
    //                }
    //                catch (Exception ex)
    //                {
    //                    lblError.Text = string.Format("Error processing payment. Please contact customer support with your transaction ID: {0}",
    //                        transID);
    //                    success = false;
    //                }
    //                finally
    //                {
    //                    conn.Close();
    //                }
    //            }
    //            else
    //            {
    //                //dr.Close();
    //                //conn.Close();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            lblError.Text = string.Format("Error processing payment. Please contact customer support with your transaction ID: {0}",
    //                        transID);
    //            success = false;
    //        }
    //        finally
    //        {
    //            conn.Close();
    //        }

    //    }
    //    /* Post payment with customer details */
    //    string paymentNbr = "";
    //    using (SqlConnection conn = new SqlConnection(GetConnString(true)))
    //    {
    //        cmd = new SqlCommand("dbo.UM_spWrkPymtAdd", conn);
    //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

    //        cmd.Parameters.AddWithValue("@INBatchID", szBatch);
    //        cmd.Parameters.AddWithValue("@INLocationID", locationID);
    //        cmd.Parameters.AddWithValue("@INPaymentAmount", amt);
    //        cmd.Parameters.AddWithValue("@INCustomerNumber", customerID);
    //        //cmd.Parameters.AddWithValue("@INPaymentNumber", "");
    //        SqlParameter param = new SqlParameter("@INPaymentNumber", paymentNbr);
    //        param.SqlDbType = SqlDbType.Char;
    //        param.Size = 15;
    //        param.Direction = ParameterDirection.InputOutput;
    //        cmd.Parameters.Add(param);

    //        cmd.Parameters.AddWithValue("@InStatDocNo", "");
    //        cmd.Parameters.AddWithValue("@INSortFilter", 1);
    //        cmd.Parameters.AddWithValue("@INPaymentDate", DateTime.Now.ToString());
    //        cmd.Parameters.AddWithValue("@INPaymentType", 2);
    //        cmd.Parameters.AddWithValue("@INDDLPymtOther", 1);
    //        cmd.Parameters.AddWithValue("@INComment", string.Format("WEB PYMT - AUTH # {0}", transID));
    //        cmd.Parameters.AddWithValue("@INChequeNumber", "");
    //        cmd.Parameters.AddWithValue("@INExtraControl", 0);

    //        try
    //        {
    //            conn.Open();
    //            string result = cmd.ExecuteNonQuery().ToString();
    //            string paymentNumber = cmd.Parameters["@INPaymentNumber"].Value.ToString();

    //            //lblTransNumber.Text = paymentNumber;
    //        }
    //        catch (Exception ex)
    //        {
    //            lblError.Text = string.Format("Error processing payment. Please contact customer support with your transaction ID: {0}",
    //                        transID);
    //            success = false;
    //        }
    //        finally
    //        {
    //            conn.Close();
    //        }

    //    }
    //    /* Update Batch Total */
    //    using (SqlConnection conn = new SqlConnection(GetConnString(true)))
    //    {
    //        cmd = new SqlCommand("UPDATE UM00100 SET umBatchTotal = ISNULL( (SELECT SUM(umPaymentAmount) FROM UM10800 WHERE" +
    //                " umBatchID = @ID AND umNumErrors = 0),0.00), umNumTrans = ISNULL((SELECT COUNT(*) FROM " +
    //                "UM10800 WHERE umBatchID = @ID) ,0) WHERE umBatchID = @ID", conn);
    //        cmd.CommandType = System.Data.CommandType.Text;

    //        cmd.Parameters.AddWithValue("@ID", szBatch);

    //        try
    //        {
    //            conn.Open();
    //            cmd.ExecuteNonQuery();
    //        }
    //        catch (Exception ex)
    //        {
    //            lblError.Text = string.Format("Error processing payment. Please contact customer support with your transaction ID: {0}",
    //                        transID);
    //            success = false;
    //        }
    //        finally
    //        {
    //            conn.Close();
    //        }
    //    }
    //    return success;
    //}
    private string GetConnString(bool gp)
    {
        if (Credentials.debug)
        {
            return ConfigurationManager.ConnectionStrings[gp ? "dbGPTest" : "dbMyCMSConnectionString"].ConnectionString;
        }
        else
            return ConfigurationManager.ConnectionStrings[gp ? "dbGP" : "dbMyCMSConnectionString"].ConnectionString;
    }
}