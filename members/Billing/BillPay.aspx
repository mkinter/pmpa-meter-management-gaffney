﻿<%@ Page EnableEventValidation="false" Language="C#" AutoEventWireup="true" CodeFile="BillPay.aspx.cs" MasterPageFile="~/members/themes/default/default.master" Inherits="members_BillPay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Pay Bill</title>
  
    <!-- <script src="../js/js/dw_tooltip_c.js" type="text/javascript"></script> -->
    <script type="text/javascript">
        $(function () {
            $("#<%=tbCC1.ClientID %>").keyup(function () {
                GetCreditCardTypeByNumber($(this).val());
                if ($(this).val().length == 4) {
                    $("#<%=tbCC2.ClientID %>").focus();
                }
            });
            $("#<%=tbCC2.ClientID %>").keyup(function () {
                if ($(this).val().length == 4) {
                    $("#<%=tbCC3.ClientID %>").focus();
                }
            });
            $("#<%=tbCC3.ClientID %>").keyup(function () {
                if ($(this).val().length == 4) {
                    $("#<%=tbCC4.ClientID %>").focus();
                }
            });
        });
        function ParseAndGetCardType(field) {
        /*
            var cc = field.value;
            var type = GetCreditCardTypeByNumber(cc);
            field2.value = type;*/
            GetCreditCardTypeByNumber(field.value);
            
            return;
        }
        function GetCreditCardTypeByNumber(ccnumber) {
            var cc = (ccnumber + '').replace(/\s/g, ''); //remove space
            var mc = document.getElementById('<%=imgMC.ClientID %>');
            var visa = document.getElementById('<%=imgVisa.ClientID %>');
            if (cc.length > 16) {
                mc.src = "themes/default/images/mcgray.png";
                visa.src = "themes/default/images/visagray.png";
                return '?'; //unknow type
            }
            else if ((/^(51|52|53|54|55)/).test(cc)){
                mc.src = "themes/default/images/mastercard.png";
                visa.src = "themes/default/images/visagray.png";
                return 'MasterCard'; //MasterCard beigins with 51-55, and length is 16.
            } else if ((/^(4)/).test(cc)) {
                visa.src = "themes/default/images/visa.png";
                mc.src = "themes/default/images/mcgray.png";
                return 'Visa'; //VISA begins with 4, and length is 13 or 16.
            } else{
                mc.src = "themes/default/images/mcgray.png";
                visa.src = "themes/default/images/visagray.png";
                return '?'; //unknow type
            }
        }

    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>Payment Center</h2>
            <asp:Label ID="lblTest" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Panel ID="pnlDataEntry" runat="server">
            
            <asp:DetailsView ID="dvCustomerInfo" runat="server" Height="50px" 
                AutoGenerateRows="False" DataSourceID="sdsCustInfo" CellPadding="5" 
                GridLines="None" >
                <Fields>
                    <asp:BoundField DataField="Name" HeaderText="Customer Name" ReadOnly="True" 
                        SortExpression="Name" >
                        <ItemStyle Width="405px" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CustNbr" HeaderText="Customer Number" ReadOnly="True" 
                        SortExpression="CustNbr" >
                        <ItemStyle Width="405px" Font-Bold="True" />
                            
                        </asp:BoundField>
                    <asp:BoundField DataField="Address" HeaderText="Mailing Address" ReadOnly="True" 
                        SortExpression="Address" >
                        <ItemStyle Width="405px" Font-Bold="True" />
                         </asp:BoundField>
                </Fields>
            </asp:DetailsView>
        <asp:panel runat="server" BorderStyle="Solid" BorderWidth="1px" CssClass="panelPaymentS1">
        <h4>STEP 1: Payment Amount</h4>
            <asp:GridView ID="gvBills" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="LocationID" DataSourceID="sdsBills" CellPadding="5" 
                BorderStyle="None" ShowFooter="True" ondatabound="gvBills_DataBound" width="100%">
                <Columns>
                    <asp:BoundField DataField="LocationID" HeaderText="Location ID" 
                        ReadOnly="True" SortExpression="LocationID" >
                        <FooterStyle BorderStyle="None" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FullAddressLine1" HeaderText="Service Address" ReadOnly="True" 
                        SortExpression="FullAddressLine1" >
                        <FooterStyle BorderStyle="None" />
                        <ItemStyle Width="300px" />
                    </asp:BoundField>

                    <asp:TemplateField HeaderText="Balance" SortExpression="AccountBalance">
                        <ItemTemplate>
                            <asp:Label ID="lblBalance" runat="server" Text='<%# Bind("AccountBalance", "{0:C}") %>'></asp:Label>
                            
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label runat="server" ID="lblBalanceTotal"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Right" Font-Bold="True" />
                        <FooterStyle HorizontalAlign="Right" Font-Bold="True" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount" SortExpression="AccountBalance">
                        <ItemTemplate>
                            $
                            <asp:TextBox ID="tbAmount" runat="server" AutoPostBack="true" 
                                onfocus="try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}" 
                                OnTextChanged="CalculateAmount" Text='<%# Bind("DefaultPaymentAmount") %>' Width="75px" style="text-align: right;"></asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label runat="server" ID="lblAmountTotal"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" Font-Bold="True" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="imgError" runat="server" AlternateText="Invalid Input" 
                                ImageUrl="~/images/invalid.png" ToolTip="Invalid Input" Visible="false" />
                        </ItemTemplate>
                        <HeaderStyle BorderStyle="None" />
                        <ItemStyle BorderStyle="None" />
                        <FooterStyle BorderStyle="None" />
                        
                    </asp:TemplateField>
                   
                  
                </Columns>
            </asp:GridView>
            <br />
        </asp:panel>            
        <table runat="server" id="tableCC" width="100%">
            <tr>
                <td colspan="2"><h4>STEP 2: Payment Information</h4></td>
            </tr>
            
            <tr>
                <td> <asp:TextBox ID="amountTextBox" runat="server" Enabled="false" 
                        Visible="False" /></td>
                <td />
            </tr>
            <tr>
            
            <td align="right">
                    
                    <asp:Image ID="imgVisa"  runat="server"  ImageUrl="~/members/themes/default/images/visagray.png" ImageAlign="Right" />
                    </td>
                    <td width="35px">
                    <asp:Image ID="imgMC" runat="server" 
                        ImageUrl="~/members/themes/default/images/mcgray.png" ImageAlign="Right"/>
                </td>
                <td align="center" width="200px"> 
                
                <asp:TextBox ID="tbCC1" runat="server" MaxLength="4" Width="30px" AutoCompleteType="Disabled" >
                
                    </asp:TextBox> -
                    <asp:TextBox ID="tbCC2" runat="server" MaxLength="4" Width="30px" AutoCompleteType="Disabled"></asp:TextBox> -
                    <asp:TextBox ID="tbCC3" runat="server" MaxLength="4" Width="30px" AutoCompleteType="Disabled"></asp:TextBox> -
                    <asp:TextBox ID="tbCC4" runat="server" MaxLength="4" Width="30px" AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                
                <td align="center">
                    <asp:DropDownList ID="expdateDropDown" runat="server">
                    <asp:ListItem>01</asp:ListItem>
                    <asp:ListItem>02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="yearDropDown" runat="server" AppendDataBoundItems="True" />
                </td>
                <td align="center">
                    <asp:TextBox ID="CVVcodeTextBox" runat="server" MaxLength="3" Width="30px"/>
                </td>
            </tr>
            <tr style="height: 120px;">
            <td colspan="2" />
                <td align="center" valign="top">Card Number</td>
                
                <td align="center" valign="top">Expiration Date</td>
                <td align="center" valign="top" style="width: 175px;">3 Digit Security Code <br />
                     <div id="floatbar">
                        <a href="" onclick="make it float 10px under this yay">What is this?</a>
                        <div class="box">This is the 3-digit number on the back of your card<br />
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/members/themes/default/images/mini_cvv2.gif" /></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                   <asp:Panel ID="pnlAddress" runat="server">
                <h4>Credit Card Billing Address</h4>
                    <table>
                        <tr>
                            <td>Street:</td>
                            <td><asp:TextBox ID="tbStreet" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td><asp:TextBox ID="tbCity" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>State:</td>
                            <td><asp:TextBox ID="tbState" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                    
                    
                    
                </asp:Panel>
                </td>
                <td align="center"><br /> <asp:Button ID="submit" runat="server" Text="Make Payment" onclick="submit_Click" /> 
                <br />
                (Please Click Once)</td>
                  
            </tr>
        </table>
         <asp:Label ID="successLabel" runat="server" Text=""></asp:Label>
            
               
         </asp:Panel>
        <table style="margin-top: 15px;" width="700px" id="tableConfirmation" runat="server" visible="false">
            <tr>
                <td colspan="2" align="center"><h3>Thank you 
                    <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>, for your payment!</h3>
                    <h5>
                        <asp:Label ID="lblPmtAmtConfirm" runat="server" Text="Label"></asp:Label><br />Please keep your transaction number: <asp:Label ID="lblTransNumber" runat="server" Text="Label "></asp:Label>
                     on file for reference.</h5>
                    </td>
            </tr>
            
        </table>
       
            <br />
            <asp:Label ID="errLabel" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <asp:Label ID="errcodeLabel" runat="server" Text=""></asp:Label>
            <asp:SqlDataSource ID="sdsBills" runat="server" 
                ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" 
                SelectCommand="EXEC [PayBill].[spAccountBalance]
		@CustNbr" 
                onselecting="sdsBills_Selecting">
                <SelectParameters>
                    <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" 
                        Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sdsCustInfo" runat="server" 
                ConnectionString="<%$ ConnectionStrings:dbMyCMSConnectionString %>" 
                SelectCommand="sp_GetCustInfo" SelectCommandType="StoredProcedure" 
                onselecting="sdsCustInfo_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="CustNbr" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</form>
   
   
    
    

</asp:Content>