﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayPal.Payments.Samples.CS.DataObjects.BasicTransactions;
using ClearTextPasswordImport.Data_Structures;
using System.Web.Security;
using System.Web.Profile;
using System.Data.SqlClient;

public partial class members_Template : System.Web.UI.Page
{
    private static string custNbr;

    protected static string wsatDefaultRole = "Member";
    private static string connectionString = "Data Source=EUSVR42;Initial Catalog=App_eBilling;Integrated Security=True";

    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        custNbr = Profile.CustomerDetails.CustNbr;
        string encStr = Request.RawUrl;
        if (encStr.IndexOf("?") > -1)
        {
            encStr = encStr.Substring(encStr.IndexOf('?') + 1);
            QueryStringEncryption encObj = new QueryStringEncryption();
            encStr = encObj.Decrypt(encStr, "s3curity");
            Label1.Text = encStr;
        }*/
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        List<User2> users = App_eBilling.GetReImportUsers();
        foreach (User2 curr in users)
        {
            string username, email;
            email = username = curr.username;
            string password = curr.password;
            string custNbr = curr.custnbr;
            MembershipCreateStatus status = new MembershipCreateStatus();
            try
            {
                Membership.CreateUser(username, password, email, null, null, true, out status);
                // Add the newly created user to the default Role.
                Roles.AddUserToRole(username, wsatDefaultRole);

                // Create an empty Profile for the newly created user
                ProfileBase p = ProfileBase.Create(username, true);
                p.GetProfileGroup("CustomerDetails").SetPropertyValue("CustNbr", custNbr);
            
            //p.CustomerDetails.CustNbr = custNbr;
            /* Get Customer Name */
            SqlCommand cmd;
            string name = "";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd = new SqlCommand("SELECT custName FROM CACHED_Customers WHERE custNbr = @custNbr", conn);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@custNbr", custNbr);

                try
                {

                    conn.Open();
                    object retVal = cmd.ExecuteScalar();
                    name = retVal == null ? "Default" : retVal.ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            p.GetProfileGroup("CustomerDetails").SetPropertyValue("CustName", name);
            //p.CustomerDetails.CustName = name;
            //p.CustomerDetails.CustName = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custName")).Text; 
            p.GetProfileGroup("Preferences").SetPropertyValue("eBilling", "NotSet");
            //p.Preferences.eBilling = "NotSet";
            // Save profile - must be done since we explicitly created it
            p.Save();
            }
            catch (Exception ex)
            {

            }
        }
        
    }
}