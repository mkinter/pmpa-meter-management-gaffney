﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Samples.CS.DataObjects.BasicTransactions;
using System.Globalization;
using Utilities.Logging;
using System.Web.Security;

public partial class members_BillPay : System.Web.UI.Page
{
    #region Global Vars
    private string custNbr;
    private const string SCRIPT_DOFOCUS =
            @"window.setTimeout('DoFocus()', 1);
            function DoFocus()
            {
                try {
                    document.getElementById('REQUEST_LASTFOCUS').focus();
                    document.getElementById('REQUEST_LASTFOCUS').select();
                } catch (ex) {}
            }";
    public enum CreditCardTypeType
    {
        Visa,
        MasterCard,
        Discover,
        Amex,
        Switch,
        Solo
    }
    private const string cardRegex = "^(?:(?<Visa>4\\d{3})|" +
    "(?<MasterCard>5[1-5]\\d{2})|(?<Discover>6011)|(?<DinersClub>" +
    "(?:3[68]\\d{2})|(?:30[0-5]\\d))|(?<Amex>3[47]\\d{2}))([ -]?)" +
    "(?(DinersClub)(?:\\d{6}\\1\\d{4})|(?(Amex)(?:\\d{6}\\1\\d{5})" +
    "|(?:\\d{4}\\1\\d{4}\\1\\d{4})))$";
    #endregion
    protected string sessionID;
    protected void Page_Load(object sender, EventArgs e)
    {
        //locationID = "007832";
        //custNbr = "510018";
        if (!Page.IsPostBack)
        {
            /*
            string mail = App_eBilling.GetMailingAddress(Profile.CustomerDetails.CustNbr);
            string[] mailSeparate = mail.Split(',');

            if (mailSeparate.Length == 3)
            {
                tbStreet.Text = mailSeparate[0].Trim();
                tbCity.Text = mailSeparate[1].Trim();
                tbState.Text= mailSeparate[2].Trim();
            }
            else
                tbStreet.Text = mail;*/
            CustAddressInfo info = App_eBilling.getAddressInfo(Profile.CustomerDetails.CustNbr);
            tbStreet.Text = info.Address;
            tbCity.Text = info.City;
            tbState.Text = info.State;
            tbZip.Text = info.ZipCode;

            sessionID = App_eBilling_Customer_Data.GenerateSessionKey();
            Session["SubmitKey"] = sessionID;
            Session["Paypal"] = false;
            lblTest.Text = string.Format("SID:{0}. Paypal:{1}", sessionID, Session["Paypal"].ToString());
        }
        custNbr = Profile.CustomerDetails.CustNbr;
        string name = Profile.CustomerDetails.CustName;
        int index = name.IndexOf(" ");
        string firstname;
        if (index == -1)
            firstname = name;
        else
        {
            firstname = name.Substring(0, index);
            firstname = firstname == null ? "" : name.Substring(0, index).ToLower();
        }
        lblFirstName.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(firstname);
        //string jsFunction = "javascript:ParseAndGetCardType(" + ccnumberTextbox.ClientID + ");";
        //string jsFunction = "javascript:ParseAndGetCardType(" + tbCC1.ClientID + ");";
        /*
        ccnumberTextbox.Attributes.Add("onKeyDown", jsFunction);
        ccnumberTextbox.Attributes.Add("onKeyUp", jsFunction);*/

        if (!Page.IsPostBack)
        {
            populateYearDDL();
            CalculateAmount(null, null);
        }
        ScriptManager.RegisterStartupScript(
               this,
               typeof(Page),
               "ScriptDoFocus",
               SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
               true);
    }

    private void populateYearDDL()
    {
        int currYear = DateTime.Now.Year;
        yearDropDown.Items.Clear();
        for (int x = 0; x < 15; x++)
        {
            yearDropDown.Items.Add(new ListItem(currYear.ToString(), currYear.ToString()));
            currYear = currYear + 1;
        }
    }


    #region Submit To Paypal/SQL
    protected void submit_ClickZipAndState(object sender, EventArgs e)
    {
        Random rnd = new Random();
        float tmpAmount;
        float.TryParse(amountTextBox.Text.Replace("$", ""), out tmpAmount);

        Session["paypalTrxID"] = rnd.Next(10000, 50000);
        Session["paypalAmount"] = tmpAmount;
        Session["paymentLogID"] = rnd.Next(1, 1000);

        System.Threading.Thread.Sleep(3000);

        Response.Redirect("~/members/BillPayProcessing.aspx");

        //ProfileCommon profile = Profile;
        //LogWriter.Write(string.Format("Pay Bill: Submit button clicked for user: {0} <#{1}>", profile.UserName, profile.CustomerDetails.CustNbr.Trim()));

       
        //int paymentLogID = App_eBilling_Customer_Data.getNewPaymentLogID();

        //App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1 - Submit Button Clicked", Membership.GetUser().ProviderUserKey.ToString(),
        //    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //CreditCard cc = new CreditCard(tbCC1.Text.Trim() + tbCC2.Text.Trim() + tbCC3.Text.Trim() + tbCC4.Text.Trim(),
        //    expdateDropDown.Text + yearDropDown.Text.Substring(yearDropDown.Text.Length - 2));
        //cc.Cvv2 = CVVcodeTextBox.Text.Trim();


        ///* Duplicate Entry Check */
        //float tmpAmount;
        //if (!float.TryParse(amountTextBox.Text.Replace("$", ""), out tmpAmount))
        //{
        //    LogWriter.Write(string.Format("Amount Parse failed for amountTextBox with value '{0}' (User: {1} <#{2}>)", amountTextBox.Text.Replace("$", ""),
        //        profile.UserName, profile.CustomerDetails.CustNbr.Trim()));
        //}
        
        //string duplicateEntryTrxID = App_eBilling_Payments.DuplicationEntryCheck(profile.UserName, tmpAmount);
        //App_eBilling_Customer_Data.LogPaymentStep(paymentLogID,
        //    string.Format("1.01a - Duplicate Entry test, with parameters ({0}, {1}) Result {2}", profile.UserName, 
        //    amountTextBox.Text.Replace("$", ""), duplicateEntryTrxID), 
        //    Membership.GetUser().ProviderUserKey.ToString(),
        //    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
        //if (duplicateEntryTrxID != "")
        //{
        //    App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1.01b - Duplicate Entry detected, rerouting user to confirmation page (Aborting Payment Process)", 
        //        Membership.GetUser().ProviderUserKey.ToString(),
        //        amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //    Session["paypalTrxID"] = duplicateEntryTrxID;
        //    Session["paypalAmount"] = tmpAmount;
        //    Session["paymentLogID"] = paymentLogID;
        //    Session["amount"] = tmpAmount;
        //    Response.Redirect("~/members/BillPayProcessing.aspx");
        //    return;
        //}

        ///* Initialize list of transactions (amounts to locationIDs) */
        //List<PaymentTransaction> payments = new List<PaymentTransaction>();

        //foreach (GridViewRow locationRow in gvBills.Rows)
        //{
        //    if (locationRow.RowType == DataControlRowType.DataRow)
        //    {
        //        string locationID = gvBills.DataKeys[locationRow.RowIndex].Value.ToString();
        //        string amount = ((TextBox)locationRow.FindControl("tbAmount")).Text;
        //        decimal tmp_amount;
        //        try {
        //            tmp_amount = decimal.Parse(amount);
        //        }
        //        catch(Exception){
        //            successLabel.Text = string.Format("'{0}' is not a valid amount. Please make sure the amount entered is a valid number.", amount);
        //            successLabel.ForeColor = System.Drawing.Color.Red;
        //            App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, string.Format("1.02 - Invalid Amount Input ({0})", 
        //                amount), Membership.GetUser().ProviderUserKey.ToString(), amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
        //            return;
        //        }
        //        if (tmp_amount > 0)
        //        {
        //            payments.Add(new PaymentTransaction(custNbr: custNbr, locationID: locationID, amount: tmp_amount.ToString()));
        //        }
                

        //    }
        //}
    
        ///* Initialize Payment record in eusvr40's queue  */
        //Tuple<bool, string, List<PaymentTransaction>> initResponse =
        //    App_eBilling_Payments.InitializePaymentRecord(trxList: payments, username: profile.UserName);

        ///* Initializing payment record failed. Abort payment process and alert user to try again later */
        //if(initResponse.Item1 == false)
        //{

        //    App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1.1 - Failed to initialize Payment Record in Payment Queue (Aborting Payment Process)", Membership.GetUser().ProviderUserKey.ToString(),
        //        amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
        //    string strSuccess = initResponse.Item2;
        //    successLabel.Text = strSuccess;
        //    return;
        //}

        //App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1.1 - Succeeded in initializing Payment Record in Payment Queue", Membership.GetUser().ProviderUserKey.ToString(),
        //    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //TransactionResponse trx = null;
        //try
        //{

        //    App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "2 - Sent request to Paypal", Membership.GetUser().ProviderUserKey.ToString(),
        //    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //    /* Run Paypal transaction */
        //    decimal amount = decimal.Parse(amountTextBox.Text.Replace("$", ""));
        //    object[] values = DOSaleCompleteCS.RunV2_ZipAndState(Profile, cc, amount
        //       , tbStreet.Text, tbCity.Text, tbState.Text, tbZip.Text);

        //    LogWriter.Write(string.Format("Pay Bill: Successfully completed DOSaleCompleteCS.RunV2() for user: {0}", profile.UserName));
        //    string msg = values[0].ToString();
        //    bool success = bool.Parse(values[1].ToString());
        //    trx = (TransactionResponse)values[2];

        //    if (success)
        //    {
        //        App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "3 - Received Success response from Paypal (#" +
        //            ((TransactionResponse)values[2]).Pnref + ")", Membership.GetUser().ProviderUserKey.ToString(),
        //            amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

                
        //        /* Update payment record in SQL (and queue for Cogsdale injection) */
                
        //        App_eBilling_Payments.CompletePayment(trxList: payments, paypalTrxID: trx.Pnref, username: profile.UserName);

        //        App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "4 - Succeeded in updating Payment Record in Payment Queue", Membership.GetUser().ProviderUserKey.ToString(),
        //            amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //        Session["paypalTrxID"] = trx.Pnref;
        //        Session["paypalAmount"] = amount;
        //        Session["paymentLogID"] = paymentLogID;
        //        Session["amount"] = amountTextBox.Text.Replace("$", "");
        //        Response.Redirect("~/members/BillPayProcessing.aspx");
        //        return;
                
        //    }
        //    else
        //    {
        //        App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, string.Format("3 - Received Failed response from Paypal (#{0}, Error: {1})", ((TransactionResponse)values[2]).Pnref, msg), Membership.GetUser().ProviderUserKey.ToString(),
        //           amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        //        LogWriter.Write(string.Format("Pay Bill: Error Processing to Paypal for user: {0}. Error: {1}", profile.UserName, msg));
        //        string strErr = "Error: " + msg;
        //        errLabel.Text = strErr;
        //        pnlAddress.Visible = true;
        //        return;
        //    }
        //}
        //catch (Exception ex)
        //{

        //    LogWriter.Write(string.Format("Pay Bill: Error Processing to GP SQL for user: {0}. Error: {1}", profile.UserName, ex.Message));
        //    successLabel.Text = string.Format("Error Processing Payment. Please contact customer support with your transaction number: {0}",
        //        trx != null ? trx.Pnref : "null");
        //    //Response.Write("error processing");
        //}


        ////LogWriter.Write(string.Format("Pay Bill: Reached end of Pay Bill routine for user: {0}", profile.UserName));
    }
    protected void submit_ClickZipOnly(object sender, EventArgs e)
    {


        ProfileCommon profile = Profile;
        LogWriter.Write(string.Format("Pay Bill: Submit button clicked for user: {0} <#{1}>", profile.UserName, profile.CustomerDetails.CustNbr.Trim()));
        int paymentLogID = App_eBilling_Customer_Data.getNewPaymentLogID();

        
        App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1 - Submit Button Clicked", Membership.GetUser().ProviderUserKey.ToString(),
                amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
        CreditCard cc = new CreditCard(tbCC1.Text.Trim() + tbCC2.Text.Trim() + tbCC3.Text.Trim() + tbCC4.Text.Trim(),
            expdateDropDown.Text + yearDropDown.Text.Substring(yearDropDown.Text.Length - 2));
        cc.Cvv2 = CVVcodeTextBox.Text.Trim();


        TransactionResponse trx = null;
        try
        {
            int PaypalHeaderID = App_eBilling_Customer_Data.CreatePaypalHeader(custNbr);
            //object[] values = DOSaleCompleteCS.Run(Profile, cc, decimal.Parse(amountTextBox.Text.Replace("$", "")));
            App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "2 - Sent request to Paypal", Membership.GetUser().ProviderUserKey.ToString(),
            amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
            object[] values = DOSaleCompleteCS.RunV2_ZipOnly(Profile, cc, decimal.Parse(amountTextBox.Text.Replace("$", ""))
                , tbZip.Text);

            LogWriter.Write(string.Format("Pay Bill: Successfully completed DOSaleCompleteCS.RunV2() for user: {0}", profile.UserName));
            string msg = values[0].ToString();
            bool success = bool.Parse(values[1].ToString());
            trx = (TransactionResponse)values[2];
            string amount = "";

            App_eBilling_Customer_Data.UpdatePaypalHeader(trx.Pnref, msg, success, PaypalHeaderID);
            if (success)
            {
                App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "3 - Received Success response from Paypal (#" +
                    ((TransactionResponse)values[2]).Pnref + ")", Membership.GetUser().ProviderUserKey.ToString(),
                    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

                Session["Paypal"] = true;
                string strAmt = Convert.ToDecimal(trx.Amt).ToString("c", new CultureInfo("en-US"));
                //string strCcy = htResponse["CURRENCYCODE"].ToString();
                string strTransactionID = trx.Pnref;
                bool sqlSuccess = true;
                //ordersDataSource.InsertParameters["TransactionID"].DefaultValue = strTransactionID;
                List<PaymentTransaction> payments = new List<PaymentTransaction>();
                foreach (GridViewRow locationRow in gvBills.Rows)
                {
                    if (locationRow.RowType == DataControlRowType.DataRow)
                    {
                        string locationID = gvBills.DataKeys[locationRow.RowIndex].Value.ToString();
                        amount = ((TextBox)locationRow.FindControl("tbAmount")).Text;
                        if (decimal.Parse(amount) > 0)
                        {
                            App_eBilling_Customer_Data.AddSqlPayment(decimal.Parse(amount), locationID.Trim(),
                                custNbr.Trim(), strTransactionID, PaypalHeaderID);
                            payments.Add(new PaymentTransaction(amount, locationID.Trim(), custNbr, strTransactionID));
                        }

                        //sqlSuccess = postPaymentToSql(amount, locationID.Trim(), custNbr, strTransactionID);

                        //LogWriter.Write(string.Format("Pay Bill: Successfully completed postPaymentToSql() for user: {0}", profile.UserName));
                    }

                    /*
                    if (sqlSuccess == false)
                        break;*/
                }
                Session["Payments"] = payments;
                Session["PaymentLogID"] = paymentLogID;
                Session["Amount"] = amount;
                //Response.Redirect("~/members/BillPayFrame.aspx");
                Response.Redirect("~/members/BillPayProcessing.aspx");
                if (sqlSuccess)
                {
                    string strSuccess = "Thank you, your payment has been processed.";
                    successLabel.Text = strSuccess;
                    tableCC.Visible = gvBills.Visible = false;


                    lblTransNumber.Text = strTransactionID;
                    tableConfirmation.Visible = true;
                    lblPmtAmtConfirm.Text = string.Format("Payment Amount : {0}", strAmt);
                    pnlDataEntry.Visible = errLabel.Visible = false;
                }
                else
                {
                    string strSuccess = "Error processing payment.";
                    successLabel.Text = strSuccess;
                }

                //Response.Redirect(string.Format("~/members/Confirmation.aspx?transID={0}&amt={1}", strTransactionID, strAmt), false);
            }
            else
            {
                App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, string.Format("3 - Received Failed response from Paypal (#{0}, Error: {1})", ((TransactionResponse)values[2]).Pnref, msg), Membership.GetUser().ProviderUserKey.ToString(),
                   amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

                LogWriter.Write(string.Format("Pay Bill: Error Processing to Paypal for user: {0}. Error: {1}", profile.UserName, msg));
                string strErr = "Error: " + msg;//htResponse["L_LONGMESSAGE0"].ToString();
                //string strErrcode = "Error code: " + htResponse["L_ERRORCODE0"].ToString();
                errLabel.Text = strErr;
                pnlAddress.Visible = true;
                //errcodeLabel.Text = strErrcode;
                return;
            }
        }
        catch (Exception ex)
        {
            // do something to catch the error, like write to a log file.

            LogWriter.Write(string.Format("Pay Bill: Error Processing to GP SQL for user: {0}. Error: {1}", profile.UserName, ex.Message));
            successLabel.Text = string.Format("Error Processing Payment. Please contact customer support with your transaction number: {0}",
                trx != null ? trx.Pnref : "null");
            //Response.Write("error processing");
        }


        LogWriter.Write(string.Format("Pay Bill: Reached end of Pay Bill routine for user: {0}", profile.UserName));
    }
    protected void submit_Click(object sender, EventArgs e)
    {


        ProfileCommon profile = Profile;
        LogWriter.Write(string.Format("Pay Bill: Submit button clicked for user: {0} <#{1}>", profile.UserName, profile.CustomerDetails.CustNbr.Trim()));
        int paymentLogID = App_eBilling_Customer_Data.getNewPaymentLogID();

        App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "1 - Submit Button Clicked", Membership.GetUser().ProviderUserKey.ToString(),
            amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

        CreditCard cc = new CreditCard(tbCC1.Text.Trim() + tbCC2.Text.Trim() + tbCC3.Text.Trim() + tbCC4.Text.Trim(),
            expdateDropDown.Text + yearDropDown.Text.Substring(yearDropDown.Text.Length - 2));
        cc.Cvv2 = CVVcodeTextBox.Text.Trim();

       
        TransactionResponse trx = null;
        try
        {
            int PaypalHeaderID = App_eBilling_Customer_Data.CreatePaypalHeader(custNbr);
            //object[] values = DOSaleCompleteCS.Run(Profile, cc, decimal.Parse(amountTextBox.Text.Replace("$", "")));
            App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "2 - Sent request to Paypal", Membership.GetUser().ProviderUserKey.ToString(),
            amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);
            object[] values = DOSaleCompleteCS.RunV2(Profile, cc, decimal.Parse(amountTextBox.Text.Replace("$", ""))
                , tbStreet.Text, tbCity.Text, tbState.Text);
            
            LogWriter.Write(string.Format("Pay Bill: Successfully completed DOSaleCompleteCS.RunV2() for user: {0}", profile.UserName));
            string msg = values[0].ToString();
            bool success = bool.Parse(values[1].ToString());
            trx = (TransactionResponse)values[2];
            string amount = "";

            App_eBilling_Customer_Data.UpdatePaypalHeader(trx.Pnref, msg, success, PaypalHeaderID);
            if (success)
            {
                App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, "3 - Received Success response from Paypal (#" +
                    ((TransactionResponse)values[2]).Pnref + ")", Membership.GetUser().ProviderUserKey.ToString(),
                    amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

                Session["Paypal"] = true;
                string strAmt = Convert.ToDecimal(trx.Amt).ToString("c", new CultureInfo("en-US"));
                //string strCcy = htResponse["CURRENCYCODE"].ToString();
                string strTransactionID = trx.Pnref;
                bool sqlSuccess = true;
                //ordersDataSource.InsertParameters["TransactionID"].DefaultValue = strTransactionID;
                List<PaymentTransaction> payments = new List<PaymentTransaction>();
                foreach (GridViewRow locationRow in gvBills.Rows)
                {
                    if (locationRow.RowType == DataControlRowType.DataRow)
                    {
                        string locationID = gvBills.DataKeys[locationRow.RowIndex].Value.ToString();
                        amount = ((TextBox)locationRow.FindControl("tbAmount")).Text;
                        if (decimal.Parse(amount) > 0)
                        {
                            App_eBilling_Customer_Data.AddSqlPayment(decimal.Parse(amount), locationID.Trim(), 
                                custNbr.Trim(), strTransactionID, PaypalHeaderID);
                            payments.Add(new PaymentTransaction(amount, locationID.Trim(), custNbr, strTransactionID));
                        }

                        //sqlSuccess = postPaymentToSql(amount, locationID.Trim(), custNbr, strTransactionID);

                        //LogWriter.Write(string.Format("Pay Bill: Successfully completed postPaymentToSql() for user: {0}", profile.UserName));
                    }

                    /*
                    if (sqlSuccess == false)
                        break;*/
                }
                Session["Payments"] = payments;
                Session["PaymentLogID"] = paymentLogID;
                Session["Amount"] = amount;
                //Response.Redirect("~/members/BillPayFrame.aspx");
                Response.Redirect("~/members/BillPayProcessing.aspx");
                if (sqlSuccess)
                {
                    string strSuccess = "Thank you, your payment has been processed.";
                    successLabel.Text = strSuccess;
                    tableCC.Visible = gvBills.Visible = false;


                    lblTransNumber.Text = strTransactionID;
                    tableConfirmation.Visible = true;
                    lblPmtAmtConfirm.Text = string.Format("Payment Amount : {0}", strAmt);
                    pnlDataEntry.Visible = errLabel.Visible = false;
                }
                else
                {
                    string strSuccess = "Error processing payment.";
                    successLabel.Text = strSuccess;
                }

                //Response.Redirect(string.Format("~/members/Confirmation.aspx?transID={0}&amt={1}", strTransactionID, strAmt), false);
            }
            else
            {
                App_eBilling_Customer_Data.LogPaymentStep(paymentLogID, string.Format("3 - Received Failed response from Paypal (#{0}, Error: {1})", ((TransactionResponse)values[2]).Pnref, msg), Membership.GetUser().ProviderUserKey.ToString(),
                   amountTextBox.Text.Replace("$", ""), "CC Paypal", DateTime.Now);

                LogWriter.Write(string.Format("Pay Bill: Error Processing to Paypal for user: {0}. Error: {1}", profile.UserName, msg));
                string strErr = "Error: " + msg;//htResponse["L_LONGMESSAGE0"].ToString();
                //string strErrcode = "Error code: " + htResponse["L_ERRORCODE0"].ToString();
                errLabel.Text = strErr;
                pnlAddress.Visible = true;
                //errcodeLabel.Text = strErrcode;
                return;
            }
        }
        catch (Exception ex)
        {
            // do something to catch the error, like write to a log file.

            LogWriter.Write(string.Format("Pay Bill: Error Processing to GP SQL for user: {0}. Error: {1}", profile.UserName, ex.Message));
            successLabel.Text = string.Format("Error Processing Payment. Please contact customer support with your transaction number: {0}",
                trx != null ? trx.Pnref : "null");
            //Response.Write("error processing");
        }


        LogWriter.Write(string.Format("Pay Bill: Reached end of Pay Bill routine for user: {0}", profile.UserName));
    }
    private string GetConnString(bool gp)
    {
        if (Credentials.debug)
        {
            return ConfigurationManager.ConnectionStrings[gp ? "dbGPTest" : "dbMyCMSConnectionString"].ConnectionString;
        }
        else
            return ConfigurationManager.ConnectionStrings[gp ? "dbGP" : "dbMyCMSConnectionString"].ConnectionString;
    }
    private bool postPaymentToSql(string amt, string locationID, string customerID, string transID)
    {
        SqlCommand cmd;
        SqlDataReader dr;
        bool success = true;

        /* Form batch ID */
        string szBatch = "";
        DateTime dt = DateTime.Now;
        if (dt.Month < 10)
            szBatch = szBatch + "0" + dt.Month.ToString();
        else
            szBatch = szBatch + dt.Month.ToString();

        if (dt.Day < 10)
            szBatch = szBatch + "0" + dt.Day.ToString();
        else
            szBatch = szBatch + dt.Day.ToString();

        szBatch = "WBPY" + szBatch + dt.Year.ToString();

        SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(GetConnString(true));
        csb.ConnectTimeout = 10;
        /* Check if batch exists (if not, create it) */
        using (SqlConnection conn = new SqlConnection(csb.ConnectionString))
        {
            cmd = new SqlCommand("select * from UM00100 where umBatchID = @ID", conn);
            cmd.Parameters.AddWithValue("@ID", szBatch);

            try
            {
                conn.Open();
                dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    dr.Close();
                    conn.Close();
                    cmd = new SqlCommand("INSERT INTO UM00100 (umApproved, umApprovedDate, umApprovedUser, umBatchComment," +
                        " umBatchID, umBatchSource, umBatchStatus, BACHFREQ, umBatchTotal, umControlTotal, umCtrlNum, " +
                        "umDateCreated, umDateModified, umDatePosted, umMarked, umNumTrans, umPostUserID, umGLPOST, " +
                        "umRecurrPost, umRecurrLastDate, umNumPostings, umMiscBatchDays, umCurncy, umChkBook, NOTEINDX) " +
                        " SELECT '', '01-01-1900', '', '', @ID, 'PAYMENTS', 0, 1, 0.00, 0.00, 0, '01-01-1900'," +
                        " '01-01-1900', '01-01-1900', 0, 0, '', CONVERT(CHAR(10),GETDATE(),101), 0, '01-01-1900', 0, 0," +
                        " '', CHEKBKID, 0 FROM UM40000 WHERE umSetupKey = 1", conn);
                    cmd.Parameters.AddWithValue("@ID", szBatch);
                    cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Response.Write("error processsing");
                        success = false;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                else
                {
                    //dr.Close();
                    //conn.Close();
                }
            }
            catch (Exception ex)
            {
                Response.Write("error processsing");
                success = false;
            }
            finally
            {
                conn.Close();
            }

        }
        /* Post payment with customer details */
        string paymentNbr = "";
        using (SqlConnection conn = new SqlConnection(GetConnString(true)))
        {
            cmd = new SqlCommand("dbo.UM_spWrkPymtAdd", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@INBatchID", szBatch);
            cmd.Parameters.AddWithValue("@INLocationID", locationID);
            cmd.Parameters.AddWithValue("@INPaymentAmount", amt);
            cmd.Parameters.AddWithValue("@INCustomerNumber", custNbr);
            //cmd.Parameters.AddWithValue("@INPaymentNumber", "");
            SqlParameter param = new SqlParameter("@INPaymentNumber", paymentNbr);
            param.SqlDbType = SqlDbType.Char;
            param.Size = 15;
            param.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(param);

            cmd.Parameters.AddWithValue("@InStatDocNo", "");
            cmd.Parameters.AddWithValue("@INSortFilter", 1);
            cmd.Parameters.AddWithValue("@INPaymentDate", DateTime.Now.ToString());
            cmd.Parameters.AddWithValue("@INPaymentType", 2);
            cmd.Parameters.AddWithValue("@INDDLPymtOther", 1);
            cmd.Parameters.AddWithValue("@INComment", string.Format("WEB PYMT - AUTH # {0}", transID));
            cmd.Parameters.AddWithValue("@INChequeNumber", "");
            cmd.Parameters.AddWithValue("@INExtraControl", 0);

            try
            {
                conn.Open();
                string result = cmd.ExecuteNonQuery().ToString();
                string paymentNumber = cmd.Parameters["@INPaymentNumber"].Value.ToString();

                lblTransNumber.Text = paymentNumber;
            }
            catch (Exception ex)
            {
                Response.Write("error processing");
                success = false;
            }
            finally
            {
                conn.Close();
            }

        }
        /* Update Batch Total */
        using (SqlConnection conn = new SqlConnection(GetConnString(true)))
        {
            cmd = new SqlCommand("UPDATE UM00100 SET umBatchTotal = ISNULL( (SELECT SUM(umPaymentAmount) FROM UM10800 WHERE" +
                    " umBatchID = @ID AND umNumErrors = 0),0.00), umNumTrans = ISNULL((SELECT COUNT(*) FROM " +
                    "UM10800 WHERE umBatchID = @ID) ,0) WHERE umBatchID = @ID", conn);
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.Parameters.AddWithValue("@ID", szBatch);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("error processing");
                success = false;
            }
            finally
            {
                conn.Close();
            }
        }
        return success;
    }
    #endregion

    protected void sdsBills_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CustNbr"].Value = custNbr;
    }
    protected void CalculateAmount(object sender, EventArgs e)
    {
        float amtTotal = 0;
        if (gvBills.Rows.Count <= 0)
            return;
        foreach (GridViewRow row in gvBills.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox tbAmount = (TextBox)row.FindControl("tbAmount");
                Image img = (Image)row.FindControl("imgError");
                try
                {
                    float tmp = float.Parse(tbAmount.Text);

                    img.Visible = false;
                    tbAmount.Text = tmp.ToString("0.00");
                    tmp = float.Parse(tmp.ToString("0.00"));
                    amtTotal += tmp;

                }
                catch (Exception)
                {
                    // tbAmount.Text = (0.00f).ToString("0.00");

                    img.Visible = true;
                }

            }
        }

        //amountTextBox.Text = amtTotal.ToString("0.00");
        Label lbl = (Label)gvBills.FooterRow.FindControl("lblAmountTotal");
        lbl.Text = amountTextBox.Text = amtTotal.ToString("C");
    }

    protected void sdsCustInfo_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CustNbr"].Value = custNbr;
    }
    protected void gvBills_DataBound(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;
        if (gv.Rows.Count <= 0)
        {
            pnlDataEntry.Visible = false;
            return;
        }
        decimal balanceTotal = 0, amountTotal = 0;
        foreach (GridViewRow row in gv.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lbl = (Label)row.FindControl("lblBalance");
                decimal balance;
                decimal.TryParse(lbl.Text.Replace("$", ""), out balance);
                balanceTotal += balance;

                TextBox tb = (TextBox)row.FindControl("tbAmount");
                decimal amount;
                decimal.TryParse(tb.Text, out amount);
                amountTotal += amount;

            }
        }

        Label lblBalanceTotal = (Label)gv.FooterRow.FindControl("lblBalanceTotal");
        lblBalanceTotal.Text = "$" + balanceTotal.ToString();

        Label tbAmtTotal = (Label)gv.FooterRow.FindControl("lblAmountTotal");
        tbAmtTotal.Text = amountTotal.ToString("C");
    }
    protected void tbCC1_TextChanged(object sender, EventArgs e)
    {
        TextBox tb = (TextBox)sender;
        if (tb.Text.Length > 4)
        {
            string[] ccPartitions = Split(tb.Text, 4);
            tbCC1.Text = ccPartitions[0];
            tbCC2.Text = ccPartitions[1];
            tbCC3.Text = ccPartitions[2];
            tbCC4.Text = ccPartitions[3];
        }
        else if (tb.Text.Length == 4)
        {
            tbCC2.Focus();
        }
    }
    protected void tbCC1_TextChanged()
    {
        TextBox tb = tbCC1;
        if (tb.Text.Length > 4)
        {
            string[] ccPartitions = Split(tb.Text, 4);
            tbCC1.Text = ccPartitions[0];
            tbCC2.Text = ccPartitions[1];
            tbCC3.Text = ccPartitions[2];
            tbCC4.Text = ccPartitions[3];
        }
        else if (tb.Text.Length == 4)
        {
            tbCC2.Focus();
        }
    }
    static string[] Split(string str, int chunkSize)
    {
        string[] retVal = new string[4] { "", "", "", "" };
        int retValIndex = 0;
        for (int i = 0; i < str.Length; i += chunkSize)
        {
            if (i + chunkSize > str.Length) chunkSize = str.Length - i;
            //Console.WriteLine(str.Substring(i, chunkSize));
            retVal[retValIndex++] = str.Substring(i, chunkSize);
        }
        return retVal;
    }
}