﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BillPayFrame.aspx.cs" Inherits="members_BillPayFrame"
    MasterPageFile="~/members/themes/default/default.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div runat="server" id="slowScreenSplash" class="centered" style="text-align: center; position:relative; top: 200px;">
    <asp:Image runat="server" ImageUrl="~/members/themes/default/images/progress_indicator.gif"
      />
      <br />
      Payment processing...
      </div>
    <iframe runat="server" id="iframe1" frameborder="0" height="700px" width="755px"
        style="z-index: 2;" ClientIDMode="Static"></iframe>
    <script type="text/javascript">
        $('#<%= iframe1.ClientID %>').load(function () {
            // do something once the iframe is loaded
            $('#<%= slowScreenSplash.ClientID %>').hide();
        });
    </script>
</asp:Content>
