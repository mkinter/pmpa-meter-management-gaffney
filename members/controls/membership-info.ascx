﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="membership-info.ascx.cs" Inherits="members_controls_membership_info" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../themes/default/default.css" rel="stylesheet" type="text/css" />

<div class="userInfoWrap">
    <%-- ajax update panel start --%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%-- ajax tab container start --%>
            <cc1:TabContainer ID="tcntUserInfo" runat="server" ActiveTabIndex="1" 
                Width="100%" Font-Size="10px" CssClass="aTab1">
                <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Change Password/Email">
                    <ContentTemplate>
                        <div class="contentTemplate"><div class="formSectionTitle2">USER DETAILS </div>
                <asp:DetailsView AutoGenerateRows="False" DataSourceID="ObjectDataSource1" ID="DetailsView1" runat="server" OnItemUpdating="DetailsView1_ItemUpdating" CssClass="dv">
                    <AlternatingRowStyle CssClass="dvAlternateRowStyle" />
                    <FieldHeaderStyle CssClass="dvFieldHeader" />
                    <Fields><asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" SortExpression="UserName"></asp:BoundField>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email"><EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="Email only!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="Email is required!" SetFocusOnError="True"></asp:RequiredFieldValidator></EditItemTemplate>
                            <InsertItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox></InsertItemTemplate>
                            <ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Bind("Email") %>'></asp:Label></ItemTemplate></asp:TemplateField>
                        <asp:BoundField DataField="CreationDate" HeaderText="Creation Date" 
                            ReadOnly="True" SortExpression="CreationDate"></asp:BoundField>
                        <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" 
                            ReadOnly="True" SortExpression="LastActivityDate"></asp:BoundField>
                        <asp:CheckBoxField DataField="IsOnline" HeaderText="Online?" 
                            SortExpression="IsOnline" ReadOnly="True" Visible="False"></asp:CheckBoxField>
                        <asp:CheckBoxField DataField="IsApproved" HeaderText="Approved?" 
                            SortExpression="IsApproved" ReadOnly="True" Visible="False"></asp:CheckBoxField>
                        <asp:CheckBoxField DataField="IsLockedOut" HeaderText="Locked Out?" 
                            SortExpression="IsLockedOut" ReadOnly="True" Visible="False"></asp:CheckBoxField>
                        <asp:BoundField DataField="PasswordQuestion" HeaderText="Password Question" 
                            SortExpression="PasswordQuestion" ReadOnly="True"></asp:BoundField>
                        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" 
                            SortExpression="LastLoginDate" ReadOnly="True"></asp:BoundField>
                        <asp:BoundField DataField="LastLockoutDate" HeaderText="Last Lockout Date" 
                            ReadOnly="True" SortExpression="LastLockoutDate"></asp:BoundField>
                        <asp:BoundField DataField="LastPasswordChangedDate" 
                            HeaderText="Last Password Changed Date" 
                            SortExpression="LastPasswordChangedDate" ReadOnly="True"></asp:BoundField>
                        <asp:CommandField ButtonType="Button" ShowEditButton="True" EditText="Edit User Details">
                        <ControlStyle Font-Size="11px" /></asp:CommandField></Fields>
                    <HeaderStyle CssClass="dvHeader" />
                    <RowStyle CssClass="dvRowStyle" /></asp:DetailsView>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="System.Web.Security.MembershipUser" SelectMethod="GetUser" TypeName="System.Web.Security.Membership"></asp:ObjectDataSource>
                            <br /><br />
                            <br /><br />
                            <asp:ChangePassword ID="ChangePassword1" runat="server" EnableViewState="False" ContinueDestinationPageUrl="~/members/UserPanel.aspx">
                                <MailDefinition BodyFileName="~/email_templates/change-password.htm" IsBodyHtml="True" Subject="Your password has been changed!"></MailDefinition></asp:ChangePassword>
                                
                                <br />
                                <br />
                            <asp:Button Visible='False' ID="btnDeleteCurrentUser" runat="server" Text="Close and Delete my account" OnClick="btnDeleteCurrentUser_Click" OnClientClick="return confirm('Are you sure? This action will delete all your information and is unrecoverable.');" /><br />
                            <br /><asp:Label ID="lblResult" runat="server" Visible="False" BackColor="Red" /></div></ContentTemplate></cc1:TabPanel><cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Edit Profile">
                    <HeaderTemplate>
                        Edit Subscriptions
                    </HeaderTemplate>
                    <ContentTemplate>

                        <div class="contentTemplate">
                        <div class="formSectionTitle2">CUSTOMER DETAIL</div>
                             <asp:Label ID="Label3" runat="server" Text="Customer Name: "></asp:Label>
                                <asp:Label ID="custName" runat="server" ></asp:Label>
                                <br />
                                <asp:Label ID="Label2" runat="server" Text="Customer Number: "></asp:Label>
                                <asp:Label ID="custNbr" runat="server"></asp:Label>
                                <br />
                                <br />
                        <div class="formSectionTitle2">PAPERLESS BILLING OPT-IN</div>
                                e-Statement:<br /><br />  
                                <asp:DropDownList runat="server" ID="ddlNewsletter">
                                    <asp:ListItem Text="No subscription" Value="False" Selected="True" />
                                    <asp:ListItem Text="Subscribe to paperless billing" Value="True" /></asp:DropDownList></div>
                            <div class="formSectionEnd"></div>
                            <div class="formButton"><asp:Button ID="btnUpdateProfile" runat="server" Text="Save Profile" ValidationGroup="EditProfile" OnClick="btnUpdateProfile_Click" OnClientClick="return confirm('This will Update your Profile information..\n\rClick OK to continue.')" />&#160;&#160;<asp:Label ID="lblProfileMessage" runat="server" /></div>
                        </div></ContentTemplate></cc1:TabPanel></cc1:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
