﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities.Security;

public partial class members_controls_Account_Info : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LocationID"] == null)
        {
            pnlAcctInfo.Visible = false;
            pnlNoInfo.Visible = true;
        }
        else
        {
            pnlAcctInfo.Visible = true;
            pnlNoInfo.Visible = false;

            var parms = new
            {
                LocationID = Session["LocationID"].ToString(),//"999998",
                Username = Membership.GetUser().UserName
            };
            var jsonString = JsonConvert.SerializeObject(parms);

            hfLocationReset.Value = System.Uri.EscapeDataString(AES2.Encrypt(jsonString));
            hfUsername.Value = parms.Username;
        }

        //if (Request.QueryString["test"] == "1")
        //{
        //    string[] vars = pnlResetCable.Attributes["style"].Split(';');
        //    List<string> newVars = new List<string>();

        //    foreach (string tmpVar in vars)
        //    {
        //        var keyValuePair = tmpVar.Split(':');
        //        if (keyValuePair.Length < 2)
        //            continue;

        //        var key = keyValuePair[0];
        //        var value = keyValuePair[1];

        //        if (key.ToLower().Trim() != "display")
        //            newVars.Add(string.Format("{0}: {1};", key, value));

        //    }
        //    var tmpStyle = "";
        //    foreach(string tmpVar in newVars){
        //        tmpStyle += tmpVar;
        //    }
        //    pnlResetCable.Attributes["style"] = tmpStyle;

        //}
        //else
        //    pnlResetCable.Visible = false;

        gvBills.DataSourceID = sdsBills.ID;
        gvRecentPayments.DataSourceID = sdsPayments.ID;
        gvLast12Statements.DataSourceID = sdsLast12Statements.ID;
        gvLast12Payments.DataSourceID = sdsLast12Payments.ID;
        gvConnections.DataSourceID = sdsConnections.ID;
    }
    
    protected void gvBills_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label pastDue = (Label)e.Row.FindControl("lblPastDue");
            //double pastDueAmt = double.Parse(pastDue.ToString());
            /*if (pastDueAmt == 0)
            {

            }*/


        }
    }
    protected void btnPayments_Click(object sender, EventArgs e)
    {
        mvPaymentStatements.SetActiveView(vwRecentPayments);
    }
    protected void btnStatements_Click(object sender, EventArgs e)
    {
        mvPaymentStatements.SetActiveView(vwStatements);
    }
    protected void imgBtnPayments_Click(object sender, ImageClickEventArgs e)
    {
        if (mvPaymentStatements.GetActiveView() == vwRecentPayments)
            return;
        else
        {
            imgBtnPayments.ImageUrl = "~/members/themes/default/images/tabs/Payments1.png";
            imgBtnStatements.ImageUrl = "~/members/themes/default/images/tabs/Statements2.png";
            mvPaymentStatements.SetActiveView(vwRecentPayments);
        }
    }
    protected void imgBtnStatements_Click(object sender, ImageClickEventArgs e)
    {
        if (mvPaymentStatements.GetActiveView() == vwStatements)
            return;
        else
        {
            imgBtnPayments.ImageUrl = "~/members/themes/default/images/tabs/Payments2.png";
            imgBtnStatements.ImageUrl = "~/members/themes/default/images/tabs/Statements1.png";
            mvPaymentStatements.SetActiveView(vwStatements);
        }
    }
    protected void gvLast12Statements_DataBound(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;
        for(int index = 0; index < gv.Rows.Count; index++)
        {
            GridViewRow row = gv.Rows[index];
            if (row.RowType == DataControlRowType.DataRow)
            {
                string statementID = gv.DataKeys[index].Values["StatementID"].ToString();
                HyperLink hl = (HyperLink)row.FindControl("hlPDF");
                hl.NavigateUrl = string.Format("javascript:void(popitup('http://aps1.eastonutilities.com/StatementViewerV2/?StatementNbr={0}'));", statementID);
            }
        }
    }
    protected void gvConnections_DataBound(object sender, EventArgs e)
    {
        pnlResetCable.Visible = false;

        foreach (GridViewRow row in gvConnections.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                var service = row.FindControl("lblService") as Label;
                var status = row.FindControl("lblStatus") as Label;

                if (service.Text == "CABLE" && status.Text == "Active")
                {
                    pnlResetCable.Visible = true;
                }
            }
        }
    }
}