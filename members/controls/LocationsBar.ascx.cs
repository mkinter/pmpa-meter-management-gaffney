﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_controls_LocationsBar : System.Web.UI.UserControl
{
    ProfileCommon profile;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated)
        {
            // get the selected user's profile based on query string
            profile = Profile;
        }
        if (!Page.IsPostBack)
        {

            if (gvLocations.Rows.Count > 0)
            {
                if (Session["LocationID"] != null)
                    SetSelectedRow(Session["LocationID"].ToString());
                else
                {
                    gvLocations.SelectedIndex = 0;
                    gvLocations_SelectedIndexChanged(gvLocations, null);
                }
            }
            string name = profile.CustomerDetails.CustName;
            name = name == null ? "" : name.ToLower();
            lblName.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                   name);
        }
    }
    private void SetSelectedRow(string LocationID)
    {
        if (string.IsNullOrEmpty(LocationID))
            return;
        gvLocations.SelectedIndex = 0;
        foreach (GridViewRow row in gvLocations.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (gvLocations.DataKeys[row.RowIndex].Value.ToString() == LocationID)
                {
                    gvLocations.SelectedIndex = row.RowIndex;
                    gvLocations_SelectedIndexChanged(gvLocations, null);

                    return;
                }
            }
        }
    }
    protected void gvLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gv = (GridView) sender;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Clear();

            e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'; this.style.backgroundColor='lightsteelblue';");
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.gvLocations, "Select$" + e.Row.RowIndex));

            if(gv.SelectedIndex != e.Row.RowIndex)
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
                
        }
    }
    protected void gvLocations_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;
        GridViewRow row = gv.SelectedRow;
        string locationID = gv.DataKeys[row.RowIndex].Value.ToString();
        Session["LocationID"] = locationID;

        foreach (GridViewRow currRow in gv.Rows)
        {
            if (currRow.RowType == DataControlRowType.DataRow)
            {
                currRow.Attributes.Clear();

                currRow.Attributes.Add("onmouseover", "this.style.cursor='pointer'; this.style.backgroundColor='lightsteelblue';");
                currRow.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.gvLocations, "Select$" + currRow.RowIndex));

                if (gv.SelectedIndex != currRow.RowIndex)
                    currRow.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
            }
        }
    }
}