﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Account-Info.ascx.cs" Inherits="members_controls_Account_Info" %>

<script type="text/javascript">
        
</script>


<asp:Panel ID="pnlNoInfo" runat="server" Visible="false">
    <h3>No current active accounts.</h3>

</asp:Panel>

<asp:Panel ID="pnlAcctInfo" runat="server">
    <h2>Account Info</h2>
    <asp:GridView ID="gvBills" runat="server" AutoGenerateColumns="False"
        CellPadding="5"
        BorderStyle="None" Width="580px"
        OnRowDataBound="gvBills_RowDataBound" GridLines="None">
        <Columns>
            <asp:BoundField DataField="AccountBalance" HeaderText="Balance"
                SortExpression="AccountBalance" DataFormatString="{0:c}">
                <ItemStyle ForeColor="#1F8C1F" Width="33%" HorizontalAlign="Center"
                    Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="PastDue" HeaderText="Past Due"
                SortExpression="PastDue" DataFormatString="{0:c}">
                <ItemStyle ForeColor="#CC0000" Width="33%" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="BudgetAmount" HeaderText="Budget Amount"
                SortExpression="BudgetAmount" DataFormatString="{0:c}" ReadOnly="True">
                <ItemStyle ForeColor="#1F8C1F" Font-Bold="True" />
            </asp:BoundField>

        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
        <RowStyle BorderStyle="None" HorizontalAlign="Center" Font-Size="13pt" />
    </asp:GridView>
    <br />
    <asp:Panel ID="pnlLatestPayments" Visible="false" runat="server">
        <h2>Latest Payment</h2>
        <asp:GridView ID="gvRecentPayments" runat="server"
            AutoGenerateColumns="False" CellPadding="5">
            <Columns>
                <asp:BoundField DataField="PaymentState" HeaderText="Payment State"
                    SortExpression="PaymentState" />
                <asp:BoundField DataField="PaymentDate" HeaderText="Date"
                    SortExpression="PaymentDate" DataFormatString="{0:MM/dd/yyyy}" />
                <asp:BoundField DataField="PaymentAmount" HeaderText="Amount"
                    SortExpression="PaymentAmount" ReadOnly="True" DataFormatString="{0:c}" />
            </Columns>
        </asp:GridView>

    </asp:Panel>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="paymentsTable" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="210px">
                        <table width="100%" cellpadding="0" cellspacing="0px">
                            <tr>
                                <td width="105px" class="tabTD">
                                    <asp:ImageButton ID="imgBtnStatements" runat="server"
                                        ImageUrl="~/members/themes/default/images/tabs/Statements1.png" Width="105px"
                                        OnClick="imgBtnStatements_Click" />
                                </td>
                                <td width="5px">&nbsp;</td>
                                <td class="tabTD">

                                    <asp:ImageButton ID="imgBtnPayments" runat="server"
                                        ImageUrl="~/members/themes/default/images/tabs/Payments2.png" Width="105px"
                                        OnClick="imgBtnPayments_Click" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" border="0" style="background-color: #E8F1F8;">
                            <tr>
                                <td>
                                    <asp:MultiView ID="mvPaymentStatements" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="vwStatements" runat="server">
                                            <asp:GridView ID="gvLast12Statements" runat="server" AutoGenerateColumns="False"
                                                CellPadding="5" Width="540px" DataKeyNames="StatementID"
                                                OnDataBound="gvLast12Statements_DataBound" EnableModelValidation="True">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:BoundField DataField="StatementDate" HeaderText="Date"
                                                        SortExpression="StatementDate" DataFormatString="{0:MM/dd/yyyy}">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle Width="95px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AmountDue" HeaderText="Amount Due"
                                                        SortExpression="AmountDue" DataFormatString="{0:c}">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AmountOutstanding" HeaderText="Amount Outstanding"
                                                        SortExpression="AmountOutstanding" DataFormatString="{0:c}">


                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>


                                                    <asp:TemplateField HeaderText="Statement">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlPDF" runat="server" NavigateUrl=''
                                                                ImageUrl="~/members/themes/default/images/paper.png" BorderStyle="None" Border="0">
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                </Columns>
                                                <HeaderStyle BackColor="#D6DDE7" ForeColor="#364761" />
                                            </asp:GridView>
                                        </asp:View>
                                        <asp:View ID="vwRecentPayments" runat="server">
                                            <asp:GridView ID="gvLast12Payments" runat="server" AutoGenerateColumns="False"
                                                CellPadding="5" BackColor="#E8F1F8" Width="540px" EnableModelValidation="True">

                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:BoundField DataField="PaymentState" HeaderText="Payment State"
                                                        SortExpression="PaymentState">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PaymentDate" DataFormatString="{0:MM/dd/yyyy}"
                                                        HeaderText="Date" SortExpression="PaymentDate">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PaymentAmount" DataFormatString="{0:c}"
                                                        HeaderText="Amount" ReadOnly="True" SortExpression="PaymentAmount">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle BackColor="#D6DDE7" ForeColor="#364761" />
                                            </asp:GridView>
                                        </asp:View>
                                    </asp:MultiView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div>
                <h2 style="display: inline-block;">Services</h2>
                <%--<asp:Panel ID="pnlResetCable" Visible="false" runat="server" CssClass="displayInlinex" Style="top: 11px; padding-right: 58px; float: right; display: inline; position: relative;">--%>
                <asp:Panel ID="pnlResetCable" runat="server" Style="top: 11px; padding-right: 58px; float: right;  position: relative;" >
                    <a class="popup-with-form" href="#cableResetModal" >
                        <asp:Image ID="imgResetCable" runat="server" ImageUrl="~/images/CableReset3.png" />

                    </a>
                    <span id="floatbar">
                        <a href="#" >
                            <asp:Image ID="imgResetInfo" ClientIDMode="Static" runat="server" ImageUrl="~/images/InformationIcon.png" style="padding-bottom: 5px;" />

                        </a>
                        <div class="box" style="left: 190px; top: 0px; position: absolute;">If you are experiencing issues with your cable service, 
                            click the 'Reset Cable Box' link to the left to start the automated process to reset your cable signal.
                        </div>
                    </span>
                    <asp:HiddenField ID="hfLocationReset" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hfUsername" runat="server" ClientIDMode="Static" />
                </asp:Panel>
            </div>
            <asp:GridView ID="gvConnections" runat="server" AutoGenerateColumns="False"
                CellPadding="5" BackColor="#E8F1F8" Width="540px" OnDataBound="gvConnections_DataBound">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Service" SortExpression="Service">
                        <ItemTemplate>
                            <asp:Label ID="lblService" runat="server" Text='<%# Bind("Service") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Meter" HeaderText="Meter"
                        SortExpression="Meter">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RateDescription" HeaderText="Description"
                        SortExpression="RateDescription" ReadOnly="True" />
                    <asp:TemplateField HeaderText="Status" SortExpression="ConnectionStatusDesc">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("ConnectionStatusDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#D6DDE7" ForeColor="#364761" />
            </asp:GridView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

<asp:SqlDataSource ID="sdsConnections" runat="server"
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" SelectCommand="EXEC [Overview].spConnections
		@CustNbr,
		@LocationID">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" />
        <asp:SessionParameter Name="LocationID" SessionField="LocationID" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLast12Payments" runat="server"
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" SelectCommand="EXEC Overview.spPayments @CustNbr,
		@LocationID">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" />
        <asp:SessionParameter Name="LocationID" SessionField="LocationID" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLast12Statements" runat="server"
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" SelectCommand="EXEC [Overview].spStatements
		@CustNbr,
		@LocationID">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" />
        <asp:SessionParameter Name="LocationID" SessionField="LocationID" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsPayments" runat="server"
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" SelectCommand="EXEC	 [Overview].[spLatestPayment]
		@CustNbr,
		@LocationID">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" />
        <asp:SessionParameter Name="LocationID" SessionField="LocationID" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsBills" runat="server"
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>"
    SelectCommand="EXEC	[Overview].[spHeaderInformation] @CustNbr,
		@LocationID">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr"
            Type="String" />
        <asp:SessionParameter Name="LocationID" SessionField="LocationID" />
    </SelectParameters>
</asp:SqlDataSource>


