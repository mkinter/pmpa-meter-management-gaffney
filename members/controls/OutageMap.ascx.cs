﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Live.ServerControls.VE;

public partial class members_controls_OutageMap : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            getOutageDataAndPlot();
        }
    }

    private void getOutageDataAndPlot()
    {
        Shape shape;
        List<MapOutage> outages = App_OutageManagementSystem.GetRecentOutages();
        foreach (MapOutage point in outages)
        {
            shape = new Shape(ShapeType.Pushpin, point.coordinates);
            switch (point.outageType.ToLower())
            {
                case("electric"):
                    shape.CustomIcon = "themes/default/images/electric.png";
                    shape.Description = "Electric Outage";
                    break;
                case("cable"):
                    shape.CustomIcon = "themes/default/images/cable.png";
                    shape.Description = "Cable Outage";
                    break;
                case ("gas"):
                    shape.CustomIcon = "themes/default/images/gas.png";
                    shape.Description = "Gas Outage";
                    break;
                case ("internet"):
                    shape.CustomIcon = "themes/default/images/internet.png";
                    shape.Description = "Internet Outage";
                    break;
                case ("water"):
                    shape.CustomIcon = "themes/default/images/water.png";
                    shape.Description = "Water Outage";
                    break;
                default:
                    shape.CustomIcon = "themes/default/images/PushPin.png";
                    break;
            }
            Map1.AddShape(shape);
        }
    }
}