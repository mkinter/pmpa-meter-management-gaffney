﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutageMap.ascx.cs" Inherits="members_controls_OutageMap" %>
<%@ Register assembly="Microsoft.Live.ServerControls.VE" namespace="Microsoft.Live.ServerControls.VE" tagprefix="ve" %>
<%@ Register assembly="Microsoft.Live.ServerControls.VE" namespace="Microsoft.Live.ServerControls.VE.Extenders" tagprefix="cc1" %>


<ve:Map ID="Map1" runat="server" Height="600px" Width="100%" ZoomLevel="11"
                        Center-Latitude="38.7741667" Center-Longitude="-76.0766667" 
                        DisambiguationDialog="False" HTTPS="true"  />