﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationsBar.ascx.cs" Inherits="members_controls_LocationsBar" %>
Welcome, 
                        <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
<h2>Accounts</h2>
<asp:GridView ID="gvLocations" runat="server" AutoGenerateColumns="False" 
    CellPadding="5" DataSourceID="sdsLocations" DataKeyNames="LocationID" 
    onrowdatabound="gvLocations_RowDataBound" 
    onselectedindexchanged="gvLocations_SelectedIndexChanged" Width="200px" 
    EnableModelValidation="True">
    
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:BoundField DataField="LocationID" HeaderText="ID" 
            SortExpression="LocationID" />
        <asp:BoundField DataField="FullAddressLine1" HeaderText="Service Address" 
            SortExpression="FullAddressLine1" >
        <ItemStyle Wrap="True" />
        </asp:BoundField>
        <asp:BoundField DataField="AccountBalance" DataFormatString="{0:c}" 
            HeaderText="Balance" SortExpression="AccountBalance" >
        <HeaderStyle Wrap="False" />
        </asp:BoundField>
    </Columns>
    <HeaderStyle BackColor="#D6DDE7" ForeColor="#364761" />
    <SelectedRowStyle BackColor="LightSteelBlue" />
</asp:GridView>
<asp:SqlDataSource ID="sdsLocations" runat="server" 
    ConnectionString="<%$ ConnectionStrings:dbCustomerInfo %>" SelectCommand="EXEC	[Overview].[spCustomerLocationSummary]
		@CustNbr">
    <SelectParameters>
        <asp:ProfileParameter Name="CustNbr" PropertyName="CustomerDetails.CustNbr" />
    </SelectParameters>
</asp:SqlDataSource>

