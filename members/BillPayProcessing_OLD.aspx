﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BillPayProcessing_OLD.aspx.cs"
    Inherits="members_BillPayProcessing" MasterPageFile="~/members/themes/default/default.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div runat="server" id="slowScreenSplash" style="position: absolute; z-index: 1;
                top: 50%; left: 50%; margin-top: -75px; margin-left: -75px; font-family: Arial;
                font-weight: bold;" align="center">
                <br />
                Payment Processing...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <asp:Label ID="lblError" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    <table style="margin-top: 15px;" width="700px" id="tableConfirmation" runat="server"
        visible="false">
        <tr>
            <td colspan="2" align="center">
                <h3>
                    Thank you
                    <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>, for your payment!</h3>
                <h5>
                    <asp:Label ID="lblPmtAmtConfirm" runat="server" Text="Label"></asp:Label><br />
                    Please keep your transaction number:
                    <asp:Label ID="lblTransNumber" runat="server" Text="Label "></asp:Label>
                    on file for reference.</h5>
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>
