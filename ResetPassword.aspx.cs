﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Utilities.Logging;

public partial class ResetPassword : System.Web.UI.Page
{
    private PasswordRecoveryResponse response;
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        string username = "mlaw@eucmail.com";
        string password = Membership.Providers["dbSqlMemberShipProviderAdmin"].GetPassword(username, null);
        Membership.GetUser(username).ChangePassword(password, "password");*/
        if (Request.QueryString["token"] != null)
        {
            string token = Request.QueryString["token"].ToString();
            response = App_eBilling_Customer_Data.validateRecoveryToken(token);
            if (response.success)
            {
                pnlUpdatePassword.Visible = true;
                lblUser.Text = string.Format("Password reset for username: <b>{0}</b>", response.username);
            }
            else
            {
                lblError.Text = "The link you are trying to access has expired. Click <a href='https://olp1.eastonutilities.com/recover-password.aspx'>here</a> to resend another password recovery link";
            }
        }
        else
            lblError.Text = "Invalid link (no token provided in query)";
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (string.Compare(tbPassword1.Text, tbPassword2.Text) != 0)
        {
            lblError.Text = "Passwords do not match. Please re-enter passwords.";
            return;
        }
        else
        {
            string username = response.username;
            string password = Membership.Providers["dbSqlMemberShipProviderAdmin"].GetPassword(username, null);
            Membership.GetUser(username).ChangePassword(password, tbPassword1.Text);
            pnlConfirm.Visible = true;
            pnlUpdatePassword.Visible = false;
            lblError.Text = "";
            lblConfirm.Text = "Password has been successfully set. Click <a href='https://olp1.eastonutilities.com'>here</a> to login!";
            LogWriter.Write(string.Format("Password Set for {0}", username));
            App_eBilling_Customer_Data.VoidToken(response.P_ID);
        }

    }
}