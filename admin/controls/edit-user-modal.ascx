﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="edit-user-modal.ascx.cs" Inherits="admin_controls_edit_user_modal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="js-include3.ascx" TagName="js" TagPrefix="uc3" %>
<%@ Register Src="~/js/js/jquery.ascx" TagName="jquery" TagPrefix="uc4" %>
<div>
    <%-- ajax update panel start --%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%-- ajax tab container start --%>
            <cc1:TabContainer ID="tcntUserInfo" runat="server" ActiveTabIndex="3" 
                Width="100%" Font-Size="10px" CssClass="aTab1">
                <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Create New Role">
                    <ContentTemplate>
                        <div class="contentTemplate">
                            <div class="formSectionTitle2">
                                CREATE NEW ROLE
                            </div>
                            <%-- create new role --%>
                            <asp:TextBox runat="server" ID="NewRole" MaxLength="50" ToolTip="Type the name of a new role you want to create."></asp:TextBox>
                            <asp:Button ID="Button3" runat="server" CssClass="inputButton" OnClick="AddRole" Text="Add Role" ToolTip="Click to create new role." />
                            <%-- confirmation message --%>
                            <div runat="server" id="ConfirmationMessage">
                            </div>
                        </div>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="General User Info">
                    <ContentTemplate>
                        <div class="contentTemplate">
                            <div class="formSectionTitle2">
                                USER ROLES
                            </div>
                            <div class="checkboxList">
                                <asp:CheckBoxList ID="UserRoles" runat="server" RepeatDirection="Horizontal" />
                            </div>
                            <br />
                            <div class="formSectionTitle2">
                                USER INFO
                            </div>
                            <asp:DetailsView AutoGenerateRows="False" DataSourceID="MemberData" ID="UserInfo" runat="server" OnItemUpdating="UserInfo_ItemUpdating" DefaultMode="Edit" CssClass="dv" EnableModelValidation="True" GridLines="None">
                                <Fields>
                                    <asp:BoundField DataField="UserName" HeaderText="User Name" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="Email" HeaderText="Email" ControlStyle-Width="245px"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Comment">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Comment") %>' TextMode="MultiLine" Height="100px" Width="245px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CheckBoxField DataField="IsApproved" HeaderText="Active User"></asp:CheckBoxField>
                                    <asp:CheckBoxField DataField="IsLockedOut" HeaderText="Is Locked Out" ReadOnly="True"></asp:CheckBoxField>
                                    <asp:CheckBoxField DataField="IsOnline" HeaderText="Is Online" ReadOnly="True"></asp:CheckBoxField>
                                    <asp:BoundField DataField="CreationDate" HeaderText="Creation Date" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="LastLockoutDate" HeaderText="Last Lockout Date" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="LastPasswordChangedDate" HeaderText="Last Password Changed Date" ReadOnly="True"></asp:BoundField>
                                    <asp:TemplateField ShowHeader="False">
                                        <EditItemTemplate>
                                            <div class="clearBoth2">
                                            </div>
                                            <asp:Button ID="Button1" CssClass="inputButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update User" OnClientClick="return confirm('This will UPDATE the User Info. Click OK to continue.')"/>
                                            <asp:Button ID="Button2" CssClass="inputButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                                            <asp:Button ID="Button4" CssClass="inputButton" runat="server" Text="Unlock User" OnClick="UnlockUser" OnClientClick="return confirm('Click OK to unlock this user.')" />
                                            <asp:Button ID="Button5" CssClass="inputButton" runat="server" Text="Delete User" OnClick="DeleteUser" OnClientClick="return confirm('Are you sure? This will delete all information related to this user including the user profile.')" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <div class="clearBoth2">
                                            </div>
                                            <asp:Button ID="Button1" CssClass="inputButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit User Info" />
                                        </ItemTemplate>
                                        <ControlStyle Font-Size="11px" />
                                    </asp:TemplateField>
                                </Fields>
                                <RowStyle CssClass="dvRowStyle" />
                                <FieldHeaderStyle CssClass="dvFieldHeader" />
                                <HeaderStyle CssClass="dvHeader" />
                                <AlternatingRowStyle CssClass="dvAlternateRowStyle" />
                            </asp:DetailsView>
                            <div class="messageWrap2">
                                <asp:Literal ID="UserUpdateMessage" runat="server"></asp:Literal>
                            </div>
                            <br />
                            <asp:ObjectDataSource ID="MemberData" runat="server" DataObjectTypeName="System.Web.Security.MembershipUser" SelectMethod="GetUser" UpdateMethod="UpdateUser" TypeName="System.Web.Security.Membership">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="username" QueryStringField="username" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="User Profile">
                    <ContentTemplate>
                        <div class="contentTemplate">
                            <div class="formSectionTitle2">
                                USER DETAIL
                            </div>
                            <div class="formLabelsText">
                                Customer Number:<br />
                                <asp:TextBox ID="custNbr" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="formLabelsText">
                                Customer Name:<br />
                                <asp:TextBox ID="custName" runat="server" Width="99%" MaxLength="50" />
                            </div>
                            <div class="formSectionTitle2">
                                e-Billing
                            </div>
                            <div class="formLabelsText">
                                Subscribed to e-Bill:<br />
                                <asp:DropDownList ID="ddlNewsletter" runat="server">
                                    <asp:ListItem Selected="True" Text="No" Value="False" />
                                    <asp:ListItem Text="Yes" Value="True" />
                                </asp:DropDownList>
                            </div>
                            <div class="formSectionEnd">
                            </div>
                            <div class="formButton">
                                <asp:Button ID="btnUpdateProfile" runat="server" CssClass="inputButton" 
                                    OnClick="btnUpdateProfile_Click" Text="Update Profile" 
                                    ValidationGroup="EditProfile" />
                                <asp:Button ID="btnDeleteProfile" runat="server" CssClass="inputButton" 
                                    OnClick="btnDeleteProfile_Click" 
                                    OnClientClick="return confirm('Are Your Sure?')" Text="Delete Profile" />
                                &#160;
                                <asp:Label ID="lblProfileMessage" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Change Password">
                    <HeaderTemplate>
                        Change Password
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="contentTemplate">
                            <div class="formSectionTitle2">
                                CHANGE PASSWORD:
                            </div>
                            <div class="formLabelsText">
                                New Password:<br />
                                <asp:TextBox ID="PasswordTextbox" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequiredValidator" runat="server" ControlToValidate="PasswordTextbox" Display="Dynamic" ErrorMessage="Required" ValidationGroup="changepassword"></asp:RequiredFieldValidator>
                            </div>
                            <div class="formLabelsText">
                                Confirm New Password:<br />
                                <asp:TextBox ID="PasswordConfirmTextbox" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordConfirmRequiredValidator" runat="server" ControlToValidate="PasswordConfirmTextbox" Display="Dynamic" ErrorMessage="Required" ValidationGroup="changepassword"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="PasswordConfirmCompareValidator" runat="server" ControlToCompare="PasswordTextBox" ControlToValidate="PasswordConfirmTextbox" Display="Dynamic" ErrorMessage="NEW password must match CONFIRM password." ValidationGroup="changepassword"></asp:CompareValidator>
                            </div>
                            <div>
                                <asp:Button ID="ChangePasswordButton" CssClass="inputButton" runat="server" OnClick="ChangePassword_OnClick" Text="Change Password" ValidationGroup="changepassword" />
                            </div>
                            <div class="formSectionEnd">
                            </div>
                            <div class="formSectionTitle2">
                                CHANGE PASSWORD Q AND A
                            </div>
                            <div class="formLabelsText">
                                Password:<br />
                                <asp:TextBox ID="qaCurrentPassword" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="qaCurrentPassword" ErrorMessage="Required" ValidationGroup="changePasswordQA"></asp:RequiredFieldValidator>
                            </div>
                            <div class="formLabelsText">
                                New Pw. Question:<br />
                                <asp:TextBox ID="qaNewQuestion" runat="server" MaxLength="256" Width="140px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="QuestionRequiredValidator" runat="server" ControlToValidate="qaNewQuestion" ErrorMessage="Required" ValidationGroup="changePasswordQA"></asp:RequiredFieldValidator>
                            </div>
                            <div class="formLabelsText">
                                New Pw. Answer:<br />
                                <asp:TextBox ID="qaNewAnswer" runat="server" MaxLength="128" Width="140px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequiredValidator" runat="server" ControlToValidate="qaNewAnswer" ErrorMessage="Required" ValidationGroup="changePasswordQA"></asp:RequiredFieldValidator>
                            </div>
                            <div>
                                <asp:Button ID="ChangePasswordQuestionButton" CssClass="inputButton" runat="server" OnClick="ChangePasswordQuestion_OnClick" Text="Change Q. and A." ValidationGroup="changePasswordQA" />
                            </div>
                            <div>
                                <asp:Label ID="Msg" runat="server" ForeColor="Maroon"></asp:Label>
                            </div>
                        </div>
                    </ContentTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer>
            <br />
            <%-- ajax update panel end --%>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<%-- help sidebar --%>
<div id="helpSidebarShow" class="helpSidebarShow">
    <a onclick="ShowHide(); return false;" href="#">H<br />
        I<br />
        N<br />
        T </a>
</div>
<div id="helpSidebar" class="helpSidebar" style="display: none;">
    <span class="helpSidebarClose"><a onclick="ShowHide(); return false;" href="#">CLOSE</a> </span>
    <div class="clearBoth2">
    </div>
    <div class="helpHintIcon">
    </div>
    <div>
        <asp:Repeater ID="rptHelp" runat="server" DataSourceID="xmlHelp">
            <ItemTemplate>
                <div class="helpTitle">
                    <asp:Literal ID="ltlTitle" runat="server" Text='<%#XPath("title")%>'></asp:Literal>
                </div>
                <div class="helpText">
                    <asp:Literal ID="ltlText" runat="server" Text='<%#XPath("text")%>'></asp:Literal>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:XmlDataSource ID="xmlHelp" runat="server" DataFile="~/admin/help/edit-user-modal.xml"></asp:XmlDataSource>
    </div>
</div>
<%-- sidebar help js --%>
<uc3:js ID="js3" runat="server" />
<%-- jquery js --%>
<uc4:jquery ID="jquery1" runat="server" />
