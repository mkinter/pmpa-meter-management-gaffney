﻿using System;
using System.Data;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class admin_controls_search_membership_users : System.Web.UI.UserControl
{
    #region PageLoad events

    protected void Page_Load(object sender, EventArgs e)
    {
        UserGvUtil.AssignConfirmMessagesToDdls(ddlAddUsersToRole, ddlAddAllUsersToRole, ddlRemoveAllUsersFromRole, ddlRemoveUsersFromRole, ddlDeleteAllUsersFromRole);
        if (!Page.IsPostBack)
        {
            ddlPageSize2.SelectedValue = GridView2.PageSize.ToString();
        }
    }

    #endregion

    #region DELETE selected users - button

    protected void btnDeleteSelected_Click(object sender, EventArgs e)
    {
        UserGvUtil.DeleteSelectedUsers(GridView2, Msg);
    }

    #endregion

    #region DELETE ALL all users and ALL related DATA - button

    protected void deleteAllUsers_Click(object sender, EventArgs e)
    {
        UserGvUtil.DeleteAllUsersAndAllRelatedData(Msg, GridView2);
    }

    #endregion

    #region DELETE ALL users and ALL related DATA present in selected ROLE - dropdownlist

    protected void ddlDeleteAllUsersFromRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserGvUtil.DeleteAllUsersAndRelatedInfoPresentInSelectedRole(Msg, ddlDeleteAllUsersFromRole, ddlRemoveUsersFromRole, ddlAddUsersToRole, ddlAddAllUsersToRole, ddlRemoveAllUsersFromRole, GridView2);
    }

    #endregion




    #region NAVIGATION and PAGING - Gridview

    #region methods

    // gridview sending request
    private GridView GetGridView(object sender)
    {
        return UserGvUtil.GetGridView(sender);
    }

    #endregion
    
    // go to page number typed into the textbox
    protected void GoToPage_TextChanged(object sender, EventArgs e)
    {
        UserGvUtil.GoToPageNumber(sender, GridView2);
    }

    // setup gridview sorting and row highlight to work with css
    

    #endregion

    #region ADD selected users TO selected ROLE - dropdownlist

    protected void ddlAddUsersToRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserGvUtil.AddSelectedUsersToSelectedRole2(GridView2, ddlAddUsersToRole, Msg, ddlRemoveUsersFromRole, ddlAddAllUsersToRole, ddlRemoveAllUsersFromRole, ddlDeleteAllUsersFromRole);
    }

    #endregion

    #region ADD ALL users TO selected ROLE - dropdownlist

    protected void ddlAddAllUsersToRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserGvUtil.AddAllUsresToSelectedRole2(Msg, ddlAddAllUsersToRole, ddlRemoveUsersFromRole, ddlAddUsersToRole, ddlRemoveAllUsersFromRole, ddlDeleteAllUsersFromRole);
    }

    #endregion

    #region REMOVE selected users FROM selected ROLE - dropdownlist

    protected void ddlRemoveUsersFromRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserGvUtil.RemoveSelectedUsersFromSelectedRole2(GridView2, Msg, ddlRemoveUsersFromRole, ddlAddUsersToRole, ddlAddAllUsersToRole, ddlRemoveAllUsersFromRole, ddlDeleteAllUsersFromRole);
    }

    #endregion

    #region REMOVE ALL users FROM selected ROLE - dropdownlist

    protected void ddlRemoveAllUsersFromRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserGvUtil.RemoveAllUsersFromSelectedRole2(Msg, ddlRemoveUsersFromRole, ddlAddUsersToRole, ddlAddAllUsersToRole, ddlRemoveAllUsersFromRole, ddlDeleteAllUsersFromRole);
    }

    #endregion

    #region REMOVE ALL users from ALL ROLES - button

    protected void btnRemoveAllUsersFromAllRoles_Click(object sender, EventArgs e)
    {
        UserGvUtil.RemoveAllUsersFromAllRoles2(Msg);
    }

    #endregion

    #region APPROVE selected users - button

    protected void btnApproveSelected_Click(object sender, EventArgs e)
    {
        UserGvUtil.ApproveSelectedUsers2(GridView2, Msg);
    }

    #endregion

    #region APPROVE ALL users - button

    protected void btnApproveAllUsers_Click(object sender, EventArgs e)
    {
        UserGvUtil.ApproveAllUsers2(Msg, GridView2);
    }

    #endregion

    #region UNAPPROVE selected users - button

    protected void btnUnApproveSelected_Click(object sender, EventArgs e)
    {
        UserGvUtil.UnapproveSelectedUsers2(GridView2, Msg);
    }

    #endregion

    #region UNAPPROVE ALL users -button

    protected void btnUnapproveAllUsers_Click(object sender, EventArgs e)
    {
        UserGvUtil.UnapproveAllUsers2(Msg, GridView2);
    }

    #endregion

    #region UNLOCK selected users - button

    protected void btnUnlockSelected_Click(object sender, EventArgs e)
    {
        UserGvUtil.UnlockSelectedUsers2(GridView2, Msg);
    }

    #endregion

    #region UNLOCK ALL users - button

    protected void btnUnlockAllUsers_Click(object sender, EventArgs e)
    {
        UserGvUtil.UnlockAllUsers2(Msg, GridView2);
    }

    #endregion
    protected void GridView2_DataBound(object sender, EventArgs e)
    {
        pnlHideItems.Visible = GridView2.Rows.Count > 0;
        if (GridView2.BottomPagerRow == null)
            return;
        ((TextBox)GridView2.BottomPagerRow.FindControl("txtGoToPage")).Text = (GridView2.PageIndex + 1).ToString();
        ((Label)GridView2.BottomPagerRow.FindControl("lblTotalNumberOfPages")).Text = GridView2.PageCount.ToString();
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UserGvUtil.HighlightRowOnClick(e);
        UserGvUtil.HighlightSortedColumn(sender, e);
        UserGvUtil.DisplayAndRememberPageSize(sender, e);
    }
    protected void GridView2_PreRender(object sender, EventArgs e)
    {
        this.GridView2.Controls[0].Controls[this.GridView2.Controls[0].Controls.Count - 1].Visible = true;
    }
    protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView2.PageSize = Int32.Parse(ddlPageSize2.SelectedValue.ToString());
        GridView2.PageIndex = 0;
        GridView2.DataBind();
        if (GridView2.BottomPagerRow == null)
            return;
        ((Label)GridView2.BottomPagerRow.FindControl("lblTotalNumberOfPages")).Text = GridView2.PageCount.ToString();
        ((TextBox)GridView2.BottomPagerRow.FindControl("txtGoToPage")).Text = (GridView2.PageIndex + 1).ToString();
    }
}