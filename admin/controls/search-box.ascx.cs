﻿using System;

public partial class admin_controls_search_box : System.Web.UI.UserControl
{
    #region page_load

    protected void Page_Load(object sender, EventArgs e)
    {
        // add focus to username textbox
        txbUserName.Focus();
        if (String.IsNullOrEmpty(txbUserName.Text) && String.IsNullOrEmpty(txbEmail.Text) &&
            String.IsNullOrEmpty(tbCustName.Text) && String.IsNullOrEmpty(tbCustNbr.Text))
        {
            txbUserName.Text = Request.QueryString["UserName"] != null ? Request.QueryString["UserName"].ToString() : "";
            txbEmail.Text = Request.QueryString["Email"] != null ? Request.QueryString["Email"].ToString() : "";
            tbCustName.Text = Request.QueryString["CustName"] != null ? Request.QueryString["CustName"].ToString() : "";
            tbCustNbr.Text = Request.QueryString["CustNbr"] != null ? Request.QueryString["CustNbr"].ToString() : "";
        }
    }

    #endregion

    #region SEND Search Query - button

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txbUserName.Text) || !String.IsNullOrEmpty(txbEmail.Text) ||
            !String.IsNullOrEmpty(tbCustName.Text) || !String.IsNullOrEmpty(tbCustNbr.Text))
        {
            string UserName = txbUserName.Text.Trim();
            string Email = txbEmail.Text.Trim();

            Response.Redirect("search-user.aspx" + "?UserName=" + UserName + "&Email=" + Email + "&CustName=" + tbCustName.Text.Trim()
                +"&CustNbr=" + tbCustNbr.Text.Trim());
        }
        else
        {
            Msg2.Text = "Please enter a search term...";
            Msg2.Visible = true;
        }
    }

    #endregion
}