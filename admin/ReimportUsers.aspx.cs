﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReimportUsers : System.Web.UI.Page
{
    private string pwd = "Pa$$w0rd";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (tbPass.Text == pwd)
        {
            lblError.Text = "";
            App_eBilling.ReimportUsers();
        }
        else
            lblError.Text = "Wrong password";
    }
}