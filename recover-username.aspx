﻿
<%@ Page Title="Username Recovery" Language="C#" MasterPageFile="~/themes/default/default.master" AutoEventWireup="true" CodeFile="recover-username.aspx.cs" Inherits="recover_username" EnableViewState="false" %>
  <%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>Username Recovery</h3>
    <asp:Panel ID="pnlEnter" runat="server">
    <p>Enter E-mail Address:*</p>
    <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
    <br />
    <div runat="server" id="pbTarget" visible="false"></div>
    <p>Enter Security Code:*</p>
    <recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    PublicKey="6LdL-90SAAAAABpYYdt8DQNuFYEd60FIDskg2754"
    PrivateKey="6LdL-90SAAAAAKn0cRJdwFt9kEBBhC3z_mIkcBHH"
    />
    <br />
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
            onclick="btnSubmit_Click" />
            <br/>
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
        <asp:Label ID="lblConfirm" runat="server" Text="Thank you, please check your inbox to complete the username recovery."></asp:Label>
    </asp:Panel>
</asp:Content>
