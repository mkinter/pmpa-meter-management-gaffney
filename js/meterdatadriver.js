﻿var webservicePrefix = '/Meters/MeterService.asmx';
var GetChartDataURL = ResolveUrl(webservicePrefix + '/GetChartData');
var GetChartDataByGroupIdURL = ResolveUrl(webservicePrefix + '/GetChartDataByMeterGroupId');
var GetReadingsByGroupIdURL = ResolveUrl(webservicePrefix + '/GetReadingsByMeterGroupId');
var GetMultipliersByGroupURL = ResolveUrl(webservicePrefix + '/GetMultipliersByGroup');
var progressBarTimerInterval = 100;

var chart;
function ResolveUrl(url) {
    return url.replace("~/", baseUrl);
}



$(function () {
    var progressTime;
    var progressbar = $("#progressbar");

    $('#multipliers').hide();
    $('#divDetailLoading').hide();
    $('#divDrillDownHeader').hide();
    $("#progressbar").progressbar({
        change: function () {
            var value = $("#progressbar").progressbar("value") + "%";
            $('#txt_loading').text(value);
        }
    });

    //var ready = 0;
    //ShowLoading();
    //if ($("#ddl_meter option[value='PelMed1']").length > 0)
    //    $("#ddl_meter").val('PelMed1');
    //FetchChartData();
    FetchChartDataByGroupId();

    //var groupId = $("#ddl_meter").val();

});

function FetchChartDataByGroupId() {
    var groupId = $("#ddl_meter").val();
    var groupName = $('#ddl_meter :selected').text();
    var companyId = $('#hfCompanyId').val();

    $.ajax({
        type: "GET",
        url: GetChartDataByGroupIdURL,
        data: {
            companyId: companyId,
            groupId: groupId
        },
        beforeSend: function () {
            ShowLoading();
        },
        complete: function () {
            HideLoading();
        },
        dataType: "html",
        error: function (data) {
            var tmp = data;
        },
        success: function (data) {

            var x = $.parseXML(data);
            var months = [], drilldown = [];

            $(x).find("MonthData").each(function () {
                current = parseInt($(this).text());
                var tmpMonthTotalValue = $(this).find("MTotal").first().text();
                var tmpMonthName = $(this).find("MName").first().text();
                var tmpMonthYear = $(this).find("MYear").first().text();
                var monthDrillDownID = tmpMonthName + 'days' + tmpMonthYear;

                months.push({
                    name: tmpMonthName + ' ' + tmpMonthYear,
                    month: tmpMonthName,
                    year: tmpMonthYear,
                    tooltipName: tmpMonthName,
                    y: parseFloat(tmpMonthTotalValue),
                    drilldown: monthDrillDownID,
                    level: 1,
                    color: '#7CB5EC'
                });
                var tmpDayData = [];
                $(this).find("DD").each(function () {
                    var tmpDayTotalValue = $(this).find("DTotal").first().text();
                    var tmpDayName = $(this).find("DName").first().text();
                    var tmpDayDrilldownID = tmpMonthName + tmpDayName + 'hours';

                    tmpDayData.push({
                        name: tmpDayName,
                        y: parseFloat(tmpDayTotalValue),
                        tooltipName: tmpMonthName + ' ' + getGetOrdinal(parseInt(tmpDayName)),
                        level: 2,
                        drilldown: tmpDayDrilldownID
                    });
                    var tmpHourData = [];
                    $(this).find("HD").each(function () {
                        var tmpHourValue = $(this).find("v").first().text();
                        var tmpHourName = hoursToAMPM(parseFloat($(this).find("h").first().text()));

                        tmpHourData.push({
                            name: tmpHourName,
                            y: parseFloat(tmpHourValue),
                            level: 3,
                            tooltipName: tmpHourName
                        });

                    });
                    drilldown.push({
                        id: tmpDayDrilldownID,
                        data: tmpHourData
                    });

                });

                drilldown.push({
                    id: monthDrillDownID,
                    name: tmpMonthName,
                    data: tmpDayData
                })
            });


            initHighCharts(months, drilldown, groupName);

            GetMultipliers();


        }
    });
}

function GetMultipliers()
{
    var groupId = $("#ddl_meter").val();
    var companyId = $('#hfCompanyId').val();

    $.ajax({
        type: "GET",
        url: GetMultipliersByGroupURL,
        data: {
            companyId: companyId,
            groupId: groupId
        },
        dataType: "html",
        error: function (data) {
            var tmp = data;

            $('#multipliers').html('');
            $('#multipliers').hide();
        },
        success: function (data) {

            var x = $.parseXML(data);

            var meterNum = '';
            var multiplier = '';

            var html = "<table><tr>"
            $(x).find("MeterMultiplier").each(function () {
                
                meterNum = $(this).find("MeterNumber").first().text();
                multiplier = $(this).find("Multiplier").first().text();

                html += "<tr><td>" + meterNum + "</td><td>" + multiplier + "</td></tr>";
            });
            html += "</table>";

            $('#multipliers').html(html);
            $('#multipliers').show();
        }
    });
}

function ShowLoading() {

    $("#progressbar").progressbar({
        value: 0
    });

    $('#loading').show();
    $('#divDisclaimer').hide();
    //setInterval(UpdateLoadingMessage, 750);

    progressTimer = setTimeout(Progress, progressBarTimerInterval);
    //$('#progressbar').show();
}

function Progress() {
    var val = $("#progressbar").progressbar("value") || 0;

    if (val < 95) {
        $("#progressbar").progressbar("value", val + 5);
        progressTimer = setTimeout(Progress, progressBarTimerInterval);
    }
}

function CloseProgressTimer() {
    clearTimeout(progressTimer);
    //$('#progressbar').hide();
}

var loadingMsgSuffix = '...';
var selectedMonth = '';
var selectedDay = '';
var selectedYear = '';
var currentLevel = 0;

function HideLoading() {
    CloseProgressTimer();
    $('#divDisclaimer').show();
    $('#loading').hide();
}
function btnUpdate_onclick() {

    $('#divDetailLoading').hide();
    $('#readResults').html('');
    $('#divDrillDownHeader').hide();

    $('#container').highcharts().destroy();
    //FetchChartData();
    FetchChartDataByGroupId();
}
function UpdateLoadingMessage() {

    switch (loadingMsgSuffix.length) {
        case (0):
            loadingMsgSuffix = '.';
            break;
        case (1):
            loadingMsgSuffix = '..';
            break;
        case (2):
            loadingMsgSuffix = '...';
            break;
        default:
            loadingMsgSuffix = '';
            break;
    }
    $('#txt_loading').text(loadingMsgSuffix);
}
function initHighCharts(months, drilldown, groupName) {
    // Create the chart
    var chart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'container',
            events: {
                drilldown: function (e) {
                    if (e.point.level === 1) {
                        selectedMonth = e.point.month;
                        selectedYear = e.point.year;
                        currentLevel++;

                        chart.setTitle({ text: groupName }, { text: selectedMonth + ' ' + selectedYear });

                    }
                    else if (e.point.level === 2) {
                        selectedDay = e.point.name;
                        chart.setTitle({ text: groupName }, { text: selectedMonth + ' ' + selectedDay + ', ' + selectedYear });
                        currentLevel++;
                    }


                    true;
                },
                drillup: function (e) {
                    if (currentLevel === 2)
                        chart.setTitle({ text: groupName }, { text: selectedMonth + ' ' + selectedYear });
                    else
                        chart.setTitle({ text: groupName }, { text: 'Last 13 Months' });

                    currentLevel--;
                }
            }
        },
        credits: {
            enabled: false
        },
        //title: {
        //    text: 'Group: ' + groupName
        //},
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'kW'
            }
        },

        legend: {
            enabled: false
        },

        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                },
                point: {
                    events: {
                        click: function (e) {
                            if (e.point.level === 3) {
                                var now = new Date();
                                var localOffset = (-1) * now.getTimezoneOffset() * 60000;

                                var hourIndex = e.point.index;
                                var eDate = new Date(getMonth(selectedMonth) + '/' + selectedDay + '/' + selectedYear + ' ' + getFormattedTime(hourIndex));
                                var sDate = new Date(eDate);
                                sDate.setHours(eDate.getHours() - 1);

                                getReadDetails($("#ddl_meter").val(), Math.round(new Date(sDate.getTime() + localOffset).getTime()), Math.round(new Date(eDate.getTime() + localOffset).getTime()), 5, sDate, eDate);
                            }

                            true;
                        }
                    }
                }
            }
        },
        tooltip: {

            shared: true,
            useHTML: true,

            formatter: function () {
                var s = '';

                s = '<span><b>' + this.points[0].point.tooltipName + '</b></span><table>';

                $.each(this.points, function () {
                    s += '<tr>' +
                        '<td><b>' + this.y + ' kW' + '</b></td></tr>';
                });


                return s + '</table>';
            }
        },
        series: [{
            name: 'Months',
            colorByPoint: true,
            data: months
        }],
        drilldown: {
            series: drilldown,
            drillUpButton: {
                position: {
                    y: -29
                }
            }
        }

    });

    chart.setTitle({ text: $('#ddl_meter :selected').text() }, { text: 'Last 13 Months' });
}

function getReadDetails(groupId, sDate, eDate, interval, stringStart, stringEnd) {
    var companyId = $('#hfCompanyId').val();

    $.ajax({
        type: "GET",
        url: GetReadingsByGroupIdURL,
        data: {
            companyId: companyId,
            groupId: groupId,
            sDate: sDate,
            eDate: eDate,
            interval: interval
        },
        dataType: "html",
        beforeSend: function () {
            $('#divDrillDownHeader').hide();
            $('#divDetailLoading').show();
        },
        error: function (result) {
            var tmp = result;
            $('#divDetailLoading').hide();
            $('#readResults').html('');
            $('#lblLoadingDetails').text('No details available at this time.');
        },
        success: function (result) {

            if (result !== null) {
                var rowCounter = 0;
                var data = $.parseXML(result);

                // Build read results table
                var table = "<table class='table'>";
                table += "<tr><th>Reading Date</th><th>Demand</th><th>Read</th><th>Prev. Read</th><th>Prev. Date</th><th>Usage</th></tr>";
                $(data).find("MeterReadDetails").each(function () {

                    table += "<tr>";
                    table += "<td>" + $(this).find("ReadingDate").first().text(); + "</td>";
                    table += "<td>" + $(this).find("Demand").first().text(); + "</td>";
                    table += "<td>" + $(this).find("PosKWh").first().text(); + "</td>";
                    table += "<td>" + $(this).find("PrevPosKWh").first().text(); + "</td>";
                    table += "<td>" + $(this).find("PrevPosKWhReadingDate").first().text(); + "</td>";
                    table += "<td>" + $(this).find("Usage").first().text(); + "</td>";
                    table += "</tr>";
                    rowCounter++;
                });
                table += "</table>";

                if (rowCounter > 0)
                    $('#lblDDHeader').text('Reading Details for ' + stringStart.toLocaleString() + ' to ' + stringEnd.toLocaleString());
                else
                    $('#lblDDHeader').text('NO Reading Details for ' + stringStart.toLocaleString() + ' to ' + stringEnd.toLocaleString());

                $('#divDetailLoading').hide();
                $('#readResults').html('');
                $('#readResults').append(table);
                $('#divDrillDownHeader').show();
            }
            else {
                $('#divDetailLoading').hide();
                $('#readResults').html('');
                $('#lblLoadingDetails').text('No details available.');
            }
        }

    });
}

function convertMonthNameToNumber(monthName) {
    var myDate = new Date(monthName + " 1, 2000");
    var monthDigit = myDate.getMonth();
    return isNaN(monthDigit) ? 0 : (monthDigit + 1);
}

function getGetOrdinal(n) {
    var s = ["th", "st", "nd", "rd"],
        v = n % 100;
    return n + (s[(v - 20) % 10] || s[v] || s[0]);
}

function hoursToAMPM(hours) {
    var suffix = hours >= 12 ? "PM" : "AM";
    hours = ((hours + 11) % 12 + 1) + ' ' + suffix;
    return hours;
}

function getMonth(monthStr) {
    return new Date(monthStr + '-1-01').getMonth() + 1
}

function getFormattedTime(timeIndex) {
    var hours = ((timeIndex + 11) % 12) + 1;
    var amPm = timeIndex > 11 ? 'PM' : 'AM';
    return hours + ':00 ' + amPm;
}


function closeDetails() {
    $('#lblDDHeader').text('');
    $('#readResults').html('');
    $('#divDrillDownHeader').hide();
}
