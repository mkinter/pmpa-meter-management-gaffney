﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" MasterPageFile="~/themes/default/default.master"
    Inherits="ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
    <title>Reset Password</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    <br />
    <asp:Panel ID="pnlUpdatePassword" runat="server" Visible="false">
        <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblUser" runat="server" Text="Label"></asp:Label></td>
        </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Enter New Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbPassword1" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Confirm Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbPassword2" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:Button ID="Button1" runat="server" Text="Change Password" 
            onclick="Button1_Click" />
        <br />
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
    
    <asp:Label ID="lblConfirm" runat="server"></asp:Label>
    </asp:Panel>
</asp:Content>
