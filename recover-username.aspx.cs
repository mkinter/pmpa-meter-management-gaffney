﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class recover_username : System.Web.UI.Page
{
    protected string FromEmailAddress = "do_not_reply@eastonutilities.com"; // from email used for new credentials

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            List<User> usernames = App_eBilling_Customer_Data.getUsernames(tbEmail.Text);
            if (usernames.Count > 0)
            {
                SendEmail(usernames, tbEmail.Text);
            }
            else
                lblError.Text = "No accounts associated with this e-mail address.";
        }
        else
        {

            pbTarget.Visible = true;  
            ScriptManager.RegisterClientScriptBlock(  
                recaptcha,  
                recaptcha.GetType(),  
                "recaptcha",  
                "Recaptcha._init_options(RecaptchaOptions);"  
                + "if ( RecaptchaOptions && \"custom\" == RecaptchaOptions.theme )"  
                + "{"  
                + "  if ( RecaptchaOptions.custom_theme_widget )"  
                + "  {"  
                + "    Recaptcha.widget = Recaptcha.$(RecaptchaOptions.custom_theme_widget);"  
                + "    Recaptcha.challenge_callback();"  
                + "  }"  
                + "} else {"  
                + "  if ( Recaptcha.widget == null || !document.getElementById(\"recaptcha_widget_div\") )"  
                + "  {"  
                + "    jQuery(\"#" + pbTarget.ClientID + "\").html('<div id=\"recaptcha_widget_div\" style=\"display:none\"></div>');"  
                + "    Recaptcha.widget = Recaptcha.$(\"recaptcha_widget_div\");"  
                + "  }"  
                + "  Recaptcha.reload();"  
                + "  Recaptcha.challenge_callback();"  
                + "}",  
                true  
            );
            lblError.Text = "Security code incorrect, please try again";
            
        }
    }

    private void SendEmail(List<User> usernames, string emailAddress)
    {
       /*
        string userNameHTMLTable = "<table><tr><th>Username</th><th>Customer Number</th></tr>";//<tr><td>1234</td><td>1234</td></tr></table>";
        foreach (User user in usernames)
        {
            userNameHTMLTable = string.Format("{0}<tr><td>{1}</td><td>{2}</td></tr>", userNameHTMLTable, user.username, user.custNbr);
        }
        userNameHTMLTable = string.Format("{0}</table>", userNameHTMLTable);
        // send new credentials to user via EMAIL
        try
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            // determine the site's URL so we can use it in the email
            string urlBase = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;


            string eEmail = emailAddress;
            string eFrom = FromEmailAddress;
            string eFromDisplayName = "Site Administrator";
            string eSubject = "Username Request! - Your usernames associated with this e-mail address.";

            message.To.Add(eEmail);
            message.From = new MailAddress(eFrom, "Username Recovery");
            message.Body = string.Format("<p>Hello,</p><p>Your request to retrieve your username has been received.<p>The username associated with this e-mail address is as follows: {0}<br/><br/> Thank You!<br/>Webmaster.</p>", userNameHTMLTable);
            message.Subject = eSubject;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;

            // cool! we only have declare the smtp. System.Net.Mail automatically finds it in web.config
            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;
            client.Send(message);

            

        }
        catch (Exception ex)
        {
            lblError.Text += " ERROR Sending Mail: " + ex.Message;
            lblError.Visible = true;
        }
        finally
        {
            // do nothing
        }*/
        string userNameHTMLTable = "<table><tr><th>Username</th><th>Customer Number</th></tr>";//<tr><td>1234</td><td>1234</td></tr></table>";
        foreach (User user in usernames)
        {
            userNameHTMLTable = string.Format("{0}<tr><td>{1}</td><td>{2}</td></tr>", userNameHTMLTable, user.username, user.custNbr);
        }
        userNameHTMLTable = string.Format("{0}</table>", userNameHTMLTable);
        try
        {
            // get default email address from web.config
            string AdminEmail = FromEmailAddress;
            //string emailToWebConfig = AdminEmail.ToString();

            // create timestamp
            string timeStamp = DateTime.Now.ToString("F");

            // send mail
            System.Net.Mail.MailMessage MyMailer = new System.Net.Mail.MailMessage();
            MyMailer.To.Add(emailAddress);
            MyMailer.From = new MailAddress(AdminEmail, "Username Recovery");
            MyMailer.Subject = "Username Request! - Your usernames associated with this e-mail address.";
            /*
            MyMailer.Body = "Dear Administrator, <br/><br/> You have recieved a New Account Registration Notification. <br/> Please login to your web site administration panel and visit the users a-z section under the user accounts menu.";*/
            MyMailer.Body = string.Format("Dear {0}, <br/><br/> Your request to retrieve your username has been received. This request has been created through the Easton Utilities' My Account 'Username Recovery' option. <br/><br/>The usernames associated with this e-mail address are as follows: {1} <br/>Please login at https://olp1.eastonutilities.com to access your account.<br/><br/> Thank You!<br/>Easton Utilities</p>", emailAddress, userNameHTMLTable);
            MyMailer.IsBodyHtml = true;
            MyMailer.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;
            client.Send(MyMailer);

            pnlEnter.Visible = false;
            pnlConfirm.Visible = true;
        }
        catch(Exception ex)
        {
            lblError.Text += " ERROR Sending Mail: " + ex.Message;
            lblError.Visible = true;
        }
    }
}