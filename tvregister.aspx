﻿<%@ Page Debug="true" Title="Create an Account" Language="C#" MasterPageFile="~/themes/default/tv.master" AutoEventWireup="true" CodeFile="tvregister.aspx.cs" Inherits="register" %>
<%@ Register Src="controls/register-with-role.ascx" TagName="RegisterWithRole" TagPrefix="uc1" %>
<%@ Register Src="controls/register-with-role-and-profile.ascx" TagName="RegisterWithRoleAndProfile" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:RegisterWithRoleAndProfile ID="RegisterWithRole1" runat="server" />
</asp:Content>
