﻿using System.Web.UI.WebControls;
using System.Configuration;
using System;
using Utilities.Logging;
using System.Web.UI.HtmlControls;
using System.Web.UI;

public partial class controls_recover_password : System.Web.UI.UserControl
{
    #region page_load - set focus to user name textbox

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // find user name textbox in UserName Template Container
            TextBox UserName = PasswordRecovery1.UserNameTemplateContainer.FindControl("UserName") as TextBox;
            UserName.Focus();
            if (Request.QueryString["Username"] != null)
                UserName.Text = Request.QueryString["Username"].ToString();
        }
    }

    #endregion
    
    #region validate user name

    protected void PasswordRecovery1_UserLookupError(object sender, System.EventArgs e)
    {
        // Show the error message                
        FailureText.Text = "User Name is INCORRECT! Please try again.<div class='clearBoth2'></div>";
        FailureText.Visible = true;
    }

    #endregion

    #region validate captcha input

    protected void PasswordRecovery1_VerifyingUser(object sender, LoginCancelEventArgs e)
    {
        
        /*
        // find captcha control
        WebControlCaptcha.CaptchaControl registerCAPTCHA = (WebControlCaptcha.CaptchaControl)PasswordRecovery1.UserNameTemplateContainer.FindControl("CAPTCHA");

        // if captcha is missing or incorrect
        if (!registerCAPTCHA.UserValidated)
        {
            // Show the error message                
            FailureText.Text = "Security Code MISSING or INCORRECT!<div class='clearBoth2'></div>";
            FailureText.Visible = true;

            // Cancel the transaction                
            e.Cancel = true;
        }*/
        Recaptcha.RecaptchaControl captcha = (Recaptcha.RecaptchaControl)PasswordRecovery1.UserNameTemplateContainer.FindControl("recaptcha");
        HtmlGenericControl pbTarget = (HtmlGenericControl)PasswordRecovery1.UserNameTemplateContainer.FindControl("pbTarget");
        captcha.Validate();
        if (!captcha.IsValid)
        {

            pbTarget.Visible = true;
            ScriptManager.RegisterClientScriptBlock(
                captcha,
                captcha.GetType(),
                "recaptcha",
                "Recaptcha._init_options(RecaptchaOptions);"
                + "if ( RecaptchaOptions && \"custom\" == RecaptchaOptions.theme )"
                + "{"
                + "  if ( RecaptchaOptions.custom_theme_widget )"
                + "  {"
                + "    Recaptcha.widget = Recaptcha.$(RecaptchaOptions.custom_theme_widget);"
                + "    Recaptcha.challenge_callback();"
                + "  }"
                + "} else {"
                + "  if ( Recaptcha.widget == null || !document.getElementById(\"recaptcha_widget_div\") )"
                + "  {"
                + "    jQuery(\"#" + pbTarget.ClientID + "\").html('<div id=\"recaptcha_widget_div\" style=\"display:none\"></div>');"
                + "    Recaptcha.widget = Recaptcha.$(\"recaptcha_widget_div\");"
                + "  }"
                + "  Recaptcha.reload();"
                + "  Recaptcha.challenge_callback();"
                + "}",
                true
            );
            // Show the error message                
            FailureText.Text = "Security Code MISSING or INCORRECT!<div class='clearBoth2'></div>";
            FailureText.Visible = true;

            // Cancel the transaction                
            e.Cancel = true;
        }
    }

    #endregion
    
    #region send carbon copy to admin

    // this code send a carbon copy CC email to the webmaster
    protected void PasswordRecovery1_SendingMail(object sender, MailMessageEventArgs e)
    {
        //e.Cancel = true;
        string token = GenerateToken();
        e.Message.Body = string.Format(@"Your password has been reset, {1}!
<br/><br/>
According to our records, you have requested to reset your password. 
<br/><br/>
Please click this link to create a new password for your account.
<a href='{0}'>{0}</a>
<br/><br/>
If you have any questions or trouble logging on please contact a site administrator.
<br/><br/>
Thank you!", token, PasswordRecovery1.UserName);
        LogWriter.Write(string.Format("Password Recovery Email sent for user: {0}", e.Message.To.ToString()));

        e.Message.IsBodyHtml = true;
    }



    #endregion

    #region custom code routine to generate password recovery token
    protected string GenerateToken()
    {
        string URL = "https://olp1.eastonutilities.com/ResetPassword.aspx";
        //string URL = "http://localhost:58656/eBillingEU/ResetPassword.aspx";
        string token = Guid.NewGuid().ToString();
        while (!App_eBilling_Customer_Data.IsValidateGUID(token))
        {
            token = Guid.NewGuid().ToString();
        }
        bool success = App_eBilling_Customer_Data.AddToken(PasswordRecovery1.UserName, token);

        LogWriter.Write(string.Format("Password Recovery token generated for user: {0}", PasswordRecovery1.UserName));
        return string.Format("{0}?token={1}", URL, token);
    }


    #endregion

}
