﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web;
using System.Configuration;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class controls_register_with_role_and_profile : System.Web.UI.UserControl
{
    //-------------------------------------------------------------------------------------------------------
    protected string wsatDefaultRole = "Member"; // default Role for membership
    protected string wsatDefaultRoleMissing = "missing_default_role@eastonutilities.com"; // from email to notify admin
    protected string wsatNewRegistration = "new_account_registration@eastonutilities.com"; // from email to notify admin
    protected string passwordComplexityRegex = @"(?=^.{5,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";
    //-------------------------------------------------------------------------------------------------------
    
    #region PageLoad - Check if user is logged in and disable form if true

    protected void Page_Load(object sender, EventArgs e)
    {
        // if current user is logged in
        if (!HttpContext.Current.User.Identity.IsAuthenticated)
            return;

        // disable form
        CreateUserWizard1.Enabled = false;

        // display message
        InvalidUserNameOrPasswordMessage.Text = "To create a new account, please Logout first...";
        InvalidUserNameOrPasswordMessage.Visible = true;
    }

    #endregion

    #region Validate CAPTCHA, check for leading/trailing spaces in user name

    // this code checks for leading and trailing spaces in username
    // and makes sure the username does not appear in the password
    protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
    {
        /*
        // find captcha control
        WebControlCaptcha.CaptchaControl registerCAPTCHA = (WebControlCaptcha.CaptchaControl)CreateUserWizardStep1.ContentTemplateContainer.FindControl("CAPTCHA");
        
        if (!registerCAPTCHA.UserValidated)
        {
            // Show the error message                
            InvalidUserNameOrPasswordMessage.Text = "Security Code MISSING or INCORRECT!";
            InvalidUserNameOrPasswordMessage.Visible = true;

            // Cancel the create user workflow                
            e.Cancel = true;
        }*/
        Recaptcha.RecaptchaControl captcha = (Recaptcha.RecaptchaControl)CreateUserWizardStep1.ContentTemplateContainer.FindControl("recaptcha");
        HtmlGenericControl pbTarget = (HtmlGenericControl)CreateUserWizardStep1.ContentTemplateContainer.FindControl("pbTarget");
        captcha.Validate();
        if (!captcha.IsValid)
        {
            
            pbTarget.Visible = true;
            ScriptManager.RegisterClientScriptBlock(
                captcha,
                captcha.GetType(),
                "recaptcha",
                "Recaptcha._init_options(RecaptchaOptions);"
                + "if ( RecaptchaOptions && \"custom\" == RecaptchaOptions.theme )"
                + "{"
                + "  if ( RecaptchaOptions.custom_theme_widget )"
                + "  {"
                + "    Recaptcha.widget = Recaptcha.$(RecaptchaOptions.custom_theme_widget);"
                + "    Recaptcha.challenge_callback();"
                + "  }"
                + "} else {"
                + "  if ( Recaptcha.widget == null || !document.getElementById(\"recaptcha_widget_div\") )"
                + "  {"
                + "    jQuery(\"#" + pbTarget.ClientID + "\").html('<div id=\"recaptcha_widget_div\" style=\"display:none\"></div>');"
                + "    Recaptcha.widget = Recaptcha.$(\"recaptcha_widget_div\");"
                + "  }"
                + "  Recaptcha.reload();"
                + "  Recaptcha.challenge_callback();"
                + "}",
                true
            );
            // Show the error message                
            InvalidUserNameOrPasswordMessage.Text = "Security Code MISSING or INCORRECT!";
            InvalidUserNameOrPasswordMessage.Visible = true;

            // Cancel the create user workflow                
            e.Cancel = true;
        }
        
        // declare variable and assign it to the user name textbox
        string trimmedUserName = CreateUserWizard1.UserName.Trim();
        
        // Check for empty spaces infront and behind the string
        if (CreateUserWizard1.UserName.Length != trimmedUserName.Length)
        {
            // Show the error message           
            InvalidUserNameOrPasswordMessage.Text = "The username cannot contain leading or trailing spaces.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            // Cancel the create user workflow           
            e.Cancel = true;
        }
        else if (!Regex.Match(CreateUserWizard1.Password, passwordComplexityRegex).Success)
        {
            // Show the error message           
            InvalidUserNameOrPasswordMessage.Text = "Invalid password. Password length must be at least 5 characters and contain at least one uppercase letter, one lowercase letter and a number.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            // Cancel the create user workflow           
            e.Cancel = true;
        }
        else if (CreateUserWizard1.UserName.Length < 5 || CreateUserWizard1.UserName.Length > 50)
        {
            // Show the error message           
            InvalidUserNameOrPasswordMessage.Text = "Invalid Username length. Username length must be between 5 and 50 characters.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            // Cancel the create user workflow           
            e.Cancel = true;
        }
        
        else
        {
            // Username is valid, make sure that the password does not contain the username           
            if (!(CreateUserWizard1.Password.IndexOf(CreateUserWizard1.UserName, StringComparison.OrdinalIgnoreCase) < 0))
            {
                // Show the error message                
                InvalidUserNameOrPasswordMessage.Text = "The username may not appear anywhere in the password.";
                InvalidUserNameOrPasswordMessage.Visible = true;

                // Cancel the create user workflow                
                e.Cancel = true;
            }
        }

        /* Validate customer against database */
        int custnbr = 9999999;
        try
        {
            custnbr = Int32.Parse(((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custNbr")).Text);
        }
        catch
        {
            InvalidUserNameOrPasswordMessage.Text = "Please enter a valid customer number.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            e.Cancel = true;
        }
        //custname = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custName")).Text;
        int verify = verifyCustNbrName(custnbr);
        if (verify == 0)
        {
            InvalidUserNameOrPasswordMessage.Text = "Customer Number does not match customer name on file. " + 
                "<br/>Please re-enter your customer number and name exactly as printed on statement.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            e.Cancel = true;
        }
        else if (verify == 2)
        {
            InvalidUserNameOrPasswordMessage.Text = "Invalid customer number, please check and re-enter.";
            InvalidUserNameOrPasswordMessage.Visible = true;
            e.Cancel = true;
        }
        else if (verify == 3)
        {
            InvalidUserNameOrPasswordMessage.Text = "Customer Number already registered to a user account. " +
                "<br/>Please login <a href=\"Members/Default.aspx\">here.</a>";
            InvalidUserNameOrPasswordMessage.Visible = true;
            e.Cancel = true;
        }
    }

    #endregion

    #region get state list from app code folder and bind them to the dropdown list

    protected void CreateUserWizard1_Load(object sender, EventArgs e)
    {
        // get state names from app_code folder and bind them to the dropdown list
        DropDownList ddlStates = CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("ddlStates") as DropDownList;
        ddlStates.DataSource = UnitedStates.StateNames.GetStates();
        ddlStates.DataBind();
    }

    #endregion
    #region verify customer number and name against database
    
    protected int verifyCustNbrName(int custNbr)
    {
        int success = 0;
        SqlCommand cmd;
        string connString = ConfigurationManager.ConnectionStrings["dbMyCMSConnectionString"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(connString))
        {
            cmd = new SqlCommand("sp_VerifyCustomer", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@custNbr", custNbr);
            //cmd.Parameters.AddWithValue("@custName", custName);

            try
            {
                
                conn.Open();
                success = Int32.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                InvalidUserNameOrPasswordMessage.Text = "Error verifying customer: " + ex.Message;
                InvalidUserNameOrPasswordMessage.Visible = true;
            }
            finally
            {
                conn.Close();
            }
        }

        return success;
    }
    #endregion
    #region add newly registered user to default role (Member) and create profiles

    protected void CreateUserWizard1_ActiveStepChanged(object sender, EventArgs e)
    {
       
        // Have we JUST reached the Complete step?
        if (CreateUserWizard1.ActiveStep.Title != "Complete")
            return;
        else
        {
            header.Visible = false;
            Image1.Visible = false;
            InvalidUserNameOrPasswordMessage.Visible = false;
        }
   
        // add newly created user to default Role specified above
        if (Roles.RoleExists(wsatDefaultRole))
        {

            // Add the newly created user to the default Role.
            Roles.AddUserToRole(CreateUserWizard1.UserName, wsatDefaultRole);

            // Create an empty Profile for the newly created user
            ProfileCommon p = (ProfileCommon)ProfileCommon.Create(CreateUserWizard1.UserName, true);

            // Populate some Profile properties. Values are located in web.config file
            /*
            p.Company.Company = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbOfficeName")).Text;
            p.Company.Address = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbOfficeAddress")).Text;
            p.Company.City = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbOfficeCity")).Text;
            p.Company.State = ((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("ddlStates")).SelectedValue;
            p.Company.PostalCode = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbOfficeZip")).Text;
            p.Company.Phone = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbContactPhone")).Text;
            p.Company.Fax = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txbContactFax")).Text;
            p.Preferences.Newsletter = ((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("ddlNewsletter")).SelectedValue;
            */
            p.CustomerDetails.CustNbr = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custNbr")).Text;
            /* Get Customer Name */
            SqlCommand cmd;
            string connString = ConfigurationManager.ConnectionStrings["dbMyCMSConnectionString"].ConnectionString;
            string name = "";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                cmd = new SqlCommand("SELECT custName FROM CACHED_Customers WHERE custNbr = @custNbr", conn);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@custNbr", 
                    ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custNbr")).Text);

                try
                {

                    conn.Open();
                    object retVal = cmd.ExecuteScalar();
                    name = retVal == null ? "Default" : retVal.ToString();
                }
                catch (Exception ex)
                {
                    InvalidUserNameOrPasswordMessage.Text = ex.Message;
                    InvalidUserNameOrPasswordMessage.Visible = true;
                }
                finally
                {
                    conn.Close();
                }
            }
            p.CustomerDetails.CustName = name;
            //p.CustomerDetails.CustName = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("custName")).Text; 
            p.Preferences.eBilling = "NotSet";
            // Save profile - must be done since we explicitly created it
            p.Save();

        }
        else
        {
            // Show the error message                
            InvalidUserNameOrPasswordMessage.Text = "Oops! The default Role does not exist for this site.<br/>The site administrator has been contacted and made aware of this error...";
            InvalidUserNameOrPasswordMessage.Visible = true;

            // notify administrator via email
            try
            {
                // get default email address from web.config
                string AdminEmail = ConfigurationManager.AppSettings["AdminEmail"];
                string emailToWebConfig = AdminEmail.ToString();

                // create timestamp
                string timeStamp = DateTime.Now.ToString("F");

                // send mail
                System.Net.Mail.MailMessage MyMailer = new System.Net.Mail.MailMessage();
                MyMailer.To.Add(emailToWebConfig);
                MyMailer.From = new MailAddress(wsatDefaultRoleMissing, "Missing Default Role Notification");
                MyMailer.Subject = "Missing Default Role Notification";
                MyMailer.Body = "Dear Site Administrator, <br/><br/> You have recieved a Missing Default Role Notification. <br/> Please remember to remedy the problem.</b><br/>";
                MyMailer.Body += "time of occurance: " + timeStamp;
                MyMailer.Body += "<br/><br/><b>Have a wonderful day!";
                MyMailer.IsBodyHtml = true;
                MyMailer.Priority = MailPriority.High;
                SmtpClient client = new SmtpClient();
                client.EnableSsl = true;
                client.Send(MyMailer);

            }
            catch
            {
                // do nothing
            }
        }
    }

    #endregion

    #region send verification email to new registrant

    // this code uses the CreateUserWizard.txt file in the EmailTemplates folder -
    // it creates and sends a verification URL to the user before allowing login -
    // user must click on link in email to verify email address -
    // the verification takes place in Verification.aspx file
    protected void CreateUserWizard1_SendingMail(object sender, MailMessageEventArgs e)
    {
        // Get the UserId of the newly added user
        MembershipUser newUser = Membership.GetUser(CreateUserWizard1.UserName);
        Guid newUserId = (Guid)newUser.ProviderUserKey;

        // Determine the full verification URL (i.e., http://yoursite.com/verification.aspx?ID=...)
        string urlBase = /*Request.Url.GetLeftPart(UriPartial.Authority) +*/ "https://olp1.eastonutilities.com";
        string verifyUrl = "/verification.aspx?ID=" + newUserId.ToString();
        string fullUrl = urlBase + verifyUrl;

        // Replace <%VerificationUrl%> with the appropriate URL and querystring
        e.Message.Body = e.Message.Body.Replace("<%VerificationUrl%>", fullUrl);
    }

    #endregion

    #region send alert email to admin to notify of new registration

    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        // Determine the full URL (i.e., http://yoursite.com/admin/users-a-z.aspx)
        string urlBase = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
        string pageUrl = "/admin/users-a-z.aspx";
        string fullUrl = urlBase + pageUrl;
        
        try
        {
            // get default email address from web.config
            string AdminEmail = ConfigurationManager.AppSettings["AdminEmail"];
            string emailToWebConfig = AdminEmail.ToString();

            // create timestamp
            string timeStamp = DateTime.Now.ToString("F");

            // send mail
            System.Net.Mail.MailMessage MyMailer = new System.Net.Mail.MailMessage();
            MyMailer.To.Add(emailToWebConfig);
            MyMailer.From = new MailAddress(wsatNewRegistration, "New Account Registration Notification");
            MyMailer.Subject = "New Account Registration Notification";
            MyMailer.Body = "Dear Administrator, <br/><br/> You have recieved a New Account Registration Notification. <br/> Please login to your web site administration panel and visit the users a-z section under the user accounts menu.";
            MyMailer.Body += "<br/><br/>To view the latest application requests:" +  fullUrl + "</b><br/> ";
            MyMailer.Body += "Time of Registration: " + timeStamp;
            MyMailer.Body += "<br/><br/><b>Have a wonderful day!";
            MyMailer.IsBodyHtml = true;
            MyMailer.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.EnableSsl = true;
            client.Send(MyMailer);
        }
        catch
        {
            // do nothing
        }
    }

    #endregion
}