﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class members_BillPayFrame : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        iframe1.Attributes["src"] = "https://olp1.eastonutilities.com/members/billpayprocessing.aspx";
        //iframe1.Attributes["src"] = "http://localhost:58656/eBillingEU/members/billpayprocessing.aspx";
        iframe1.DataBind();
    }
}