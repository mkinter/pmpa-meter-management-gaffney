﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="recover-password2.ascx.cs" Inherits="controls_recover_password" %>
<%@ Register TagPrefix="cc2" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%-- PASSWORD RECOVERY CONTROL --%>
<div class="prWrap">
    <div class="prTitle">
        PASSWORD RECOVERY</div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="PasswordRecovery1" EnableClientScript="False" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="ctl00$PasswordRecovery1" EnableClientScript="False" />
    
    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" OnSendingMail="PasswordRecovery1_SendingMail" OnVerifyingUser="PasswordRecovery1_VerifyingUser" SuccessText="Thank you! A link to reset your password has been sent to your inbox." OnUserLookupError="PasswordRecovery1_UserLookupError">
       
        <UserNameTemplate>
            <div class="pwRecoveryIcon">
            </div>
            <div class="clearBoth2">
            </div>
            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Enter User Name:*</asp:Label>
            <br />
            <asp:TextBox runat="server" ID="UserName" MaxLength="50" ValidationGroup="PasswordRecovery1" ToolTip="enter your user name."></asp:TextBox>
            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="PasswordRecovery1" EnableClientScript="False" Display="Dynamic">*</asp:RequiredFieldValidator>
            
            <div class="clearBoth2">
            </div>
            <div runat="server" id="pbTarget" visible="false"></div>
    <p>Enter Security Code:*</p>
    <recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    PublicKey="6LdL-90SAAAAABpYYdt8DQNuFYEd60FIDskg2754"
    PrivateKey="6LdL-90SAAAAAKn0cRJdwFt9kEBBhC3z_mIkcBHH"
    />
            <div class="clearBoth2">
            </div>
            
            <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" ValidationGroup="PasswordRecovery1" />
        </UserNameTemplate>
        <QuestionTemplate>
            <div class="pwRecoveryIcon">
            </div>
            <div class="clearBoth2">
            </div>
            <div class="hr">
                <b>IDENTITY CONFIRMATION</b>
            </div>
            Please answer the following question to receive your password:
            <div class="clearBoth2">
            </div>
            User Name:
            <asp:Literal ID="UserName" runat="server"></asp:Literal>
            <div class="clearBoth2">
            </div>
            Security Question:
            <asp:Literal ID="Question" runat="server"></asp:Literal>
            <br />
            <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer:*</asp:Label>
            <br />
            <asp:TextBox ID="Answer" runat="server" ValidationGroup="ctl00$PasswordRecovery1"></asp:TextBox>
            <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" ErrorMessage="Answer is required." ToolTip="Answer is required." ValidationGroup="ctl00$PasswordRecovery1" EnableClientScript="False" Display="Dynamic">*</asp:RequiredFieldValidator>

            <div class="prMessage">
                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div class="clearBoth2">
            </div>
            <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" ValidationGroup="ctl00$PasswordRecovery1" />
        </QuestionTemplate>
    </asp:PasswordRecovery>
    <div class="prMessage">
                <asp:Label ID="FailureText" runat="server" ForeColor="Red"></asp:Label>
    </div>
</div>
