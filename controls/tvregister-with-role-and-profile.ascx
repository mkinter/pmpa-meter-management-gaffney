﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tvregister-with-role-and-profile.ascx.cs" Inherits="controls_register_with_role_and_profile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="cc2" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%-- ajax script manager --%>
<%-- create user wizard with role and profile control --%>
<div class="cuwWrap">
    <div runat="server" id="header" >
        <h2 style="width: 50%; margin: 0 auto; text-align: center;">REGISTRATION FORM</h2>
        
    <p>Use the form below to create an account to access Easton Cable’s Video Anywhere content.
Note: This account is also used to access the Easton Utilities My Account customer portal.
From My Account, you can view and pay your current bill.
</p>
        <p>Follow the instructions below to create a new registration for the new Easton Utilities Online Customer Information system. If you registered on the old system, you must still re-register below.
        </p>
    </div>
    <div class="cuwMessage">
        <asp:Label ID="InvalidUserNameOrPasswordMessage" runat="server" Visible="false" Text="Label" ForeColor="Red"></asp:Label>
    </div>
    <table style="width: 100%">
        <tr><td>
        <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" ContinueDestinationPageUrl="~/members/default.aspx" DuplicateUserNameErrorMessage="The user name that you entered is already in use. Please enter a different user name." OnCreatingUser="CreateUserWizard1_CreatingUser" OnSendingMail="CreateUserWizard1_SendingMail" DisableCreatedUser="True" LoginCreatedUser="False" CompleteSuccessText="Your account has been successfully created and a confirmation email has been sent to you. Please click on the link in this email to activate your account." OnActiveStepChanged="CreateUserWizard1_ActiveStepChanged" CreateUserButtonText="Submit Request" OnCreatedUser="CreateUserWizard1_CreatedUser">
        <MailDefinition BodyFileName="~/email_templates/create-user-wizard.txt" Subject="Your new membership is almost complete! ">
        </MailDefinition>
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                <ContentTemplate>
                    <%-- validation summary --%>
                    <asp:ValidationSummary ForeColor="Red" ID="ValidationSummary1" runat="server" ValidationGroup="CreateUserWizard1" />
                    <%-- error message --%>
                    <div class="cuwMessage">
                        <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                    <%-- icon display --%>
                    <div class="createUserIcon">
                    </div>
                    <div class="clearBoth2">
                    </div>
                    <%-- NEW ACCOUNT INFO --%>
                    <div class="hr">
                        <b>NEW ACCOUNT INFO</b>
                    </div>
                    <%-- CustNbr --%>
                    Customer Number*:
                    <br />
                    <asp:TextBox ID="custNbr" runat="server" ToolTip="enter customer number as printed on bill" MaxLength="150"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCustNbr" runat="server" ControlToValidate="custNbr" ErrorMessage="Customer Number is required." ToolTip="Customer Number is required." ValidationGroup="CreateUserWizard1">* </asp:RequiredFieldValidator>
                    
                    <div class="clearBoth2">
                    </div>
                    <%-- CustName --%>
                    
                    <%-- user name --%>
                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:*</asp:Label>
                    <br />
                    <asp:TextBox ID="UserName" runat="server" ToolTip="enter a desired user name" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    <div class="clearBoth2">
                    </div>
                    <%-- password --%>
                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:*</asp:Label>
                    <br />
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                    <br />
                    <cc1:PasswordStrength ID="Password_PasswordStrength" runat="server" TargetControlID="Password"  MinimumNumericCharacters="1" PreferredPasswordLength="7" TextCssClass="passwordStrengthIndicator" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent" MinimumSymbolCharacters="1">
                    </cc1:PasswordStrength>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    <div class="clearBoth2">
                    </div>
                    <%-- confirm password --%>
                    <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:*</asp:Label>
                    <br />
                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                    <div class="clearBoth2">
                    </div>
                    <%-- email --%>
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:*</asp:Label>
                    <br />
                    <asp:TextBox ID="Email" runat="server" ToolTip="enter your contact email" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rgxEmailRequired" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RegularExpressionValidator>
                    <%-- SECURITY QUESTION --%>
                   
                    <div class="clearBoth2">
                    </div>
                    <div class="hr">
                        <b>SECURITY CODE</b>
                    </div>
                    <%-- CAPTCHA 
                    <div title="enter the code shown on the image.">
                        <cc2:CaptchaControl ID="CAPTCHA" runat="server" CaptchaFontWarping="Medium" LayoutStyle="Vertical" ShowSubmitButton="False" CssClass="captcha" />
                    </div>--%>
                    <div runat="server" id="pbTarget" visible="false"></div>
    <p>Enter Security Code:*</p>
    <recaptcha:RecaptchaControl
    ID="recaptcha"
    runat="server"
    PublicKey="6LdL-90SAAAAABpYYdt8DQNuFYEd60FIDskg2754"
    PrivateKey="6LdL-90SAAAAAKn0cRJdwFt9kEBBhC3z_mIkcBHH"
    />
                </ContentTemplate>
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard>
    
        </td>
        <td valign="top">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/statementShot1.gif" /></td>
        </tr>
    </table>
    
</div>
