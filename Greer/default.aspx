﻿<%@ Page EnableEventValidation="false" Title="Welcome" Language="C#" MasterPageFile="~/Greer/themes/default/default.master" AutoEventWireup="true" 
    CodeFile="Default.aspx.cs" Inherits="members_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var baseUrl = '<%= Page.ResolveClientUrl("~/") %>';
    </script>
    <script type="text/javascript" src="../js/jquery.1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/highcharts.js"></script>
    <script type="text/javascript" src="../js/drilldown.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="../js/meterdatadriver.js?v=3"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hfCompanyId" runat="server" ClientIDMode="Static" />
    <div id="loading" style="text-align:center;padding-top:190px;padding-right:100px;padding-left:100px;">
        <h2 style="display: inline;">Loading: </h2>
        <h2 id="txt_loading" style="display: inline;">...</h2>
        <div style="margin-top:10px;">
            <div id="progressbar"></div>
        </div>
    </div>
    
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    <div id="divDisclaimer" style="text-align:center;font-size:8.5pt;margin-top:5px;margin-bottom:20px;">
        The values shown are only estimates and may be corrected at any time.  Actual billing data may vary from these estimates if errors are present.
    </div>
    <div style="position:absolute;top:595px;">
        <h3 style="display: inline; margin-right: 10px;">Meter Group:</h3> 
        <asp:DropDownList ID="ddl_meter" ClientIDMode="Static" runat="server" 
            DataSourceID="sdsMeters" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
        <button class="btn btn-primary btn-sm" onclick="btnUpdate_onclick(); return false;">Update</button>
    </div>
    
    <div id="divDetailLoading" style="margin-top:75px;text-align:center;">
        <asp:Label ID="lblLoadingDetails" runat="server" text="Load Details" style="margin-right:10px;"></asp:Label>
        <asp:Image ID="imgloading" ClientIDMode="Static" runat="server" ImageUrl="~/images/LoadingImage.gif" style="border-width: 0px;" />
    </div>

    <div id="divDrillDownHeader" style="margin-top:60px;border-radius:4px;border:1px solid #eee;background-color:#f7f7f7;padding:12px;box-shadow: 0 0 8px rgba(0,0,0,.1);-webkit-box-shadow: 0 0 8px rgba(0,0,0,.1);-moz-box-shadow: 0 0 8px rgba(0,0,0,.1);">
        <div style="float:left;"><label id="lblDDHeader" style="position:relative;top:5px;font-weight:bold;"></label></div>
        <div style="float:right;"><button class="btn btn-default btn-sm" onclick="closeDetails();return false;">Close</button></div>
        <div style="clear:both;"></div>

        <div id="readResults" style="margin-top:20px;">

        </div>
    </div>
    
    <asp:SqlDataSource ID="sdsMeters" runat="server" 
        ConnectionString="<%$ ConnectionStrings:cpSOCKET_MultiSpeak %>" SelectCommand="EXEC	[MR].[spGetMeterGroupsByMeterNumber] @companyId">
        <SelectParameters>
            <asp:SessionParameter Name="CompanyId" Type="Int32" SessionField="CompanyId"  />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
