﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text.RegularExpressions;
using Utilities;

public partial class members_themes_default_default : System.Web.UI.MasterPage
{
    public int SiteCompanyId()
    {
        int result = ConfigurationManager.AppSettings["CompanyId"] != null ? Parser.ParseInt(ConfigurationManager.AppSettings["CompanyId"].ToString()) : 0;
        return result;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileCommon profile = null;
        Page.ClientTarget = "uplevel";
        if (!Page.IsPostBack)
        {
            //Session["CompanyId"] = 4;

            if (Page.User.Identity.IsAuthenticated)
            {
                profile = Profile;

                Session["CompanyId"] = Profile.CustomerDetails.CompanyId;
                Session["AccountId"] = Profile.CustomerDetails.CustNbr;

                if(SiteCompanyId() != Profile.CustomerDetails.CompanyId)
                {
                    Session.RemoveAll();
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                    Response.Redirect("~/login.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                string[] roles = Roles.GetRolesForUser();
                linkAdmin.Visible = false;
                foreach (string role in roles)
                {
                    if (role.CompareTo("Administrator") == 0)
                        //linkAdmin.Visible = true;
                        Response.Redirect("~/admin");
                }
            }
            
            /*
            if (Session["OneLoc"] == null && Request.Url.AbsolutePath != "/eBillingEU/Members/Locations.aspx")
                Response.Redirect("Locations.aspx");
            if (Session["OneLoc"] == "1")
                lblAllLocLink.Visible = false;*/
            if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)BillPay.aspx").Success )
            {
                
                //LocationsBar.Visible = false;
                //pnlLocations.Visible = false;
                //imgPayBillTab.ImageUrl = "~/members/themes/default/images/tabs/PayBillTabWhite.png";
            }
            else if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)Default.aspx").Success)
            {
                //imgOverviewTab.ImageUrl = "~/members/themes/default/images/tabs/OverviewTabWhite.png";
                // get the selected user's profile based on query string
                //if (profile.Preferences.eBilling == "NotSet")
                //    eBillingNote.Visible = true;
            }
            else if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)Trends.aspx").Success)
            {
                //imgTrendsTab.ImageUrl = "~/members/themes/default/images/tabs/TrendsTabWhite.png";
            }
            else if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)OutageMap.aspx").Success)
            {
               //LocationsBar.Visible = false;
                //pnlLocations.Visible = false;
                //imgMapTab.ImageUrl = "~/members/themes/default/images/tabs/OutageTabWhite.png";
            }
            else if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)BillPayProcessing.aspx").Success)
            {
                //LocationsBar.Visible = false;
                //pnlLocations.Visible = false;
                
            }
            else if (Regex.Match(Request.Url.AbsolutePath, @"(?:.*)BillPayFrame.aspx").Success)
            {
                //LocationsBar.Visible = false;
                //pnlLocations.Visible = false;

            }
            else
            {
                //LocationsBar.Visible = false;
                //pnlLocations.Visible = false;
                //imgPayBillTab.ImageUrl = "~/members/themes/default/images/tabs/PayBillTabGray.png";
                //imgOverviewTab.ImageUrl = "~/members/themes/default/images/tabs/OverviewTabGray.png";
                //imgTrendsTab.ImageUrl = "~/members/themes/default/images/tabs/TrendsTabGray.png";
            }

            #region Location Bar INIT
            //if (gvLocations.Rows != null && gvLocations.Rows.Count > 0)
            //{
            //    if (Session["LocationID"] != null)
            //        SetSelectedRow(Session["LocationID"].ToString());
            //    else
            //    {
            //        gvLocations.SelectedIndex = 0;
            //        gvLocations_SelectedIndexChanged(gvLocations, null);
            //    }
            //}
            //if (profile != null)
            //{
            //    string name = profile.CustomerDetails.CustName;
            //    name = name == null ? "" : name.ToLower();
            //    lblName.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
            //            name);
            //}
            #endregion  
        }
    }
    private void SetSelectedRow(string LocationID)
    {
        if (string.IsNullOrEmpty(LocationID))
            return;

        //foreach (GridViewRow row in gvLocations.Rows)
        //{
        //    if (row.RowType == DataControlRowType.DataRow)
        //    {
        //        if (gvLocations.DataKeys[row.RowIndex].Value.ToString() == LocationID)
        //        {
        //            gvLocations.SelectedIndex = row.RowIndex;
        //            gvLocations_SelectedIndexChanged(gvLocations, null);

        //            return;
        //        }
        //    }
        //}
    }
    #region Location Bar methods
    protected void gvLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Clear();

            e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'; this.style.backgroundColor='lightsteelblue';");
            //e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.gvLocations, "Select$" + e.Row.RowIndex));

            if (gv.SelectedIndex != e.Row.RowIndex)
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");

        }
    }
    protected void gvLocations_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView gv = (GridView)sender;
        GridViewRow row = gv.SelectedRow;
        string locationID = gv.DataKeys[row.RowIndex].Value.ToString();
        Session["LocationID"] = locationID;

        foreach (GridViewRow currRow in gv.Rows)
        {
            if (currRow.RowType == DataControlRowType.DataRow)
            {
                currRow.Attributes.Clear();

                currRow.Attributes.Add("onmouseover", "this.style.cursor='pointer'; this.style.backgroundColor='lightsteelblue';");
                //currRow.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.gvLocations, "Select$" + currRow.RowIndex));

                if (gv.SelectedIndex != currRow.RowIndex)
                    currRow.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");
            }
        }
        if (Session["LocationID"] != null)
        {
            string locID = Session["LocationID"].ToString();
            //imgTrendsTab.Visible = App_eBilling_Customer_Data.hasUtilities(locID);
        }

    }
    #endregion

    protected void gvMeters_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Clear();

            e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'; this.style.backgroundColor='lightsteelblue';");
            //e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.gvLocations, "Select$" + e.Row.RowIndex));

            if (gv.SelectedIndex != e.Row.RowIndex)
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='white';");

        }
    }

    protected void gvMeters_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }

    protected void eBillingAccept_Click(object sender, EventArgs e)
    {
        MembershipUser memUser = Membership.GetUser();


        App_eBilling_Customer_Data.setEbillFlag(true, Profile.CustomerDetails.CustNbr);
        Profile.Preferences.eBilling = "True";
        Profile.Save();
        //eBillingAccept.Visible = eBillingDecline.Visible = false;
        //eBillingLBL.Text = "You are now subscribed to eBilling and will receive your next bill via email to " +
        //    memUser.Email + ".<br/> You may change this setting anytime in 'Edit Account->Edit Subscriptions'";
    }
    protected void eBillingDecline_Click(object sender, EventArgs e)
    {
        App_eBilling_Customer_Data.setEbillFlag(false, Profile.CustomerDetails.CustNbr);
        Profile.Preferences.eBilling = "False";
        Profile.Save();
        //eBillingAccept.Visible = eBillingDecline.Visible = false;
        //eBillingLBL.Text = "You have declined the option for eBilling."
        //    + ".<br/> You may change this setting anytime in 'Edit Account->Edit Subscriptions'";
    }    
    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        //Session.RemoveAll();
        //Session.Abandon();

        Session.RemoveAll();
        Session.Abandon();
        FormsAuthentication.SignOut();
        HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
        Response.Redirect("~/login.aspx", false);
        Context.ApplicationInstance.CompleteRequest();
        
    }
    protected void imgTrendsTab_Click(object sender, ImageClickEventArgs e)
    {

    }
}
